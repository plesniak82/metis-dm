﻿namespace MetisDM.Database.Metis2.Enums
{
	public enum InterfaceKinds
	{
		Measurement	= 1,
		Connection	= 2
	}
}
