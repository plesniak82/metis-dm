﻿namespace MetisDM.Database.Metis2.Enums
{
	public enum ClientResourceTypes
	{
		MeasurementPoint	= 1,
		Device				= 2,
		SimCard				= 3,
		DeviceType			= 4,
		Subject				= 5,
		Terminal			= 6,
	}
}
