﻿using System.Data.Objects;

namespace MetisDM.Database.Metis2
{
	public partial class Metis2Entities : ObjectContext
	{
		public Metis2Entities(string connectionString, int commandTimeout) : base(connectionString, "Metis2Entities")
        {
            ContextOptions.LazyLoadingEnabled = true;
			CommandTimeout = commandTimeout;
        }
	}
}
