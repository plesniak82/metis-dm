﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetisDM.Database.Metis2.Writter
{
	public class TransmissionData
	{
		public Guid Id													{get; set;}
		public DateTimeOffset Date										{get; set;}
		public int SourceDeviceTypeId									{get; set;}
		public String SourceDeviceConnectionId							{get; set;}
		public int TargetDeviceTypeId									{get; set;}
		public String TargetDeviceConnectionId							{get; set;}
		public int TransmissionTypeId									{get; set;}
		public Guid? ParentTransmissionId								{get; set;}
		public List<TransmissionSectionData>  List_TransmissionSections	{get; set;}

		public TransmissionData()
		{}

		public TransmissionData(	Guid id, 
									Guid? parentTransmissionId, 
									int transmissionTypeId, 
									int targetDeviceTypeId, 
									string targetDeviceConnectionId, 
									int sourceDeviceTypeId,
									string sourceDeviceConnectionId,
									DateTimeOffset date, List<TransmissionSectionData> transmissionSections)
		{
			Id = id;
			Date = date;
			SourceDeviceTypeId = sourceDeviceTypeId;
			SourceDeviceConnectionId = sourceDeviceConnectionId;
			TargetDeviceTypeId = targetDeviceTypeId;
			TargetDeviceConnectionId = targetDeviceConnectionId;
			TransmissionTypeId = transmissionTypeId;
			ParentTransmissionId = parentTransmissionId;
			List_TransmissionSections = transmissionSections;

			if (List_TransmissionSections != null)
			{
				List_TransmissionSections.ForEach(e => e.TransmissionId = Id);
			}
		}

		public override string ToString()
		{
			return String.Format("[Id]: {0} [ParentTransmissionId]: {1}", Id, ParentTransmissionId);
		}
	}

	public class TransmissionSectionData
	{
		public Guid Id																{get; set;}			
		public Guid TransmissionId													{get; set;}
		public int SourceDeviceTypeId												{get; set;}
		public String SourceDeviceConnectionId										{get; set;}
		public int TargetDeviceTypeId												{get; set;}
		public String TargetDeviceConnectionId										{get; set;}
		public int TransmissionSectionTypeId										{get; set;}
		public bool HasError														{get; set;}
		public String ErrorDescription												{get; set;}
		public TransmissionSectionGsmData List_TransmissionSectionGsm				{get; set;}
		public TransmissionSectionRadioData List_TransmissionSectionRadio			{get; set;}
		public TransmissionSectionTcpData List_TransmissionSectionTcp				{get; set;}

		public TransmissionSectionData()
		{}

		public TransmissionSectionData(int sourceDeviceTypeId, string sourceDeviceConnectionId, int targetDeviceTypeId, string targetDeviceConnectionId, int transmissionSectionTypeId, bool hasError, string errorDescription, TransmissionSectionGsmData sectionGsm, TransmissionSectionRadioData sectionRadio, TransmissionSectionTcpData transmissionSectionTcp)
		{
			List_TransmissionSectionTcp = transmissionSectionTcp;
			List_TransmissionSectionRadio = sectionRadio;
			List_TransmissionSectionGsm = sectionGsm;
			ErrorDescription = errorDescription;
			HasError = hasError;
			TransmissionSectionTypeId = transmissionSectionTypeId;
			TargetDeviceConnectionId = targetDeviceConnectionId;
			TargetDeviceTypeId = targetDeviceTypeId;
			SourceDeviceConnectionId = sourceDeviceConnectionId;
			SourceDeviceTypeId = sourceDeviceTypeId;
			Id = AT.Utils.MathExtension.SequentialGuid();

			if (List_TransmissionSectionGsm != null)		List_TransmissionSectionGsm.Id = Id;
			if (List_TransmissionSectionRadio != null)		List_TransmissionSectionRadio.Id = Id;
			if (List_TransmissionSectionTcp != null)		List_TransmissionSectionTcp.Id = Id;
		}

		public override string ToString()
		{
			return String.Format("[Id]: {0} [TransmissionId]: {1}", Id, TransmissionId);
		}
	}

	public class TransmissionSectionGsmData
	{
		public Guid Id				{get; set;}
		public int Lac				{get; set;}
		public int Cid				{get; set;}
		public int Rssi				{get; set;}
		public int? Iccid			{get; set;}
		public int? ErrorCode		{get; set;}

		public TransmissionSectionGsmData()
		{}

		public TransmissionSectionGsmData(int lac, int cid, int rssi, int? iccid, int? errorCode)
		{
			ErrorCode = errorCode;
			Iccid = iccid;
			Rssi = rssi;
			Cid = cid;
			Lac = lac;
		}

		public override string ToString()
		{
			return String.Format("[Id]: {0}", Id);
		}
	}

	public class TransmissionSectionRadioData
	{
		public Guid Id		{get; set;}
		public int? Rssi	{get; set;}
		public Decimal? Afc	{get; set;}
		public int Ttl		{get; set;}

		public TransmissionSectionRadioData()
		{}

		public TransmissionSectionRadioData(int? rssi, decimal? afc, int ttl)
		{
			Ttl = ttl;
			Afc = afc;
			Rssi = rssi;
		}

		public override string ToString()
		{
			return String.Format("[Id]: {0}", Id);
		}
	}

	public class TransmissionSectionTcpData
	{
		public Guid Id				{get; set;}
		public String IpEndPoint	{get; set;}

		public TransmissionSectionTcpData()
		{}

		public TransmissionSectionTcpData(string ipEndPoint)
		{
			IpEndPoint = ipEndPoint;
		}

		public override string ToString()
		{
			return String.Format("[Id]: {0}", Id);
		}
	}

	public class ReadParameterData
	{
		public String ReadPath					{get; set;}
		public int ReadParameterTypeId			{get; set;}
		public String Value						{get; set;}
		public byte? Tariff						{get; set;}
		public byte? BillingPeriodId			{get; set;}

		public ReadParameterData()
		{}

		public ReadParameterData(String readPath, int readParameterTypeId, String value, byte? tariff, byte? billingPeriodId)
		{
			ReadPath			= readPath;
			ReadParameterTypeId	= readParameterTypeId;
			Value				= value;
			Tariff				= tariff;
			BillingPeriodId		= billingPeriodId;
		}
	}

	public class ReadEventData
	{
		public String ReadPath;
		public int EventTypeId;
		[XmlIgnore]
		public DateTimeOffset? FirstOccurance;
		[XmlElement("FirstOccurance")]
		public string FirstOccuranceString
		{
			get { return FirstOccurance != null ? FirstOccurance.Value.ToString("yyyy-MM-dd HH:mm:ss zzz") : null; }
			set { FirstOccurance = value != null ? (DateTimeOffset?)DateTimeOffset.Parse(value) : null; }
		}
		[XmlIgnore]
		public DateTimeOffset? LastOccurence;
		[XmlElement("LastOccurence")]
		public string LastOccurenceString
		{
			get { return LastOccurence != null ? LastOccurence.Value.ToString("yyyy-MM-dd HH:mm:ss zzz") : null; }
			set { LastOccurence = value != null ? (DateTimeOffset?)DateTimeOffset.Parse(value) : null; }
		}
		public int? Count;
		public decimal? Duration;
		public decimal? Volume;
		public bool? HasEverOccured;
		public bool? IsCurrent;
		public bool? IsStored;
		public string Details;
		public bool IsAnalyzed;

		public ReadEventData()
		{}

		public ReadEventData(	String readPath,
								int eventTypeId,
								DateTimeOffset? firstOccurance,
								DateTimeOffset? lastOccurence,
								int? count,
								decimal? duration,
								decimal? volume,
								bool hasEverOccured,
								bool? isCurrent,
								bool? isStored,
								string details,
								bool isAnalyzed)
		{
			ReadPath		= readPath;
			EventTypeId		= eventTypeId;
			FirstOccurance	= firstOccurance;
			LastOccurence	= lastOccurence;
			Count			= count;
			Duration		= duration;
			Volume			= volume;
			HasEverOccured	= hasEverOccured;
			IsCurrent		= isCurrent;
			IsStored		= isStored;
			Details			= details;
			IsAnalyzed		= isAnalyzed;
		}

	}

	public class ReadData
	{	
		public Guid? TransmissionId								{get; set;}
		public int DeviceTypeId									{get; set;}
		public String ConnectionId								{get; set;}
		[XmlIgnore]
		public DateTimeOffset SystemDate						{get; set;}
		[XmlElement("SystemDate")]
		public string SystemDateString
		{
			get { return SystemDate.ToString("yyyy-MM-dd HH:mm:ss zzz"); }
			set { SystemDate = DateTimeOffset.Parse(value); }
		}
		[XmlIgnore]
		public DateTimeOffset MeasurementDate					{get; set;}
		[XmlElement("MeasurementDate")]
		public string MeasurementDateString
		{
			get { return MeasurementDate.ToString("yyyy-MM-dd HH:mm:ss zzz"); }
			set { MeasurementDate = DateTimeOffset.Parse(value); }
		}
		public bool IsLogged									{get; set;}
		public int ReadSourceId									{get; set;}
		public int? InstallationSetId							{get; set;}
		public List<ReadParameterData> ReadParameters			{get; set;}
		public List<ReadEventData> ReadEvents					{get; set;}
		public List<TransmissionData> Transmissions				{get; set;}

		public ReadData()
		{}

		public ReadData(Guid? transmissionId,
						int deviceTypeId,
						String connectionId,
						DateTimeOffset systemDate,
						DateTimeOffset measurementDate,
						bool isLogged,
						int readSourceId,
						int? installationSetId,
						List<ReadParameterData> readParameters, 
						List<ReadEventData> readEvents,
						List<TransmissionData> transmissions)
		{
			if (connectionId == null) throw new ArgumentNullException("connectionId");
			if (readParameters == null) throw new ArgumentNullException("readParameters");

			TransmissionId			= transmissionId;
			DeviceTypeId			= deviceTypeId;
			ConnectionId			= connectionId;
			SystemDate				= systemDate;
			MeasurementDate			= measurementDate;
			IsLogged				= isLogged;
			ReadSourceId			= readSourceId;
			InstallationSetId		= installationSetId;
			ReadParameters			= readParameters;
			ReadEvents				= readEvents ?? new List<ReadEventData>();
			Transmissions			= transmissions ?? new List<TransmissionData>();
		}
	}
}
