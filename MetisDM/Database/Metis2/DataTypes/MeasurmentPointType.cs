﻿using System;

namespace MetisDM.Database.Metis2.DataTypes
{
	public class MeasurmentPointType
	{
		public int? AddressNode_Id;
		public int DataSource;
		public String Description;
		public String GisAddress;
		public int? Id;
		public bool? IsActive;
		public String Location;
		public bool? MainMeter;
		public int MeasuredQuantity_Id;
		public String Name;
		public int? NetworkType_Id;
		public bool? TakeToBalance;

		public MeasurmentPointType(	int? addressNode_Id,
									int dataSource,
									String description,
									String gisAddress,
									int? id,
									bool? isActive,
									String location,
									bool? mainMeter,
									int measuredQuantity_Id,
									String name,
									int? networkType_Id,
									bool? takeToBalance)
		{
			AddressNode_Id			= addressNode_Id;
			DataSource				= dataSource;
			Description				= description;
			GisAddress				= gisAddress;
			Id						= id;
			IsActive				= isActive;
			Location				= location;
			MainMeter				= mainMeter;
			MeasuredQuantity_Id		= measuredQuantity_Id;
			Name					= name;
			NetworkType_Id			= networkType_Id;
			TakeToBalance			= takeToBalance;
		}
	}
}
