﻿namespace MetisDM.Database.Metis2.DataTypes
{
	public class InterfaceEventParameterTable
	{
		public int? ConnectionEvent_Id;
		public int? Event_Id;
		public int? Interface_Id;
		public int? InterfaceNumber;
		public bool? IsDefault;
		public int? Parameter_Id;

		public InterfaceEventParameterTable(	int? connectionEvent_Id,
												int? event_Id,
												int? interface_Id,
												int? interfaceNumber,
												bool? isDefault,
												int? parameter_Id)
		{
			ConnectionEvent_Id	= connectionEvent_Id;
			Event_Id			= event_Id;
			Interface_Id		= interface_Id;
			InterfaceNumber		= interfaceNumber;
			IsDefault			= isDefault;
			Parameter_Id		= parameter_Id;
		}
	}
}
