﻿using System;

namespace MetisDM.Database.Metis2.DataTypes
{
	public class DeviceModelTableType
	{
		public String ClientDescription;
		public int? DeviceModel_Id;
		public int? DeviceType_Id;
		public bool IsDefault;
		public String Name;

		public DeviceModelTableType(	String clientDescription,
										int? deviceModel_Id,
										int? deviceType_Id,
										bool isDefault,
										String name)
		{
			ClientDescription	= clientDescription;
			DeviceModel_Id		= deviceModel_Id;
			DeviceType_Id		= deviceType_Id;
			IsDefault			= isDefault;
			Name				= name;
		}
	}
}
