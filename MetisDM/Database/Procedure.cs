﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;

namespace MetisDM.Database
{
	public class Procedure
	{

		/// <summary>
		/// Run procedure
		/// </summary>
		/// <param name="dbConnection">Db connection from ObjectContext.Connection</param>
		/// <param name="procedureName">Procedure name with schema eg. dbo.TestProcedure</param>
		/// <param name="parameters">All parameters</param>
		/// <param name="resultCode">Result parameter to invoke DomainException errors</param>
		public static void Run(DbConnection dbConnection, string procedureName,IList<SqlParameter> parameters, SqlParameter resultCode)
		{
			var connection = (SqlConnection)((EntityConnection)dbConnection).StoreConnection;
			var cmd = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = 1800};       
			cmd.Parameters.AddRange(parameters.ToArray());

			if (resultCode != null && !parameters.Contains(resultCode))
			{
				cmd.Parameters.Add(resultCode);
			}

			try
			{
				if (connection.State != ConnectionState.Open)
				{
					connection.Open();
				}
				cmd.ExecuteNonQuery();
				if (resultCode != null && 
					resultCode.Value as int? != null && 
					(int) resultCode.Value != 0)
				{
					throw new Exception(resultCode.Value.ToString());
				}
			}
			catch (Exception ex)
			{
				throw;
				//if (!ex.ToString().Contains("System.Exception"))
				//{
					
				//}
			}
		}


		public static List<T> RunWithResult<T>(DbConnection dbConnection, string procedureName, IList<SqlParameter> parameters, SqlParameter resultCode) where T : class 
		{

			var connection = (SqlConnection)((EntityConnection)dbConnection).StoreConnection;
			var cmd = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure };
			cmd.Parameters.AddRange(parameters.ToArray());
			if (!parameters.Contains(resultCode))
			{
				cmd.Parameters.Add(resultCode);
			}

			try
			{
				connection.Open();
				var list = new List<T>();
				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					DataTable schemaTable = reader.GetSchemaTable();

					if (schemaTable != null && !schemaTable.HasErrors && schemaTable.Rows != null)
					{
						var pi = typeof(T).GetProperties();
                      

						/////////////!!!!!!!
						//if (rowList.Any())
						//{
						//    dataTableWithReflection = UserDefinedTable.GetDataTable<T>(rowList);
						//}


                         
						var constructor = typeof (T).GetConstructor(null);

						if (constructor != null)
						{
							foreach (DataRow row in schemaTable.Rows)
							{
								var newObject = constructor.Invoke(null);
								for (int i = 0; i < schemaTable.Columns.Count; ++i)
								{
									pi[i].SetValue(newObject, row[i],null);
								}

								list.Add((T)newObject);
							}

						}
						//foreach (DataRow row in schemaTable.Rows)
						//{
						//    foreach (DataColumn column in schemaTable.Columns)
						//    {
						//        Console.WriteLine(String.Format("{0} = {1}",
						//            column.ColumnName, row[column]));
						//        var rowValue = row[column];
						//    }
						//}

                        
					}
					else
					{
						resultCode.Value = "-100001";
					}

				}

				if ((int) resultCode.Value != 0)
				{
					throw new Exception(resultCode.Value.ToString());
				}
				return list;
			}
			catch (Exception)
			{
				//TODO:
				throw new Exception("15000");//Wystąpił nieokreślony błąd
			}
		}
	

		///// <summary>
		///// Run procedure
		///// </summary>
		///// <param name="dbConnection">Db connection from ObjectContext.Connection</param>
		///// <param name="procedureName">Procedure name with schema eg. dbo.TestProcedure</param>
		///// <param name="parameters">All parameters</param>
		///// <param name="resultCode">Result parameter to invoke DomainException errors</param>
		//public static void Run(DbConnection dbConnection, string procedureName,IList<SqlParameter> parameters, SqlParameter resultCode=null)
		//{

		//	var connection = dbConnection is EntityConnection ? (SqlConnection)((EntityConnection)dbConnection).StoreConnection : (SqlConnection)dbConnection;
		//	var cmd = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure };
		//	cmd.Parameters.AddRange(parameters.ToArray());
		//	if (resultCode != null && !parameters.Contains(resultCode))
		//	{
		//		cmd.Parameters.Add(resultCode);
		//	}

		//	try
		//	{
		//		if (connection.State != ConnectionState.Open)
		//			connection.Open();
		//		cmd.ExecuteNonQuery();
		//		if (resultCode != null && (int) resultCode.Value != 0)
		//		{
		//			throw new DomainException(resultCode.Value.ToString(), (int) resultCode.Value);
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		//TODO:
		//		//throw new DomainException("-100189", -100189, ex);//Wystąpił nieokreślony błąd
		//	   // throw new DomainException(ex.Message, 50000, ex);
		//		ex.ToString();

		//	}
		//	finally
		//	{
		//		resultCode.ToString();
		//	}
		//}



		//public static List<T> RunWithResult<T>(DbConnection dbConnection, string procedureName, IList<SqlParameter> parameters, SqlParameter resultCode) where T : class
		//{
		//	var connection = (SqlConnection)((EntityConnection)dbConnection).StoreConnection;
		//	var cmd = new SqlCommand(procedureName, connection) { CommandType = CommandType.StoredProcedure };
		//	cmd.Parameters.AddRange(parameters.ToArray());
		//	if (!parameters.Contains(resultCode))
		//	{
		//		cmd.Parameters.Add(resultCode);
		//	}

		//	try
		//	{
		//		connection.Open();
		//		var list = new List<T>();

		//		using (SqlDataReader reader = cmd.ExecuteReader())
		//		{
		//			var pi = typeof(T).GetProperties();
		//			var constructor = typeof(T).GetConstructor(new Type[0]);

		//			if (constructor != null)
		//			{
		//				while (reader.Read())
		//				{
		//					var newObject = constructor.Invoke(null);

		//					foreach (var property in pi)
		//					{
		//						property.SetValue(newObject, reader[property.Name], null);
		//					}

		//					list.Add(newObject as T);
		//				}
		//			}
		//		}

		//		if ((int)resultCode.Value != 0)
		//		{
		//			throw new DomainException(resultCode.Value.ToString());
		//		}
		//		return list;
		//	}
		//	catch (DomainException)
		//	{
		//		throw;
		//	}
		//	catch (Exception ex)
		//	{
		//		//TODO:
		//		throw new DomainException("15000", ex);//Wystąpił nieokreślony błąd
		//	}
		//	finally
		//	{
		//		connection.Close();
		//	}
		//}

     
    }
}