﻿using System.Data.Objects;

namespace MetisDM.Database.Metis1
{
	public partial class Metis1Entities : ObjectContext
	{
		public Metis1Entities(string connectionString, int commandTimeout) : base(connectionString, "Metis1Entities")
        {
            ContextOptions.LazyLoadingEnabled = true;
			CommandTimeout = commandTimeout;
        }
	}
}
