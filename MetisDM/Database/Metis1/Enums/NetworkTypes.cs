﻿namespace MetisDM.Database.Metis1.Enums
{
	public enum NetworkTypes
	{
		WaterNetwork		= 1,
		HeatNetwork			= 2,
		ElectricNetwork		= 3,
		GasNetwork			= 4
	}
}
