﻿namespace MetisDM.Database.Metis1.Enums
{
	public enum MediumTypes
	{
		Electricity	= 1,
		ColdWater	= 2,
		HotWater	= 3,
		Gas			= 4,
		HeatEnergy	= 5,
		Temperature	= 6
	}
}
