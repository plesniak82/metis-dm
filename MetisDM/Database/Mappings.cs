﻿using System.Collections.Generic;
using AT.Models.Devices;
using Metis1.Enums;
using Metis2.Enums;
using NetworkTypes = Metis1.Enums.NetworkTypes;
using AddressNodeTypes = Metis1.Enums.AddressNodeTypes;

namespace MetisDM.Database
{
	public static class Mappings
	{
		private static readonly Dictionary<MediumTypes, MeasuredQuantities> _mediumTypeToMeasuredQuantity = new Dictionary<MediumTypes, MeasuredQuantities>
		{
			{MediumTypes.ColdWater, MeasuredQuantities.ColdWaterUsage},
			{MediumTypes.Electricity, MeasuredQuantities.ElectricEnergyUsage},
			{MediumTypes.Gas, MeasuredQuantities.GasUsage},
			{MediumTypes.HeatEnergy, MeasuredQuantities.HeatEnergyCost},
			{MediumTypes.HotWater, MeasuredQuantities.WarmWaterUsage},
			{MediumTypes.Temperature, MeasuredQuantities.Temperature}
		};

		private static readonly Dictionary<NetworkTypes, Metis2.Enums.NetworkTypes> _networkTypeToNetworkType = new Dictionary<NetworkTypes, Metis2.Enums.NetworkTypes>
		{
			{NetworkTypes.ElectricNetwork, Metis2.Enums.NetworkTypes.ElectricNetwork},
			{NetworkTypes.GasNetwork, Metis2.Enums.NetworkTypes.GasNetwork},
			{NetworkTypes.HeatNetwork, Metis2.Enums.NetworkTypes.HeatNetwork},
			{NetworkTypes.WaterNetwork, Metis2.Enums.NetworkTypes.WaterNetwork}
		};

		private static readonly Dictionary<AddressNodeTypes, Metis2.Enums.AddressNodeTypes> _addressNodeTypeToAddressNodeType = new Dictionary<AddressNodeTypes, Metis2.Enums.AddressNodeTypes>
		{
			{AddressNodeTypes.Building, Metis2.Enums.AddressNodeTypes.Building},
			{AddressNodeTypes.City, Metis2.Enums.AddressNodeTypes.City},
			{AddressNodeTypes.Local, Metis2.Enums.AddressNodeTypes.Local},
			{AddressNodeTypes.Staircase, Metis2.Enums.AddressNodeTypes.Staircase},
			{AddressNodeTypes.Street, Metis2.Enums.AddressNodeTypes.Street}
		};

		private static readonly Dictionary<DeviceTypes, KeyValuePair<Types, int?>> _deviceTypeToDeviceTypeModel = new Dictionary<DeviceTypes, KeyValuePair<Types, int?>>
		{
			{DeviceTypes.AtKEthRs232,		new KeyValuePair<Types, int?>(Types.AtKEthRs232Th,		null)},
			{DeviceTypes.AtKGsmRs232,		new KeyValuePair<Types, int?>(Types.AtKGsmRs232th,		null)},
			{DeviceTypes.AtKGsmRs2323Gth,	new KeyValuePair<Types, int?>(Types.AtKGsmRs2323Gth,	null)},
			{DeviceTypes.AtUptGsm01Rs,		new KeyValuePair<Types, int?>(Types.AtUptGsm01Rs,		(int)AT.Models.Devices.Models.AtUptGsm01.Basic)},
			{DeviceTypes.AtUptGsm01,		new KeyValuePair<Types, int?>(Types.AtUptGsm01,			(int)AT.Models.Devices.Models.AtUptGsm01.Basic)},
			{DeviceTypes.AtWmbus01,			new KeyValuePair<Types, int?>(Types.AtWmbus01,			(int)AT.Models.Devices.Models.AtWmbus01.Basic)},
			{DeviceTypes.AtWmbus04,			new KeyValuePair<Types, int?>(Types.AtWmbus04,			(int)AT.Models.Devices.Models.AtWmbus04.Basic)},
			{DeviceTypes.AtWmbus05,			new KeyValuePair<Types, int?>(Types.AtWmbus051,			(int)AT.Models.Devices.Models.AtWmbus051.Basic)},
			{DeviceTypes.AtWmbus06,			new KeyValuePair<Types, int?>(Types.AtWmbus06th,		(int)AT.Models.Devices.Models.AtWmbus06th.Basic)},
			{DeviceTypes.AtWmbus061,		new KeyValuePair<Types, int?>(Types.AtWmbus061,			(int)AT.Models.Devices.Models.AtWmbus061.Basic)},
			{DeviceTypes.AtWmbus07,			new KeyValuePair<Types, int?>(Types.AtWmbus07,			(int)AT.Models.Devices.Models.AtWmbus07.Basic)},
			{DeviceTypes.AtWmbus08,			new KeyValuePair<Types, int?>(Types.AtWmbus08,			(int)AT.Models.Devices.Models.AtWmbus08.Basic)},
			{DeviceTypes.AtWmbus09,			new KeyValuePair<Types, int?>(Types.AtWmbus09,			(int)AT.Models.Devices.Models.AtWmbus09.Basic)},
			{DeviceTypes.AtWmbus10,			new KeyValuePair<Types, int?>(Types.AtWmbus10,			(int)AT.Models.Devices.Models.AtWmbus10.Basic)},
			{DeviceTypes.AtWmbus11,			new KeyValuePair<Types, int?>(Types.AtWmbus11,			(int)AT.Models.Devices.Models.AtWmbus11.Basic)},
			{DeviceTypes.AtWmbus112,		new KeyValuePair<Types, int?>(Types.AtWmbus112,			(int)AT.Models.Devices.Models.AtWmbus112.Basic)},
			{DeviceTypes.AtWmbus161,		new KeyValuePair<Types, int?>(Types.AtWmbus161,			(int)AT.Models.Devices.Models.AtWmbus161.Basic)},
			{DeviceTypes.AtWmbus162,		new KeyValuePair<Types, int?>(Types.AtWmbus162,			(int)AT.Models.Devices.Models.AtWmbus162.Basic)},
			{DeviceTypes.AtWmbusMr01,		new KeyValuePair<Types, int?>(Types.AtWmbusMr01,		(int)AT.Models.Devices.Models.AtWmbusMr01.Basic)},
			{DeviceTypes.AtWmbusMr02,		new KeyValuePair<Types, int?>(Types.AtWmbusMr02,		(int)AT.Models.Devices.Models.AtWmbusMr02.Basic)},
			{DeviceTypes.AtWmbusMr03,		new KeyValuePair<Types, int?>(Types.AtWmbusMr03z,		(int)AT.Models.Devices.Models.AtWmbusMr03z.Basic)},
			{DeviceTypes.AtWmbusMr04, 		new KeyValuePair<Types, int?>(Types.AtWmbusMr04z,		(int)AT.Models.Devices.Models.AtWmbusMr04z.Basic)},
			{DeviceTypes.AtWmbusMr05,		new KeyValuePair<Types, int?>(Types.AtWmbusMr05z,		(int)AT.Models.Devices.Models.AtWmbusMr05z.Basic)},
			{DeviceTypes.AtWmbusMr06,		new KeyValuePair<Types, int?>(Types.AtWmbusMr06z,		(int)AT.Models.Devices.Models.AtWmbusMr06z.Basic)},
			{DeviceTypes.AtWmbusMr07,		new KeyValuePair<Types, int?>(Types.AtWmbusMr07z,		(int)AT.Models.Devices.Models.AtWmbusMr07z.Basic)},
			{DeviceTypes.AtWmbusMr09,		new KeyValuePair<Types, int?>(Types.AtWmbusMr09z,		(int)AT.Models.Devices.Models.AtWmbusMr09z.Basic)},
			{DeviceTypes.Eitn305,			new KeyValuePair<Types, int?>(Types.Eitn305,			(int)AT.Models.Devices.Models.Eitn305.Basic)},
			{DeviceTypes.Eitn3051,			new KeyValuePair<Types, int?>(Types.Eitn3051,			(int)AT.Models.Devices.Models.Eitn3051.Basic)},
			{DeviceTypes.Uniflo_GxE,		new KeyValuePair<Types, int?>(Types.UnifloGxE,			(int)AT.Models.Devices.Models.UnifloGxE.Basic)},
			{DeviceTypes.Uniflo_GxS,		new KeyValuePair<Types, int?>(Types.UnifloGxS,			(int)AT.Models.Devices.Models.UnifloGxS.Basic)},
			{DeviceTypes.Unismart,			new KeyValuePair<Types, int?>(Types.AtWmbusG01,			(int)AT.Models.Devices.Models.AtWmbusG01.Basic)},
			{DeviceTypes.CnRe5,				new KeyValuePair<Types, int?>(Types.CnRe5,				(int)AT.Models.Devices.Models.CnRe5.Basic)},
		};

		public static MeasuredQuantities MediumTypeToMeasuredQuantity(MediumTypes mediumType)
		{
			return _mediumTypeToMeasuredQuantity[mediumType];
		}
		public static Metis2.Enums.NetworkTypes NetworkTypeToNetworkType(NetworkTypes networkType)
		{
			return _networkTypeToNetworkType[networkType];
		}
		public static Metis2.Enums.AddressNodeTypes? AddressNodeTypeToAddressNodeType(AddressNodeTypes addressNodeType)
		{
			if (_addressNodeTypeToAddressNodeType.ContainsKey(addressNodeType))
			{
				return _addressNodeTypeToAddressNodeType[addressNodeType];
			}

			return null;
		}

		public static KeyValuePair<Types, int?>? DeviceTypeToDeviceTypeModel(DeviceTypes deviceType)
		{
			if (_deviceTypeToDeviceTypeModel.ContainsKey(deviceType))
			{
				return _deviceTypeToDeviceTypeModel[deviceType];
			}

			return null;
		}
	}
}
