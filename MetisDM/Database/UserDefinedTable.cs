﻿using System;
using System.Collections;
using System.Data;
using System.Linq;

namespace MetisDM.Database
{
    public class UserDefinedTable
    {
		public static DataTable GetDataTable<T>(IEnumerable list) where T:class 
        {
			if (list == null)
            {
                //throw new ArgumentNullException("list");
				return null;
            }

            var dt = new DataTable();
            // Get a list of all the properties on the object
            var pi = typeof (T).GetProperties();
			if (pi.Any())
			{
				// Loop through each property, and add it as a column to the datatable
				foreach (var p in pi.Where(e => !e.Name.ToLower().Contains("string") && 
												!e.Name.ToLower().Contains("list") && 
												!e.PropertyType.Name.ToLower().Contains("list")))
				{
					// The the type of the property
					var columnType = p.PropertyType;

					// We need to check whether the property is NULLABLE
					if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
					{
						// If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
						columnType = p.PropertyType.GetGenericArguments()[0];
					}

					if (columnType == typeof (UInt16))	columnType = typeof(int);
					if (columnType == typeof (UInt32))	columnType = typeof(int);
					if (columnType == typeof (UInt64))	columnType = typeof(long);

					// Add the column definition to the datatable.
					dt.Columns.Add(new DataColumn(p.Name, columnType));
				}	
			}

			var fields = typeof(T).GetFields();
			if (fields.Any())
			{
				foreach(var field in fields.Where(e =>	!e.Name.ToLower().Contains("string")&&
														!e.Name.ToLower().Contains("list")))
				{
					var columName = field.Name;
					var columnType = field.FieldType;

					if (columnType.IsGenericType && columnType.GetGenericTypeDefinition() == typeof(Nullable<>))
					{
						columnType = columnType.GetGenericArguments()[0];
					}

					if (columnType == typeof (UInt16))	columnType = typeof(int);
					if (columnType == typeof (UInt32))	columnType = typeof(int);
					if (columnType == typeof (UInt64))	columnType = typeof(long);

					dt.Columns.Add(new DataColumn(columName, columnType));
				}
			}

            // For each object in the list, loop through and add the data to the datatable.
            foreach (var obj in list)
            {
				if (pi.Any())
				{
					var row = new object[pi.Count(e => !e.Name.ToLower().Contains("string") && !e.Name.ToLower().Contains("list") && !e.PropertyType.Name.ToLower().Contains("list"))];
					var i = 0;

					foreach (var p in pi.Where(e => !e.Name.ToLower().Contains("string") && !e.Name.ToLower().Contains("list") && !e.PropertyType.Name.ToLower().Contains("list")))
					{
						row[i++] = p.GetValue(obj, null);
					}

					try
					{	
						if (row.Length > 0)
						{
							dt.Rows.Add(row);	
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex);
						throw;
					}					
				}
				if (fields.Any())
				{
					var row = new object[fields.Count(e => !e.Name.ToLower().Contains("string") && !e.Name.ToLower().Contains("list"))];
					var i = 0;

					foreach (var field in fields.Where(e => !e.Name.ToLower().Contains("string") && !e.Name.ToLower().Contains("list")))
					{
						try
						{
							row[i++] = field.GetValue(obj);
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.ToString());
						}						
					}
					try
					{
						if (row.Length > 0)
						{
							dt.Rows.Add(row);	
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.ToString());
					}
					
				}
            }

            return dt;
        }

        public static DataTable GetDataTable(int[] array, string columnName = "Id") 
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            if (string.IsNullOrEmpty(columnName))
            {
                throw new ArgumentException(@"Invalid column name", columnName);
            }

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn(columnName, typeof (int)));
            array.ToList().ForEach(i => dt.Rows.Add(i));
            return dt;
        }

    }
}