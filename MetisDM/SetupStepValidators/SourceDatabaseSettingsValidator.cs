﻿using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.Herplers;

namespace MetisDM.SetupStepValidators
{
    public class SourceDatabaseSettingsValidator : SetupStepBase
    {
        public override SetupStep SetupStep
        {
            get { return SetupStep.SourceDatabaseConnectionSettings; }
        }

        public override void Validate(Settings settings)
        {
            ClearErrors();
            var result = SqlHelper.TestSourceServerConnection(settings.SourceDatabaseConnectionString);
            if (result.Value.HasValue && result.Value.Value)
            {
                
            }
            else
            {
                if (result.HasErrors)
                {
                    AddErrors(result.Errors);
                }
            }
            RaiseValidated();
        }
    }
}
