﻿using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.Herplers;

namespace MetisDM.SetupStepValidators
{
    public class TargetDatabaseSettingsValidator : SetupStepBase
    {
        public override SetupStep SetupStep
        {
            get { return SetupStep.TargetDatabaseConnectionSettings; }
        }

        public override void Validate(Settings settings)
        {
            ClearErrors();
            var result = SqlHelper.TestDestinationServerConnection(settings.TargetDatabaseConnectionString);
            if (result.Value.HasValue && result.Value.Value)
            {
               
            }
            else
            {
                if (result.HasErrors)
                {
                    AddErrors(result.Errors);
                }
            }
            RaiseValidated();
        }
    }
}
