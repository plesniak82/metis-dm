﻿using System.Collections.Generic;
using System.Linq;
using MetisDM.Classes;
using MetisDM.Enums;

namespace MetisDM.SetupStepValidators
{
	public class DevicesMappingValidator : SetupStepBase
    {
        public override SetupStep SetupStep
        {
            get { return SetupStep.DevicesMapping; }
        }

        public override void Validate(Settings settings)
        {
			settings.DeviceMapping = settings.DevicesMappingViewModel.MeterModels.Select(e => new List<string>{e.MeterModel.Name, e.Metis2DeviceType != null ? e.Metis2DeviceType.Name : null}).ToList();
            ClearErrors();
            RaiseValidated();
        }
    }
}
