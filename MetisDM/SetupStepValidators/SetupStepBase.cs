﻿using System.Collections.Generic;
using MetisDM.Classes;
using MetisDM.Enums;

namespace MetisDM.SetupStepValidators
{
    public abstract class SetupStepBase : ValidateViewModel
    {
        public delegate void ValidationCompletedHandler(object sender, ResultStatus errors);
        public event ValidationCompletedHandler Validated;

        public abstract SetupStep SetupStep { get; }

        public abstract void Validate(Settings settings);

        public virtual IEnumerable<MigrationTaskType> GetNeededInstallationSteps()
        {
            return new List<MigrationTaskType>();
        }

        protected void RaiseValidated()
        {
            if (Validated != null)
            {
                Validated.Invoke(this, new ResultStatus(Errors));
                Validated = null;
            }
        }
    }
}
