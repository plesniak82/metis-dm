﻿using System;
using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.Herplers;

namespace MetisDM.SetupStepValidators
{
    public class GeneralSettingsValidator : SetupStepBase
    {
        public override SetupStep SetupStep
        {
            get { return SetupStep.GeneralSettings; }
        }

        public override void Validate(Settings settings)
        {
            ClearErrors();
            try
            {
                if (!string.IsNullOrEmpty(settings.LogFolder))
                {
                    if (FileSystemHelper.CheckFolderExists(settings.LogFolder))
                    {
						//if (settings.MigrationType == MigrationType.Migration)
						//{
						//	if (!FileSystemHelper.CheckFolderIsEmpty(settings.LogFolder))
						//	{
						//		AddError("Log folder is not empty");
						//	}
						//}
						//else
						//{
						//	if (!FileSystemHelper.CheckFileExists(settings.SettingsFile))
						//	{
						//		AddError("Migration settings file not exists in log folder");
						//	}
						//}
                    }
                    else
                    {
                        AddError("Log folder does not exists");
                    }
                }
                else
                {
                        AddError("Log folder is not set");
                }

				if (!string.IsNullOrEmpty(settings.SettingsFolder))
	            {
		            if (FileSystemHelper.CheckFolderExists(settings.SettingsFolder))
		            {
						//if (settings.MigrationType == MigrationType.Migration)
						//{
						//	if (!FileSystemHelper.CheckFileExists(settings.SettingsFile))
						//	{
						//	//    AddError("Settingsfile  doesn`t exists");
						//	}
						//}
		            }
		            else
		            {
						AddError(String.Format("Settings folder ({0}) does not exists", settings.SettingsFolder));
		            }
	            }

            }
            catch (Exception e)
            {
                AddError(e.Message);
            }
            RaiseValidated();
        }
    }
}
