﻿--SystemTelemetryczny
--Metis2_Sielanka_majowa

SET NOCOUNT ON
GO

/***********************************************************************
                SŁOWNIKI
***********************************************************************/
DECLARE @XML XML =  N'<?xml version=''1.0'' ?>
<Metis>
	<reads.ReadSource>
		<row>
		  <Id>6</Id>
		  <ResourceKey>Migration</ResourceKey>
		</row>
	</reads.ReadSource>
	<system.Resource>
		<row>
			<Key>Migration</Key>
			<DefaultValue>Migracja</DefaultValue>
			<CanDownloadWithoutAuthorization>0</CanDownloadWithoutAuthorization>
			<AddToApplicationResources>0</AddToApplicationResources>
			<DefaultSortIndex>1595</DefaultSortIndex>
	  </row>
	</system.Resource>
	
</Metis>
'

DECLARE @hdoc int
DECLARE @Dictionaries TABLE (TableName nvarchar(MAX))
DECLARE @NotrderedList dbo.StringTable
DECLARE @OrderedList dbo.KeyValueTableType

EXEC sp_xml_preparedocument @hdoc OUTPUT, @XML

;WITH XmlDoc AS (
	SELECT *
	FROM OPENXML( @hdoc, '//*',2)
)
INSERT INTO @NotrderedList
SELECT DISTINCT tbl.localname
FROM XmlDoc rt 
INNER JOIN XmlDoc tbl ON rt.id = tbl.parentID and rt.parentID is null

INSERT INTO @OrderedList([Key], [Value])
SELECT Id, TableName FROM dbo.udfGetOrderedTableList(@NotrderedList, 10)

EXEC dbo.XMLDataInsert @XML, @OrderedList
EXEC dbo.XMLDataUpdate @XML, @OrderedList
GO


/***********************************************************************
                READCALCULATOR
***********************************************************************/
ALTER PROCEDURE [reads].[uspReadsCalculate]
	@InstallationSetId			INT,
	@DeviceId					INT = null,
	@ReadsCalculatedCount		INT OUTPUT,
	@ResultCode					INT OUTPUT
AS
BEGIN
	DECLARE @Retry BIT = 1
	SET NOCOUNT ON
	SET @ResultCode = 0
	SET @ReadsCalculatedCount = 0

	WHILE @Retry = 1
	BEGIN TRY
		SET @Retry = 0
		--------------------------------------------------------------------------------------------------------
		-- BEGIN TRANSACTION
		--------------------------------------------------------------------------------------------------------
		DECLARE @TranStarted   bit
		SET @TranStarted = 0

		IF( @@TRANCOUNT = 0 )
		BEGIN
			BEGIN TRANSACTION
			SET @TranStarted = 1
		END
		ELSE
			SET @TranStarted = 0

		DECLARE @readIds dbo.IdTableType
		IF (@InstallationSetId IS NULL)
		BEGIN
			--sprawdzenie, czy serwer analiz przeanalizowa� ju� wszystkie datachangeLog
			DECLARE @saDataChangeLogId INT
			SELECT	@saDataChangeLogId = Value
			FROM	[system].[SystemSetting]
			WHERE	[SystemSettingType_Id] = 31

			IF (EXISTS(SELECT TOP 1 1 FROM [system].[DataChangeLog] WHERE Id > @saDataChangeLogId))
			BEGIN
				IF @TranStarted = 1 AND @@TRANCOUNT > 0
					COMMIT TRANSACTION
				RETURN
			END
			
			--pobranie id ostatnio zbadanego odczytu
			DECLARE @saReadId INT
			SELECT	@saReadId = Value
			FROM	[system].[SystemSetting]
			WHERE	[SystemSettingType_Id] = 32
		
			IF (@saReadId IS NULL)
				SET @saReadId = 0

			PRINT CONCAT('@saReadId: ', @saReadId)

			--pobranie id odczytow
			INSERT INTO @readIds SELECT DISTINCT TOP (3000) [Id] FROM [reads].[Read] WHERE [Id] > @saReadId ORDER BY Id ASC
		END
		ELSE		--gdy nie jest okre�lony installationSet, bademy wszystkie odczyty, kt�re jeszcze nie zosta�y zbadane
		BEGIN
			--pobranie id odczytow
			INSERT INTO @readIds 
			SELECT		DISTINCT r.[Id] 
			FROM		[reads].[Read] AS r
			JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = r.Id)
			JOIN		[measurement].[InstallationSet] AS ins ON (	ins.DestinationDevice_Id = r.Device_Id
																	AND (ins.SourceOutputInterface_Id is not null or SourceDevice_Id = DestinationDevice_Id)
																	AND r.MeasurementDate > ins.DateFrom 
																	AND	(ins.DateTo IS NULL OR r.MeasurementDate < ins.DateTo))
			WHERE		ins.Id = @InstallationSetId
		END

		if (@DeviceId is not null)
		begin
			INSERT INTO @readIds 
			SELECT		DISTINCT r.[Id] 
			FROM		[reads].[Read] AS r
			LEFT JOIN	@readIds as r2 on r2.Id = r.Id
			WHERE		r.Device_Id = @DeviceId
						and r2.Id is null
		end

		IF (NOT EXISTS (SELECT TOP 1 1 FROM @readIds))
		BEGIN
			IF @TranStarted = 1 AND @@TRANCOUNT > 0
				COMMIT TRANSACTION
			RETURN
		END

		--przypisanie installationsetoow do sciezek odczytowych
		UPDATE		ip
		SET			ip.InstallationSet_Id = ins.Id
		FROM		@readIds AS ri
		JOIN		[reads].[Read] AS r ON (r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		LEFT JOIN	[measurement].[InstallationSet] AS ins ON (	ins.ReadPath = ip.Path
																AND ins.DestinationDevice_Id = r.Device_Id
																AND (ins.SourceOutputInterface_Id is not null or SourceDevice_Id = DestinationDevice_Id)
																AND r.MeasurementDate > ins.DateFrom 
																AND	(ins.DateTo IS NULL OR r.MeasurementDate < ins.DateTo))

		--przypisanie ostatniej sciezki odczytowej do installationseta
		UPDATE		ins
		SET			ins.LastInterfacesPath_Id = ip.Id
		FROM		@readIds AS ri
		JOIN		(	SELECT	ROW_NUMBER() OVER (PARTITION BY r1.Device_Id ORDER BY r1.[MeasurementDate] DESC) AS RowNumber,
								r1.Id AS Id
						FROM	@readIds AS rids
						JOIN	[reads].[Read] AS r1 ON (r1.Id = rids.Id)
						where	1=1
								--and r1.ReadSource_Id <> 6 --migracja
					) AS r ON (r.RowNumber = 1 AND r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)

		--wstawienie do bazy danych readDevice		
		UPDATE		rd
		SET			rd.IsReadGenerated = 1
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)

		--generowanie odczyt�w diagnostycznych
		
		INSERT INTO	[reads].[ReadDiagnostic]	(	ReadDevice_Id,
													DeviceDate,
													DeviceTime,
													ManufacturerCode,
													Hardware,
													Software,
													ErrorCode,
													BatteryVoltage,
													BatteryTimeLeft,
													BatteryWorkTime,
													DeviceTemperature,
													ExternalTemperature
												)
		SELECT		DISTINCT ip.ReadDevice_Id,
					rp1.Value,
					rp1.Value,
					rp2.Value,
					rp3.Value,
					rp4.Value,
					rp5.Value,
					rp6.Value,
					case when rp7.Value IS NULL then null else dbo.[TimeSpanStringToSeconds](rp7.Value) end,
					case when rp8.Value IS NULL then null else dbo.[TimeSpanStringToSeconds](rp8.Value) end,
					rp9.Value,
					rp10.Value
		FROM		@readIds AS ri
		JOIN		[reads].[Read] as r on (r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id and ip.[Path] is null)
		--JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadDiagnostic] AS rdm ON (rdm.ReadDevice_Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 7) -- DeviceDate
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 230) -- ManufacturerCode
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 35) -- HardwareVersion
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 34) -- SoftwareVersion
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 4) -- ErrorCode
		LEFT JOIN	[reads].[ReadParameter] AS rp6 ON (rp6.InterfacesPath_Id = ip.Id AND rp6.[ReadParameterType_Id] = 13) -- BatteryVoltage
		LEFT JOIN	[reads].[ReadParameter] AS rp7 ON (rp7.InterfacesPath_Id = ip.Id AND rp7.[ReadParameterType_Id] = 33) -- BatteryTimeLeft
		LEFT JOIN	[reads].[ReadParameter] AS rp8 ON (rp8.InterfacesPath_Id = ip.Id AND rp8.[ReadParameterType_Id] = 6) -- BatteryWorkTime
		LEFT JOIN	[reads].[ReadParameter] AS rp9 ON (rp9.InterfacesPath_Id = ip.Id AND rp9.[ReadParameterType_Id] = 20) -- Temperature
		LEFT JOIN	[reads].[ReadParameter] AS rp10 ON (rp10.InterfacesPath_Id = ip.Id AND rp10.[ReadParameterType_Id] = 22) -- ExternalTemperature
		WHERE		1=1
					--and r.ReadSource_Id <> 6 --migracja
					and rdm.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
								OR rp6.Value IS NOT NULL
								OR rp7.Value IS NOT NULL
								OR rp8.Value IS NOT NULL
								OR rp9.Value IS NOT NULL
								OR rp10.Value IS NOT NULL
							)
					

		--generowanie odczytow podzielnika ciep�a
		INSERT INTO	[reads].[ReadHeatdivisor]	(	[ReadDevice_Id],
													[Bearing],
													[AverageTemperature],
													[BillingPeriodType_Id],
													[BillingPeriodDate],
													[PreviousPeriodBearing],
													[PrevoiusPeriodAvgTemperature])
		SELECT		DISTINCT rd.Id,
					rp1.Value,
					rp2.Value,
					0,
					rp3.Value,
					rp4.Value,
					rp5.Value
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 5)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadHeatdivisor] AS rhd ON (rhd.ReadDevice_Id = ip.ReadDevice_Id AND rhd.BillingPeriodType_Id = 0)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 26 AND rp1.BillingPeriodType_Id = 0)
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 25 AND rp2.BillingPeriodType_Id = 0)
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 27 AND rp3.BillingPeriodType_Id = 0)
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 43 AND rp4.BillingPeriodType_Id = 0)
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 44 AND rp5.BillingPeriodType_Id = 0)
		WHERE		rhd.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
							)

		INSERT INTO	[reads].[ReadHeatdivisor]	(	[ReadDevice_Id],
													[Bearing],
													[AverageTemperature],
													[BillingPeriodType_Id],
													[BillingPeriodDate],
													[PreviousPeriodBearing],
													[PrevoiusPeriodAvgTemperature])
		SELECT		DISTINCT rd.Id,
					rp1.Value,
					rp2.Value,
					1,
					rp3.Value,
					rp4.Value,
					rp5.Value
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 5)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadHeatdivisor] AS rhd ON (rhd.ReadDevice_Id = ip.ReadDevice_Id AND rhd.BillingPeriodType_Id = 1)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 26 AND rp1.BillingPeriodType_Id = 1)
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 25 AND rp2.BillingPeriodType_Id = 1)
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 27 AND rp3.BillingPeriodType_Id = 1)
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 43 AND rp4.BillingPeriodType_Id = 1)
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 44 AND rp5.BillingPeriodType_Id = 1)
		WHERE		rhd.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
							)

		--ISNULL(rp3.Value, (CONVERT(decimal(18,3), rp6.Value) - CONVERT(decimal(18,3), rp4.Value)))
		--generowanie odczytow wodomierza
		INSERT INTO	[reads].[ReadWaterMeter]	(	[ReadDevice_Id],
													[Volume],
													[ReverseVolume]											
												)
		SELECT		DISTINCT rd.Id,
					ISNULL	
					(
								rp1.Value, 
								ISNULL	
								(
											CONVERT	(decimal(18,3), ISNULL	(
																				rp3.Value, 
																				(CONVERT(decimal(18,3), rp6.Value) - CONVERT(decimal(18,3), rp4.Value))
																			)
													)*CONVERT(decimal(18,3), pw.Value)/1000,
											CONVERT	(decimal(18,3), ISNULL	(
																				rp3.Value, 
																				(CONVERT(decimal(18,3), rp6.Value) - CONVERT(decimal(18,3), rp4.Value))
																			)
													)*CONVERT(decimal(18,10), case	when rp5.Value = '333.333' then '333.3333333333' 
																					when rp5.Value = '33.333' then '33.3333333333' 
																					when rp5.Value = '3.333' then '3.3333333333'
																					when rp5.Value = '0.333' then '0.3333333333'
																					when rp5.Value = '0.033' then '0.0333333333'
																					when rp5.Value = '0.003' then '0.0033333333'
																					else rp5.Value end)/1000

											
								)
					) + isnull(dc.Correction, 0),
					ISNULL	
					(
								rp2.Value, 
								ISNULL	
								(
											CONVERT(decimal(18,3), rp4.Value)*CONVERT(decimal(18,3), pw.Value)/1000,
											CONVERT(decimal(18,3), rp4.Value)*CONVERT(decimal(18,10), case  when rp5.Value = '333.333' then '333.3333333333' 
																											when rp5.Value = '33.333' then '33.3333333333' 
																											when rp5.Value = '3.333' then '3.3333333333'
																											when rp5.Value = '0.333' then '0.3333333333'
																											when rp5.Value = '0.033' then '0.0333333333'
																											when rp5.Value = '0.003' then '0.0033333333'
																											else rp5.Value end)/1000
								)
					)
		FROM		@readIds AS ri
		JOIN		[reads].[Read] AS r ON (r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = r.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 1)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[network].[DeviceConnection] AS dc ON (	dc.ChildDevice_Id = ins.SourceDevice_Id 
															AND dc.ConnectionDate <= r.MeasurementDate
															AND (dc.DisconnectionDate IS NULL OR dc.DisconnectionDate >= r.MeasurementDate))
		LEFT JOIN	[network].[InterfaceConnection] AS ic ON (ic.DeviceConnection_Id = dc.Id AND ic.ChildInterface_Id = ins.SourceOutputInterface_Id)
		LEFT JOIN	[device].[PulseWeight] AS pw ON (pw.Id = ic.PulseWeight_Id)
		LEFT JOIN	[reads].[ReadWaterMeter] AS rwm ON (rwm.ReadDevice_Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 0) -- volume
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 1) -- reversevolume
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 11) --pulses
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 12) -- reverse pulses
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 172) -- pulseweight
		LEFT JOIN	[reads].[ReadParameter] AS rp6 ON (rp6.InterfacesPath_Id = ip.Id AND rp6.[ReadParameterType_Id] = 179) -- reverse pulses
		WHERE		rwm.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
								OR rp6.Value IS NOT NULL
							)

		--generowanie odczytow gazomierza
		INSERT INTO	[reads].[ReadGasMeter]	(	[ReadDevice_Id],
												[Volume],
												[CorrectedVolume],
												[Energy],
												[VolumeTariff1],
												[VolumeTariff2]											
											)
		SELECT		DISTINCT rd.Id,
					rp1.Value,
					rp2.Value,
					rp3.Value,
					rp4.Value,
					rp5.Value
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 2)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadGasMeter] AS rgm ON (rgm.ReadDevice_Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 0 AND rp1.[Tariff] IS NULL) -- volume
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 2) -- volumecorrected
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 5) -- energy
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 0 AND rp4.[Tariff] = 1) -- volume
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 0 AND rp5.[Tariff] = 2) -- volume
		WHERE		rgm.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
							)

		--generowanie odczytow ciep�omierza
		INSERT INTO	[reads].[ReadHeatMeter]	(	[ReadDevice_Id],
												[Volume],
												[Energy],
												[SupplyTemperature],
												[ReturnTemperature],
												[Flow],
												[Power]
											)
		SELECT		DISTINCT rd.Id,
					rp1.Value,
					rp2.Value,
					rp3.Value,
					rp4.Value,
					rp5.Value,
					rp6.Value
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 3)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadHeatMeter] AS rhm ON (rhm.ReadDevice_Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 0 AND rp1.[Tariff] IS NULL) -- volume
		LEFT JOIN	[reads].[ReadParameter] AS rp2 ON (rp2.InterfacesPath_Id = ip.Id AND rp2.[ReadParameterType_Id] = 68) -- ThermalEnergy
		LEFT JOIN	[reads].[ReadParameter] AS rp3 ON (rp3.InterfacesPath_Id = ip.Id AND rp3.[ReadParameterType_Id] = 21) -- FlowTemperature
		LEFT JOIN	[reads].[ReadParameter] AS rp4 ON (rp4.InterfacesPath_Id = ip.Id AND rp4.[ReadParameterType_Id] = 23) -- ReturnTemperature
		LEFT JOIN	[reads].[ReadParameter] AS rp5 ON (rp5.InterfacesPath_Id = ip.Id AND rp5.[ReadParameterType_Id] = 17) -- Flow
		LEFT JOIN	[reads].[ReadParameter] AS rp6 ON (rp6.InterfacesPath_Id = ip.Id AND rp6.[ReadParameterType_Id] = 30) -- Power
		WHERE		rhm.Id IS NULL
					AND (		rp1.Value IS NOT NULL
								OR rp2.Value IS NOT NULL
								OR rp3.Value IS NOT NULL
								OR rp4.Value IS NOT NULL
								OR rp5.Value IS NOT NULL
								OR rp6.Value IS NOT NULL
							)

		--generowanie odczytow termometru
		INSERT INTO	[reads].[ReadTemperaturesensor]	(	[ReadDevice_Id],
														[Temperature]
													)
		SELECT		DISTINCT rd.Id,
					(0.003908/(2*0.0000005775)) - 
					SQRT((0.003908*0.003908) + 4*0.0000005775 *    (1 - ((((2*(CAST(CONVERT(decimal, rp1.Value) AS int) & 0x3FF) + 2457.6) * 1000) / ((15.3868131868132*2048.0) - (2*(CAST(CONVERT(decimal, rp1.Value) AS int) & 0x3FF) + 2457.6))) - m.Resistance) / 100)  )
					/(2*0.0000005775)
				
		FROM		@readIds AS ri
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)
		JOIN		[measurement].[Montage] AS m ON (m.Id = ins.Montage_Id)
		JOIN		[device].[Interface] AS inf ON (inf.Id = ins.SourceInputInterface_Id)
		JOIN		[device].[InterfaceType] AS inft ON (inft.Id = inf.InterfaceType_Id)
		JOIN		[device].[MeasurementInterfaceTypeProperties] AS minftp ON (minftp.InterfaceType_Id = inft.Id AND minftp.MeasurementKind_Id = 6)
		JOIN		[reads].[ReadDevice] AS rd ON (rd.Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadTemperaturesensor] AS rts ON (rts.ReadDevice_Id = ip.ReadDevice_Id)
		LEFT JOIN	[reads].[ReadParameter] AS rp1 ON (rp1.InterfacesPath_Id = ip.Id AND rp1.[ReadParameterType_Id] = 234) -- Resistance
		WHERE		rts.Id IS NULL
					AND (	rp1.Value IS NOT NULL)
					AND m.Resistance IS NOT NULL

		--aktualizacja wskazania na ostatni odczyt w instalationSet
		UPDATE		ins
		SET			ins.LastInterfacesPathWithRead_Id = ip.Id
		FROM		@readIds AS ri
		JOIN		(	SELECT		ROW_NUMBER() OVER (PARTITION BY r1.Device_Id ORDER BY r1.[MeasurementDate] DESC) AS RowNumber,
									r1.Id AS Id
						FROM		@readIds AS rids
						JOIN		[reads].[Read] AS r1 ON (r1.Id = rids.Id)
						JOIN		[reads].[InterfacesPath] AS ip1 ON (ip1.Read_Id = rids.Id)
						JOIN		[reads].[ReadDevice] AS rd1 ON  (rd1.Id = ip1.ReadDevice_Id)
						LEFT JOIN	[reads].[ReadGasmeter] AS r3 ON (r3.ReadDevice_Id = rd1.Id)
						LEFT JOIN	[reads].[ReadHeatdivisor] AS r4 ON (r4.ReadDevice_Id = rd1.Id)
						LEFT JOIN	[reads].[ReadHeatmeter] AS r5 ON (r5.ReadDevice_Id = rd1.Id)
						LEFT JOIN	[reads].[ReadTemperaturesensor] AS r6 ON (r6.ReadDevice_Id = rd1.Id)
						LEFT JOIN	[reads].[ReadWatermeter] AS r7 ON (r7.ReadDevice_Id = rd1.Id)
						WHERE		1=1
									--and r1.ReadSource_Id <> 6 --migracja
									and (	r3.Id IS NOT NULL OR
											r4.Id IS NOT NULL OR
											r5.Id IS NOT NULL OR
											r6.Id IS NOT NULL OR
											r7.Id IS NOT NULL)
					) AS r ON (r.RowNumber = 1 AND r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)

		--aktualizacja wskazania na ostatne zdarzenie w instalationSet
		UPDATE		ins
		SET			ins.LastInterfacesPathWithEvent_Id = ip.Id
		FROM		@readIds AS ri
		JOIN		(	SELECT		ROW_NUMBER() OVER (PARTITION BY r1.Device_Id ORDER BY r1.[MeasurementDate] DESC) AS RowNumber,
									r1.Id AS Id
						FROM		@readIds AS rids
						JOIN		[reads].[Read] AS r1 ON (r1.Id = rids.Id)
						JOIN		[reads].[InterfacesPath] AS ip1 ON (ip1.Read_Id = rids.Id)
						JOIN		[reads].[ReadEvent] AS re ON  (re.InterfacesPath_Id = ip1.Id)
						WHERE		1=1
									--and r1.ReadSource_Id <> 6 --migracja
					) AS r ON (r.RowNumber = 1 AND r.Id = ri.Id)
		JOIN		[reads].[InterfacesPath] AS ip ON (ip.Read_Id = ri.Id)
		JOIN		[measurement].[InstallationSet] AS ins ON (ins.Id = ip.InstallationSet_Id)

		--aktualizacja wskazania na ostatni instalationset z odczytem w terminal
		update	t 
		set		t.LastInstallationSetWithReads_Id = ins.Id
		from	measurement.Terminal as t
		join	(	
					select			ROW_NUMBER() over (partition by ins.Terminal_Id order by ins.DateFrom desc) as RowNumber,
									ins.Id,
									ins.DestinationDevice_Id,
									ins.Terminal_Id,
									ins.LastInterfacesPathWithRead_Id
					from			measurement.InstallationSet as ins
					join			reads.[Read] as r on r.Device_Id = ins.DestinationDevice_Id
					join			@readIds as rids on rids.Id = r.Id
					WHERE			1=1
									--and r.ReadSource_Id <> 6 --migracja
									and ins.LastInterfacesPathWithRead_Id is not null
				)  as ins on (ins.RowNumber = 1 and ins.Terminal_Id = t.Id)

		IF (@InstallationSetId IS NULL)
		BEGIN
			SELECT	@saReadId = max(Id)
			FROM	@readIds

			IF (@saReadId IS NOT NULL)
			BEGIN

				UPDATE	[system].[SystemSetting]
				SET		Value = convert(varchar,@saReadId),
						ModifyDate = SYSDATETIMEOFFSET()
				WHERE	[SystemSettingType_Id] = 32

				IF (@@ROWCOUNT = 0)
				BEGIN
					INSERT INTO	[system].[SystemSetting]
								(
									[Value],
									[ModifyDate],
									[SystemSettingType_Id]
								)
								VALUES
								(
									convert(varchar,@saReadId),
									SYSDATETIMEOFFSET(),
									32
								)
				END
			END
		END

		SELECT @ReadsCalculatedCount = count(Id) FROM @readIds

		--------------------------------------------------------------------------------------------------------
		-- COMMIT TRANSACTION
		--------------------------------------------------------------------------------------------------------
		IF @TranStarted = 1 AND @@TRANCOUNT > 0
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @Rollbacked BIT = 0
		IF @TranStarted = 1 AND @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
			SET @Rollbacked = 1
		END

		DECLARE @ErrorState INT = ERROR_STATE();
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorCode INT = ERROR_NUMBER()

		IF @ErrorCode = 1205 OR @ErrorMessage = '-555555'
			IF @Rollbacked = 1
			BEGIN
				SET @Retry = 1
				DECLARE @ProcName VARCHAR(128) = OBJECT_NAME(@@PROCID)
			EXEC [system].WaitForDeadlock @ProcName
			END ELSE
				RAISERROR('-555555', @ErrorSeverity, @ErrorState)
		ELSE IF @ErrorState = 100
			SET @ResultCode = CAST(@ErrorMessage AS INT)
		ELSE
		BEGIN
			SET @ResultCode = ERROR_NUMBER()
			RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
		END
	END CATCH
	RETURN @ResultCode
END
GO

ALTER PROCEDURE [reads].[uspReadAddWithDeviceId]
	@DeviceId					INT,
    @TransmissionId				UNIQUEIDENTIFIER,
	@ReadSessionId				INT,
	@SystemDate					DATETIMEOFFSET(0),
    @MeasurementDate			DATETIMEOFFSET(0),
    @IsLogged					BIT,
	@ReadSourceId				INT,
	@ReadParameters				reads.[ReadParameterTypeType] READONLY,
	@ReadEvents					reads.ReadEventType READONLY,
	@Transmissions				dbo.TransmissionType READONLY,
	@TransmissionSections		dbo.TransmissionSectionType READONLY,
	@TransmissionGsmSections	dbo.TransmissionSectionGsmType READONLY,
	@TransmissionRadioSections	dbo.TransmissionSectionRadioType READONLY,
	@TransmissionTcpSections	dbo.TransmissionSectionTcpType READONLY,
	@TransmissionNetworkId		INT,
	@TransmissionNetworkStage	INT,
	@ReadId						INT OUTPUT,
	@ResultCode					INT OUTPUT
AS
BEGIN
	DECLARE @Retry BIT = 1
	SET NOCOUNT ON
	SET @ResultCode = 0
	SET @ReadId = 0
	DECLARE @temp NVARCHAR(MAX)
	DECLARE @Rows dbo.IdTableType

	  WHILE @Retry = 1
BEGIN TRY
	SET @Retry = 0
		
		--------------------------------------------------------------------------------------------------------
		-- Sprawdzenie poprawności danych
		--------------------------------------------------------------------------------------------------------
		--IF NOT EXISTS(SELECT TOP 1 1 FROM reads.Transmission WHERE Id=@TransmissionId)								RAISERROR('-100389', 18, 100) -- Transmisja o podanym identyfikatorze nie istnieje
		IF NOT EXISTS(SELECT TOP 1 1 FROM reads.ReadSource WHERE Id=@ReadSourceId)									    RAISERROR('-100391', 18, 100) -- Źródło odczytu o podanym identyfikatorze nie istnieje
		--IF NOT EXISTS(SELECT TOP 1 1 FROM @ReadParameters) AND NOT EXISTS(SELECT TOP 1 1 FROM @ReadEvents)			RAISERROR('-100392', 18, 100) -- Nie określono żadnego parametru odczytu
		IF EXISTS(SELECT TOP 1 1 FROM @ReadParameters) 
			AND EXISTS(SELECT TOP 1 1 
					FROM @ReadParameters rp 
					LEFT JOIN reads.ReadParameterType rpt ON rp.ReadParameterTypeId = rpt.Id 
					WHERE rpt.Id IS NULL)																			RAISERROR('-100393', 18, 100) -- Nieprawidłowy parameter odczytu

		IF (@DeviceId IS NULL)																						RAISERROR('-100390', 18, 100) -- Urządzenie o podanym identyfikatorze nie istnieje
		
		
		--------------------------------------------------------------------------------------------------------
		-- BEGIN TRANSACTION
		--------------------------------------------------------------------------------------------------------
		DECLARE @TranStarted   bit
		SET @TranStarted = 0

		IF( @@TRANCOUNT = 0 )
		BEGIN
			BEGIN TRANSACTION
			SET @TranStarted = 1
		END
		ELSE
			SET @TranStarted = 0
		
		--------------------------------------------------------------------------------------------------------
		-- Zapisywanie informacji o stanie odczytywanej sieci transmisyjnej
		--------------------------------------------------------------------------------------------------------
		IF	@TransmissionNetworkId IS NOT NULL
		BEGIN
			--aktualizacja etapu w sesji odczytowej
			IF (@ReadSessionId IS NULL) RAISERROR('-6000002', 18, 100)
			
			UPDATE		[reads].[ReadSession]
			SET			[Stage] = @TransmissionNetworkStage
			WHERE		[Id] = @ReadSessionId

			IF (	@TransmissionNetworkStage IS NULL OR
					@TransmissionNetworkStage % 10 = 0)
			BEGIN
				--oznaczenie zmiany w sieci transmisyjnej
				DECLARE @XMLDataChangedBeforeUpdate XML

				DELETE FROM @Rows
				INSERT INTO @Rows(Id) 
				SELECT			DISTINCT Id 
				FROM			[network].[TransmissionNetwork]
				WHERE			[Id] = @TransmissionNetworkId

				EXEC [dbo].[GetDataChangedBeforeUpdate]
											@TableName					= N'network.TransmissionNetwork',
											@RowIds						= @Rows,
											@DataChangedBeforeUpdate	= @XMLDataChangedBeforeUpdate OUTPUT 

				UPDATE	[network].[TransmissionNetwork]
				SET		[LastReadSessionStage] = @TransmissionNetworkStage
				WHERE	[Id] = @TransmissionNetworkId

				EXEC [dbo].[OnDataChangedUpdate]
								@TableName					= N'network.TransmissionNetwork',
								@RowIds						= @Rows,
								@DataChangedBeforeUpdate	= @XMLDataChangedBeforeUpdate,
								@User_Id					= null	
			END

		END


		--------------------------------------------------------------------------------------------------------
		-- Wykonanie operacji wstawienia danych o transmisjach
		--------------------------------------------------------------------------------------------------------
		EXEC [reads].[uspTransmissionsAdd]	@ReadSessionId				= @ReadSessionId,
											@Transmissions				= @Transmissions,
											@TransmissionSections		= @TransmissionSections,
											@TransmissionGsmSections	= @TransmissionGsmSections,
											@TransmissionRadioSections	= @TransmissionRadioSections,
											@TransmissionTcpSections	= @TransmissionTcpSections,
											@ResultCode					= @ResultCode OUTPUT
		-----------------------------------------------------------------------------------
		--              zapisywanie odczytu
		-----------------------------------------------------------------------------------
		EXEC [reads].[uspTransmissionIdGet]	@ReadSessionId				= @ReadSessionId,
											@SourceDeviceId				= @DeviceId,
											@TargetDeviceId				= NULL,
											@TransmissionId				= @TransmissionId OUTPUT,
											@ResultCode					= @ResultCode OUTPUT
		
		IF (EXISTS(SELECT TOP 1 1 FROM @ReadParameters) OR
			EXISTS(SELECT TOP 1 1 FROM @ReadEvents))
		BEGIN
			DELETE FROM @Rows
			INSERT INTO [reads].[Read] (
										   [Transmission_Id]
										  ,[Device_Id]
										  ,[SystemDate]
										  ,[MeasurementDate]
										  ,[IsLogged]
										  ,[ReadSource_Id]
			)
			OUTPUT inserted.Id INTO @Rows
			VALUES(
										   @TransmissionId
										  ,@DeviceId
										  ,@SystemDate
										  ,@MeasurementDate
										  ,@IsLogged
										  ,@ReadSourceId
			) 
			SELECT TOP 1 @ReadId=Id FROM @Rows

			UPDATE	[device].[Device]
			SET		[HasReads] = 1
			WHERE	[Id] = @DeviceId

			-----------------------------------------------------------------------------------
			--              zapisywanie ostatniego odczytu
			-----------------------------------------------------------------------------------
			DECLARE @lastReadId INT
			EXEC	[reads].[uspLastReadAdd]	@ReadId		= @ReadId,
												@DeviceId	= @DeviceId,
												@LastReadId	= @lastReadId OUTPUT,
												@ResultCode	= @ResultCode OUTPUT
											
			-----------------------------------------------------------------------------------
			--              zapisywanie sciezek odczytowych wraz z parametrami i zdarzeniami
			-----------------------------------------------------------------------------------
			DECLARE @ReadPath		NVARCHAR(MAX)
			DECLARE @ReadPaths		TABLE (	[Path]	NVARCHAR(MAX))

			INSERT INTO	@ReadPaths	(
										[Path]
									)
									SELECT DISTINCT [ReadPath] FROM @ReadParameters
									UNION  ALL
									SELECT DISTINCT[ReadPath] FROM @ReadEvents

			DECLARE			readPathCursor CURSOR local
			FOR 
			SELECT DISTINCT	[Path]
			FROM			@ReadPaths
	
			OPEN readPathCursor
			FETCH NEXT FROM readPathCursor
			INTO			@ReadPath

			WHILE @@FETCH_STATUS = 0
			BEGIN
				--sprawdzenie, czy nie powtarza się odczyt z tego samego urzadznie, sciezki i daty
				IF EXISTS(	SELECT TOP 1	1	
							FROM			[reads].[Read] r 
							JOIN			[reads].[InterfacesPath] as rp ON (rp.[Read_Id] = r.[Id])
							WHERE			r.IsLogged = @IsLogged
											AND r.Device_Id = @DeviceId
											AND (rp.[Path] = @ReadPath OR (@ReadPath IS NULL AND rp.[Path] IS NULL))
											AND r.MeasurementDate = @MeasurementDate)										
				BEGIN
							IF (@ReadSourceId <> 1)		-- Istnieje już odczyt z tego źródła z tej daty
							BEGIN
								RAISERROR('-100394', 18, 100) 
							END
						
							FETCH NEXT FROM readPathCursor
							INTO			@ReadPath
							CONTINUE
				END

				-- wstawienie sciezki odczytowej
				DELETE FROM @Rows
				DECLARE @InterfacesPathId INT
				INSERT INTO [reads].[InterfacesPath]	(	[Read_Id],
															[Path]
														)
														OUTPUT inserted.Id INTO @Rows
														VALUES
														(
															@ReadId,
															@ReadPath
														)
				SELECT TOP 1 @InterfacesPathId=Id FROM @Rows
				DELETE FROM @Rows

				DECLARE @ReadDeviceId INT
				INSERT INTO [reads].[ReadDevice]		(	[Read_Id],
															[IsReadGenerated]
														)
														OUTPUT inserted.Id INTO @Rows
														VALUES
														(
															@ReadId,
															0
														)
				SELECT TOP 1 @ReadDeviceId=Id FROM @Rows
				
				UPDATE [reads].[InterfacesPath] SET [ReadDevice_Id] = @ReadDeviceId WHERE Id = @InterfacesPathId
		
				--zapis odczytu parametrow
				INSERT INTO	reads.ReadParameter	(	[InterfacesPath_Id], 
													[ReadParameterType_Id], 
													[Value],
													[Tariff],
													[BillingPeriodType_Id]
												)
												SELECT	@InterfacesPathId, 
														[ReadParameterTypeId], 
														[Value],
														[Tariff],
														[BillingPeriodTypeId]
												FROM	@ReadParameters
												WHERE	((@ReadPath IS NOT NULL AND [ReadPath] IS NOT NULL AND [ReadPath] = @ReadPath) OR (@ReadPath IS NULL AND [ReadPath] IS NULL))

				-- zapis odczytu zharzen
				INSERT INTO reads.ReadEvent(	[InterfacesPath_Id],
												[EventType_Id],
												[FirstOccurance],
												[LastOccurence],
												[Count],
												[Duration],
												[Volume],
												[HasEverOccured],
												[IsCurrent],
												[IsStored],
												[IsAnalyzed]
											)
											SELECT	@InterfacesPathId,
													CASE	WHEN [ApplicationEventCode] IS NOT NULL AND EXISTS (SELECT TOP 1 1 FROM [event].[EventType] WHERE [Id] = [ApplicationEventCode])
															THEN [ApplicationEventCode]
															ELSE [EventTypeId]
															END,
													[FirstOccurance],
													[LastOccurence],
													[Count],
													[Duration],
													[Volume],
													[HasEverOccured],
													[IsCurrent],
													[IsStored],
													[IsAnalyzed]
											FROM	@ReadEvents
											WHERE	([ReadPath] = @ReadPath OR (@ReadPath IS NULL AND [ReadPath] IS NULL))

				FETCH NEXT FROM readPathCursor
				INTO			@ReadPath
			END

			CLOSE readPathCursor
			DEALLOCATE readPathCursor

			--usunięcie odczytu jesli nie ma do niego readPath
			--IF NOT EXISTS(SELECT TOP 1 1 FROM [reads].[InterfacesPath] WHERE [Read_Id] = @ReadId)
			--BEGIN
			--	DELETE FROM	[reads].[Read]
			--	WHERE		[Id] = @ReadId
			--END	
		END
	
		--------------------------------------------------------------------------------------------------------
		-- COMMIT TRANSACTION
		--------------------------------------------------------------------------------------------------------
		IF @TranStarted = 1 AND @@TRANCOUNT > 0
			COMMIT TRANSACTION

END TRY
BEGIN CATCH
	DECLARE @Rollbacked BIT = 0
	IF @TranStarted = 1 AND @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION
		SET @Rollbacked = 1
	END

	DECLARE @ErrorState INT = ERROR_STATE();
	DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
	DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
	DECLARE @ErrorCode INT = ERROR_NUMBER()

	IF @ErrorCode = 1205 OR @ErrorMessage = '-555555'
		IF @Rollbacked = 1
		BEGIN
			SET @Retry = 1
			DECLARE @ProcName VARCHAR(128) = OBJECT_NAME(@@PROCID)
			EXEC [system].WaitForDeadlock @ProcName
		END 
--		ELSE
--			RAISERROR('-555555', @ErrorSeverity, @ErrorState)
	ELSE IF @ErrorState = 100
		SET @ResultCode = CAST(@ErrorMessage AS INT)
	ELSE
	BEGIN
		SET @ResultCode = ERROR_NUMBER()
	--	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
	END
END CATCH
RETURN @ResultCode
END
GO

/***********************************************************************
                TABELA LOGÓW
***********************************************************************/
IF OBJECT_ID('dbo.MigrationLog', 'U') IS NOT NULL 
	DROP TABLE dbo.MigrationLog
GO


CREATE TABLE dbo.MigrationLog(
	Id int IDENTITY(1,1) NOT NULL,
	LogDate datetime,
	Perc decimal (10,3),
	DeviceIndex integer,
	DeviceNumber integer,
	ReadNumber integer,
	DeviceId1 integer, 
	DeviceTypeName1 varchar(max), 
	DeviceTypeId1 integer, 
	DeviceConnectionId1 varchar(max), 
	DeviceTypeName2 varchar(max),
	DeviceTypeId2 integer, 
	DeviceConnectionId2  varchar(max),
	Logg varchar(max)
CONSTRAINT [PK_MigrationLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/***********************************************************************
                /TABELA LOGÓW
***********************************************************************/


/***********************************************************************
                DANE WEJŚCIOWE
***********************************************************************/
declare @minDate datetime = '2014-05-01 00:00:00',
		@maxDate datetime = '2019-01-01 00:00:00',
		@lastDeviceId integer = 0,
		@specyficDeviceId1 integer = null,
		@deviceTypeIds1 dbo.IdTableType
		
declare @insertReadToMetis2 bit = 1,
		@deviceLimit integer = 100000000

declare @logDeviceInterval integer = 10,
		@readRangeLogged integer = 1,
		@readRangeInstantLastOnDay integer = 1,
		@readRangeInstant integer = 0

/* typy urządzeń z bazy Metis1 */
insert into @deviceTypeIds1(Id) values
(2),(3),(30),(4),(5),(6),(7),(29),(42),(8),(40),(39),(12) --nakładki
,(16),(41) --podzielniki ciepła
,(21),(22),(23) --gazomierze
,(20),(27),(32),(26),(24),(25),(28),(31) --ciepłomierze
,(37) --termometr

select	@minDate as minDate, 
		@maxDate as maxDate, 
		@lastDeviceId as lastDeviceId, 
		@specyficDeviceId1 as specyficDeviceId1, 
		@insertReadToMetis2 as insertReadToMetis2,  
		@deviceLimit as deviceLimit
/***********************************************************************
                /DANE WEJŚCIOWE
***********************************************************************/




/***********************************************************************
                MAPOWANIE TYPÓW
***********************************************************************/
declare @deviceTypeMapping table(name1 varchar(max), name2 varchar(max))

insert into @deviceTypeMapping(name1, name2) 
values		('AT-WMBUS-MR-09', 'AT-WMBUS-MR-09z'), 
			('CN-R-868', 'CN-R-868-3-1'), 
			('AT-WMBUS-MR-07', 'AT-WMBUS-MR-07z'),
			('AT-WMBUS-MR-05', 'AT-WMBUS-MR-05z'),
			('AT-WMBUS-MR-04', 'AT-WMBUS-MR-04z'),
			('AT-WMBUS-MR-03', 'AT-WMBUS-MR-03z'),
			('AT-WMBUS-MR-06', 'AT-WMBUS-MR-06z'),
			('E-ITN-305', 'E-ITN-30.5'),	
			('AT-WMBUS-12', '---'),		
			('AT-K-GSMRS232', 'AT-K-GSMRS232th'),		
			('Uniflo_GxS', 'UnifloGxS'),	
			('AT-WMBUS-05', 'AT-WMBUS-05-1'),		
			('INVENTIA-MT-703', '---'),
			('AT-WMBUS-06', 'AT-WMBUS-06th'),
			('Uniflo_GxE', 'UnifloGxE'),
			('Unismart', '---'),
			('AT-WMBUS-01', 'AT-WMBUS-01'),
			('AT-WMBUS-04', 'AT-WMBUS-04'),
			('AT-WMBUS-06-1', 'AT-WMBUS-06-1'),
			('AT-WMBUS-07', 'AT-WMBUS-07'),
			('AT-WMBUS-08', 'AT-WMBUS-08'),
			('AT-WMBUS-09', 'AT-WMBUS-09'),
			('AT-WMBUS-10', 'AT-WMBUS-10'),
			('AT-WMBUS-11', 'AT-WMBUS-11'),
			('AT-UPT-GSM-01', 'AT-UPT-GSM-01'),
			--('AT-K-ETHRS232', 'AT-K-ETHRS232'),
			('AT-AQUA-868-02', 'AT-AQUA-868-02'),
			('AT-AQUA-868-B-01', 'AT-AQUA-868-B-01'),
			('AT-AQUA-868-R-01', 'AT-AQUA-868-R-01'),
			('AT-WMBUS-MR-01', 'AT-WMBUS-MR-01'),
			('AT-WMBUS-MR-02', 'AT-WMBUS-MR-02'),
			('CN-R-433-1-1', 'CN-R-433-1-1'),
			('CN-RE-5', 'CN-RE-5'),
			('Neo3', 'Neo3'),
			('AT-UPT-GSM-01RS', 'AT-UPT-GSM-01rs'),
			('AT-K-ETHRS232', 'AT-K-ETHRS232th')

/* mapowanie typów do powyższej tabeli tymczasowej */
/*
	select		dt1.Name, dt2.Name--, dtm1.name1, dtm1.name2, concat('(''',dt1.Name, ''', ''', dt2.Name, '''),')--, dtm2.name1, dtm2.name2, * 
	from		Metis.dictionaries.DeviceType as dt1
	left join	@deviceTypeMapping as dtm1 on dtm1.name1 = dt1.Name
	left join	Metis2.device.DeviceType as dt2 on dt2.Name = dtm1.name2

	--left join	@deviceTypeMapping as dtm2 on dtm2.name1 = dt1.Name and dtm2.name2 = dt2.Name
	--where		dt2.Name is null and dtm1.name2 is null

	/*
	select		* from		Metis2.device.DeviceType where name like '%smart%'
	*/
*/
/***********************************************************************
                /MAPOWANIE TYPÓW
***********************************************************************/


/***********************************************************************
                POBRANIE URZADZEŃ Z METIS1
***********************************************************************/
/*
declare @m1Devices table(id1 integer, deviceType1 nvarchar(max), deviceNumber1 nvarchar(max), id2 integer, deviceType2 nvarchar(max), deviceNumber2 nvarchar(max))
insert into @m1Devices(id1, deviceType1, deviceNumber1, id2, deviceType2, deviceNumber2)
select		top 1000 d1.Id, dt1.Name, d1.DeviceNumber, d2.Id, dt2.Name, d2.ConnectionId
from		Metis.common.Device as d1
join		Metis.dictionaries.DeviceType as dt1 on dt1.Id = d1.DeviceType_Id
join		@deviceTypeMapping as dtm1 on dtm1.name1 = dt1.Name
join		Metis2.device.DeviceType as dt2 on dt2.Name = dtm1.name2
join		Metis2.device.Device as d2 on d2.DeviceType_Id = dt2.Id and (d2.ConnectionId = d1.DeviceNumber or (dt1.Name = 'Uniflo_GxS' and d2.ConnectionId = SUBSTRING(d1.DeviceNumber, PATINDEX('%[^_]%', d1.DeviceNumber), LEN(d1.DeviceNumber))))
*/

declare @m1Devices table(id1 integer, deviceType1 nvarchar(max), deviceTypeId1 integer, deviceNumber1 nvarchar(max), deviceType2 nvarchar(max), deviceTypeId2 integer)
insert into @m1Devices(id1, deviceType1, deviceTypeId1, deviceNumber1, deviceType2, deviceTypeId2)
select		top(@deviceLimit)
			d1.Id, 
			dt1.Name, 
			dt1.Id,
			case when dt1.Name = 'Uniflo_GxS' then SUBSTRING(d1.DeviceNumber, PATINDEX('%[^0]%', d1.DeviceNumber), LEN(d1.DeviceNumber)) else d1.DeviceNumber end as DeviceNumber,
			dtm.name2,
			dt2.Id
from		Metis.common.Device as d1
join		Metis.dictionaries.DeviceType as dt1 on dt1.Id = d1.DeviceType_Id
join		@deviceTypeMapping as dtm on dtm.name1 = dt1.Name
join		device.DeviceType as dt2 on dt2.Name = dtm.name2
where		1=1
			and dt1.Id in (select Id from @deviceTypeIds1)
			and d1.Id > @lastDeviceId
			and (@specyficDeviceId1 is null or (@specyficDeviceId1 is not null and d1.Id = @specyficDeviceId1))
order by	d1.Id
--select * from @m1Devices-- where deviceType1 like '%Uniflo%'--where id2 is null
--select * from device.DeviceType where Name like '%AT-K-ETHRS232%'
/***********************************************************************
                /POBRANIE URZADZEŃ Z METIS1
***********************************************************************/



/***********************************************************************
                ITERACJA PRZEZ URZĄDZENIA METIS1
***********************************************************************/
declare @deviceNumber integer
select	@deviceNumber = count(id1)
from	@m1Devices

declare @deviceId1 integer,
		@deviceTypeName1 nvarchar(max),
		@deviceTypeId1 integer,
		@deviceConnectionId1 nvarchar(max),
		@deviceTypeName2 nvarchar(max),
		@deviceTypeId2 integer,
		@deviceConnectionId2 nvarchar(max)

declare @readNumber integer = 0;
declare @lastReadDate datetime = null,
		@firstReadDate datetime = null
declare @deviceIndex integer = 0

--select id1, deviceType1, deviceTypeId1, deviceNumber1, deviceTypeId2, deviceType2, deviceNumber1 from @m1Devices order by id1 asc

declare cur cursor local for select id1, deviceType1, deviceTypeId1, deviceNumber1, deviceType2, deviceTypeId2, deviceNumber1 from @m1Devices order by id1 asc
open cur 

fetch next from cur into @deviceId1, @deviceTypeName1, @deviceTypeId1, @deviceConnectionId1, @deviceTypeName2, @deviceTypeId2, @deviceConnectionId2
while (@@FETCH_STATUS = 0)
begin
	
	set @readNumber = 0;

	/***********************************************************************
            pobranie odczytów do analizy
    ***********************************************************************/
	select		r.*, case rk.Id when 4 then convert(bit,1) else convert(bit,0) end as IsLogged into #tmpRead
	from		Metis.common.[Read] as r
	join		Metis.dictionaries.ReadType as rt on rt.Id = r.ReadType_Id
	join		Metis.dictionaries.ReadKind as rk on rk.Id = rt.ReadKind_Id
	where		(@readRangeLogged=1 or @readRangeInstant = 1)-- all
				and ((@readRangeLogged = 1 and rt.ReadKind_Id = 4) or (@readRangeInstant = 1 and rt.ReadKind_Id <> 4))
				and rt.Name not like '%diagnostic%' and rt.Name not like '%error%'
				and r.Device_Id = @deviceId1
				and r.MeterDate >= @minDate
				and r.MeterDate < @maxDate
	union
	select		r.*, case rk.Id when 4 then convert(bit,1) else convert(bit,0) end as IsLogged
	from		Metis.common.[Read] as r
	join		(
					select		r.Id,
								ROW_NUMBER() over (partition by r.Device_Id, datepart(year, r.MeterDate), datepart(month, r.MeterDate), datepart(day, r.MeterDate) order by r.MeterDate desc) as rownumber
					from		Metis.common.[Read] as r
					join		Metis.dictionaries.ReadType as rt on rt.Id = r.ReadType_Id
					join		Metis.dictionaries.ReadKind as rk on rk.Id = rt.ReadKind_Id
					where		1=1
								and rt.ReadKind_Id <> 4
								and rt.Name not like '%diagnostic%' and rt.Name not like '%error%'
								and r.Device_Id = @deviceId1
								and r.MeterDate >= @minDate
								and r.MeterDate < @maxDate
				) as ri on ri.rownumber = 1 and r.Id = ri.Id
	join		Metis.dictionaries.ReadType as rt on rt.Id = r.ReadType_Id
	join		Metis.dictionaries.ReadKind as rk on rk.Id = rt.ReadKind_Id
	where		@readRangeInstantLastOnDay=1 --odczyty chwilowe z ostaniego dnia


	--select * from #tmpRead
	--return

	if not exists(select top 1 1 from #tmpRead)
	begin
		drop table #tmpRead;
		fetch next from cur into @deviceId1, @deviceTypeName1, @deviceTypeId1, @deviceConnectionId1, @deviceTypeName2, @deviceTypeId2, @deviceConnectionId2
		continue
	end

	select		@readNumber = count(r.Id),
				@firstReadDate = min(r.MeterDate),
				@lastReadDate = max(r.MeterDate)
	from		#tmpRead as r
		

	declare	@lastReadId integer = 0
	while (1=1)
	begin
		--pobranie jednego odczytu ze znalezionych
		select	top 1 * into #tmpReadSingle
		from	#tmpRead 
		where	Id > @lastReadId
		order by Id asc

		--jeśli nie ma już odczytu do dodania przerywamy
		if not exists( select top 1 1 from #tmpReadSingle)
		begin
			drop table #tmpReadSingle
			break
		end

		--pobieramy id ostatnio dodanego odczytu
		select	top 1 @lastReadId = Id
		from	#tmpReadSingle
			
		--przetwarzamy odczyt


		/* read parameters */
		declare @readParameters [reads].[ReadParameterTypeType]; delete from @readParameters;

		/************************************/
		/*              wodomierze          */
		/************************************/
		if @deviceTypeId1 in (2,3,30,4,5,6,7,29,42,8,40,39,12) --wodomierze
		begin
			insert into @readParameters
			(
				[ReadPath],
				[ReadParameterTypeId],
				[Value],
				[Tariff],
				[BillingPeriodTypeId]
			)
			select	case r.DeviceInterface  when 1 then '1.1'
											when 2 then '2.1'
											when 3 then '3.1' end,
					172,convert(varchar(max), r.oid1),null,null from	#tmpReadSingle as r	where	r.oid1 is not null
			union
			select	case r.DeviceInterface  when 1 then '1.1'
											when 2 then '2.1'
											when 3 then '3.1' end,
					11,convert(varchar(max), r.oid2),null,null from	#tmpReadSingle as r	where	r.oid2 is not null
			union
			select	case r.DeviceInterface  when 1 then '1.1'
											when 2 then '2.1'
											when 3 then '3.1' end,
					12,convert(varchar(max), r.oid8),null,null from	#tmpReadSingle as r	where	r.oid8 is not null
		end
		/************************************/
		/*              gazomierze          */
		/************************************/
		if @deviceTypeId1 in (21,22,23) --gazomierze
		begin

			insert into @readParameters
			(
				[ReadPath],
				[ReadParameterTypeId],
				[Value],
				[Tariff],
				[BillingPeriodTypeId]
			)
			select	'1',5,convert(varchar(max), r.oid3),null,null from	#tmpReadSingle as r	where	r.oid3 is not null
			union
			select	'1',0,convert(varchar(max), r.oid11),null,null from	#tmpReadSingle as r	where	r.oid11 is not null
			union
			select	'1',0,convert(varchar(max), r.oid12),1,null from	#tmpReadSingle as r	where	r.oid12 is not null
			union
			select	'1',0,convert(varchar(max), r.oid13),2,null from	#tmpReadSingle as r	where	r.oid13 is not null
			union
			select	'1',2,convert(varchar(max), r.oid14),null,null from	#tmpReadSingle as r	where	r.oid14 is not null
		end
		/************************************/
		/*           termometr				*/
		/************************************/
		if @deviceTypeId1 in (37) --termometr
		begin

			insert into @readParameters
			(
				[ReadPath],
				[ReadParameterTypeId],
				[Value],
				[Tariff],
				[BillingPeriodTypeId]
			)
			select	case r.DeviceInterface  when 1 then '1'
											when 2 then '2'
											when 3 then '3'
											when 4 then '4' end,
					234,convert(varchar(max), r.oid1),null,null from	#tmpReadSingle as r	where	r.oid1 is not null
		end
		/************************************/
		/*            ciepłomierze          */
		/************************************/
		if @deviceTypeId1 in (20,27,32,26,24,25,28,31) --ciepłomierze
		begin

			insert into @readParameters
			(
				[ReadPath],
				[ReadParameterTypeId],
				[Value],
				[Tariff],
				[BillingPeriodTypeId]
			)
			select	'1.1',30,convert(varchar(max), r.oid1),null,null from	#tmpReadSingle as r	where	r.oid1 is not null
			union
			select	'1.1',68,convert(varchar(max), r.oid3),null,null from	#tmpReadSingle as r	where	r.oid3 is not null
			union
			select	'1',4,convert(varchar(max), r.oid4),1,null from	#tmpReadSingle as r	where	r.oid4 is not null
			union
			select	'1',22,convert(varchar(max), r.oid5),2,null from	#tmpReadSingle as r	where	r.oid5 is not null
			union
			select	null,6,convert(varchar(max), r.oid8),null,null from	#tmpReadSingle as r	where	r.oid8 is not null
			union
			select	'1.1',23,convert(varchar(max), r.oid10),null,null from	#tmpReadSingle as r	where	r.oid10 is not null
			union
			select	'1.1',18,convert(varchar(max), r.oid11),null,null from	#tmpReadSingle as r	where	r.oid11 is not null
			union
			select	'1.1',21,convert(varchar(max), r.oid13),null,null from	#tmpReadSingle as r	where	r.oid13 is not null
			union
			select	'1.1',31,convert(varchar(max), r.oid14),null,null from	#tmpReadSingle as r	where	r.oid14 is not null
			union
			select	'1.1',20,convert(varchar(max), r.oid16),null,null from	#tmpReadSingle as r	where	r.oid16 is not null
			union
			select	'1.1',0,convert(varchar(max), r.oid17),null,null from	#tmpReadSingle as r	where	r.oid17 is not null
			union
			select	'1.2.1',0,convert(varchar(max), r.oid18),null,null from	#tmpReadSingle as r	where	r.oid18 is not null
			union
			select	'1.3.1',0,convert(varchar(max), r.oid19),null,null from	#tmpReadSingle as r	where	r.oid19 is not null
			union
			select	'1.1',17,convert(varchar(max), r.oid20),null,null from	#tmpReadSingle as r	where	r.oid20 is not null
			union
			select	'1',42,convert(varchar(max), r.oid21),null,null from	#tmpReadSingle as r	where	r.oid21 is not null
			union
			select	'1.1',29,convert(varchar(max), r.oid22),null,null from	#tmpReadSingle as r	where	r.oid22 is not null
			union
			select	'1.1',28,convert(varchar(max), r.oid23),null,null from	#tmpReadSingle as r	where	r.oid23 is not null
		end
		/************************************/
		/*              podzielniki          */
		/************************************/
		if @deviceTypeId1 in (16,41) --podzielniki ciepła
		begin
			insert into @readParameters
			(
				[ReadPath],
				[ReadParameterTypeId],
				[Value],
				[Tariff],
				[BillingPeriodTypeId]
			)
			select '1', 26,convert(varchar(max), r.oid3),null,0 from	#tmpReadSingle as r	where	r.oid3 is not null and r.Islogged = 1
			union
			select '1', 25,convert(varchar(max), r.oid6),null,0 from	#tmpReadSingle as r	where	r.oid6 is not null and r.Islogged = 1
			union
			
			select '1', 43,convert(varchar(max), r.oid3),null,0 from	#tmpReadSingle as r	where	r.oid3 is not null and r.Islogged = 0
			union
			select '1', 26,convert(varchar(max), r.oid4),null,0 from	#tmpReadSingle as r	where	r.oid4 is not null and r.Islogged = 0
			union
			select '1', 44,convert(varchar(max), r.oid6),null,0 from	#tmpReadSingle as r	where	r.oid6 is not null and r.Islogged = 0
			union
			select '1', 25,convert(varchar(max), r.oid8),null,1 from	#tmpReadSingle as r	where	r.oid8 is not null and r.Islogged = 0
		end

		/* read events */
		declare @readEvents [reads].[ReadEventType]; delete from @readEvents;
		/* transmission */
		declare @transmission [dbo].[TransmissionType];	delete from @transmission;
		/* transmission section */
		declare @transmissionSections [dbo].[TransmissionSectionType]; delete from @transmissionSections;
		/* transmission section gsm */
		declare @transmissionSectionsGsm [dbo].[TransmissionSectionGsmType]; delete from @transmissionSectionsGsm;
		/* transmission section gsm */
		declare @transmissionSectionsRadio [dbo].[TransmissionSectionRadioType]; delete from @transmissionSectionsRadio;
		/* transmission section tcp */
		declare @transmissionSectionsTcp [dbo].[TransmissionSectionTcpType]; delete from @transmissionSectionsTcp;

		if exists (select top 1 1 from @readParameters)
		begin

			declare @systemDate datetimeoffset(0),
					@measurementDate datetimeoffset(0),
					@isLogged bit,
					@readId integer,
					@resultCode integer

			declare @timeZone integer

			
			select top 1	@systemDate = SYSDATETIMEOFFSET(),
							@measurementDate = ToDateTimeOffset(MeterDate, 
								case when	(MeterDate >= '2014-03-30 02:00' and MeterDate < '2014-10-26 02:00') or
											(MeterDate >= '2015-03-29 02:00' and MeterDate < '2015-10-25 02:00') or
											(MeterDate >= '2016-03-27 02:00' and MeterDate < '2016-10-30 02:00') or
											(MeterDate >= '2017-03-26 02:00' and MeterDate < '2017-10-29 02:00') or
											(MeterDate >= '2018-03-25 02:00' and MeterDate < '2018-10-28 02:00') or
											(MeterDate >= '2019-03-31 02:00' and MeterDate < '2019-10-27 02:00') then 120 else 60 end),
							@isLogged = IsLogged
			from			#tmpReadSingle;

			--print convert(varchar, @measurementDate)
			
			/***********************************************************************
							wstawienie odczytu do metis2
			***********************************************************************/
			if (@insertReadToMetis2 = convert(bit,1))
			begin
    			exec reads.uspReadAdd
    				@TransmissionId = null,
    				@DeviceTypeId = @deviceTypeId2,
    				@ConnectionId = @deviceConnectionId2,
    				@SystemDate = @systemDate,
    				@MeasurementDate = @measurementDate,
    				@IsLogged = @isLogged,
    				@ReadSourceId = 6, --migration
    				@ReadParameters = @readParameters,
    				@ReadEvents = @readEvents,
    				@Transmissions = @transmission,
    				@TransmissionSections = @transmissionSections,
    				@TransmissionGsmSections = @transmissionSectionsGsm,
    				@TransmissionRadioSections = @transmissionSectionsRadio,
    				@TransmissionTcpSections = @transmissionSectionsTcp,
    				@TransmissionNetworkId = null,
    				@TransmissionNetworkStage = null,
    				@ReadId = @readId OUTPUT,
    				@ResultCode = @resultCode OUTPUT
    		end

				--select @readId, @resultCode
		end
		drop table #tmpReadSingle
	end

	drop table #tmpRead;

	
	set @deviceIndex = @deviceIndex + 1;

	declare @msg varchar(max) = 
	concat(convert(varchar,GETDATE()), 
			':  ',
			convert(varchar,@deviceIndex), 
			'/', 
			convert(varchar,@deviceNumber),
			' ', 
			convert(varchar, convert(decimal(10,0),convert(decimal(10,1),@deviceIndex)/convert(decimal(10,1),@deviceNumber)*100)),
			'% ', 
			@deviceTypeName1, '(',convert(varchar, @deviceTypeId1),')', '-', @deviceTypeName2, '(',convert(varchar, @deviceTypeId2),')', 
			'(id: ',convert(varchar,@deviceId1), 
			'): ',
			@deviceConnectionId1, 
			' readNumber: ', 
			convert(nvarchar(max), @readNumber), 
			' firstReadDate: ', 
			convert(varchar, @firstReadDate) , 
			' lastReadDate: ', 
			convert(varchar, @lastReadDate))

	insert into dbo.MigrationLog values (GETDATE(), convert(decimal(10,3),convert(decimal(10,3),@deviceIndex)/convert(decimal(10,3),@deviceNumber)*100), @deviceIndex, @deviceNumber, @readNumber, @deviceId1, @deviceTypeName1, @deviceTypeId1, @deviceConnectionId1, @deviceTypeName2, @deviceTypeId2, @deviceConnectionId2, @msg)

	if ((@deviceIndex-1) % @logDeviceInterval = 0)
	begin
		print @msg
	end
	--if (@readNumber > 0)
	--begin
	--	break
	--end


	fetch next from cur into @deviceId1, @deviceTypeName1, @deviceTypeId1, @deviceConnectionId1, @deviceTypeName2, @deviceTypeId2, @deviceConnectionId2
	
end

print @msg

/***********************************************************************
                /ITERACJA PRZEZ URZĄDZENIA METIS1
***********************************************************************/
close cur
deallocate cur


select top 3 * from dbo.MigrationLog order by id desc
select	DATEDIFF(second, MIN(LogDate), max(LogDate)), 
		(DATEDIFF(second, MIN(LogDate), max(LogDate))*100) / max(Perc) as 'seconds left',
		dateadd(second, ((DATEDIFF(second, MIN(LogDate), max(LogDate))*100) / max(Perc)), GETDATE()) as ETA
from	dbo.MigrationLog

