﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis1.Enums;
using Metis2;
using Metis2.DataTypes;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using MeasurementPoint = Metis2.MeasurementPoint;
using NetworkTypes = Metis1.Enums.NetworkTypes;

namespace MetisDM.Tasks
{
	public class MeasurementPoints : TaskBaseGeneric<Metis1.MeasurementPoint, MeasurementPoint, int, int>
	{
		private readonly Clients _clients;
		private readonly AddressNodes _addressNodes;
		private List<Address_Node_To_Address_Detail_Ids> _addressNodeDetails;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.MeasurementPoint;
			}
		}

		public MeasurementPoints(Settings settings, Migrate.Migrate migrate, Clients clients, AddressNodes addressNodes)
			: base(settings, migrate)
		{
			if (clients == null)
				throw new ArgumentNullException("clients");
			if (addressNodes == null)
				throw new ArgumentNullException("addressNodes");

			_clients = clients;
			_addressNodes = addressNodes;
		}

		public override void Dispose(bool disposing)
		{
			if (disposing)
			{
					if (_addressNodeDetails != null) _addressNodeDetails.Clear();
			}

			_addressNodeDetails = null;

			base.Dispose(disposing);
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.MeasurementPoint.Where(e => e.Client.Name != "System");
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => Settings.ClientIds.Contains(e.Client_Id));
			}

			MigrationElements = query.OrderByDescending(e => e.MainMeter).ToList();

			_addressNodeDetails = metis1Entity.Address_Node_To_Address_Detail_Ids.ToList();
		}

		protected override MeasurementPoint AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Metis1.MeasurementPoint migrationElement, out string description)
		{
			if (migrationElement == null)	throw new ArgumentNullException("migrationElement");

			var clientId = _clients.Mapping.ContainsKey(migrationElement.Client_Id)
									? _clients.Mapping[migrationElement.Client_Id].Key
									: (int?)null;

			if (clientId == null || clientId == -1)
			{
				throw new Exception(String.Format("Warning: measurementPoint's client ({0}) not found", migrationElement.Client.Name));
			}

			var newMeasurementPoint = metis2Entity.MeasurementPoint.FirstOrDefault(e => e.Client_Id == clientId &&
																					e.Name == migrationElement.CustomerNumber);
			if (newMeasurementPoint == null)
			{
				var addressNodeId = migrationElement.AddreessNode_Id != null && _addressNodes.Mapping.ContainsKey(migrationElement.AddreessNode_Id.Value)
									? _addressNodes.Mapping[migrationElement.AddreessNode_Id.Value].Key
									: (int?)null;

				if (addressNodeId == null || addressNodeId == -1)
				{
					throw new Exception(String.Format("Warning: measurementPoint's address node (id: {0}) not found", migrationElement.AddreessNode_Id));
				}
				if (migrationElement.MediumType_Id == null)
				{
					throw new Exception(String.Format("Warning: measurementPoint's medium type not specified"));
				}

				var mainMeterId = (int?) null;

				if (!migrationElement.MainMeter)
				{
					var addressNodeDetail = _addressNodeDetails.FirstOrDefault(e => e.AddressNode_Id == migrationElement.AddreessNode_Id);
					var buildingId = addressNodeDetail != null ? addressNodeDetail.Building_Id ?? 0 : 0;
					var addressNodeIds = _addressNodeDetails.Where(e => e.Building_Id == buildingId).Select(e => e.AddressNode_Id).ToList();

					var mainMeters = MigrationElements.Where(e => e.MainMeter && e.AddreessNode_Id != null && addressNodeIds.Contains((int)e.AddreessNode_Id)).ToList();

					if (mainMeters.Any())
					{
						var mainMeterMetis1 = mainMeters.FirstOrDefault();
						if (mainMeterMetis1 != null && mainMeters.Count == 1)
						{
							mainMeterId = Mapping.ContainsKey(mainMeterMetis1.Id)
									? Mapping[mainMeterMetis1.Id].Key
									: (int?)null;
						}
					}
				}

				var measuredQuantity = Mappings.MediumTypeToMeasuredQuantity((MediumTypes) migrationElement.MediumType_Id);
				var networkType = Mappings.NetworkTypeToNetworkType((NetworkTypes) migrationElement.NetworkType_Id);

				// odpowiednie ustawienie typu sieci w przypadku, gdy w bazie źródłowej był błąd.
				switch (measuredQuantity)
				{
					case MeasuredQuantities.GasUsage: networkType = Metis2.Enums.NetworkTypes.GasNetwork;
						break;
					case MeasuredQuantities.ElectricEnergyUsage: networkType = Metis2.Enums.NetworkTypes.ElectricNetwork;
						break;
					case MeasuredQuantities.HeatEnergyCost: networkType = Metis2.Enums.NetworkTypes.HeatNetwork;
						break;
					case MeasuredQuantities.HeatEnergyUsage: networkType = Metis2.Enums.NetworkTypes.HeatNetwork;
						break;
				}

				var measurementPointTypes = new List<MeasurmentPointType>{	new MeasurmentPointType(	addressNodeId,
																										null,
																										0,
																										migrationElement.Description != null 
																										   ? migrationElement.Description.Length > 200 ? migrationElement.Description.Substring(0, 200) : migrationElement.Description
																										   : null,
																										null,
																										null,
																										migrationElement.IsActive,
																										null,
																										migrationElement.Location,
																										null,
																										migrationElement.MainMeter,
																										(int)measuredQuantity,
																										mainMeterId,
																										migrationElement.CustomerNumber,
																										(int)networkType)};

				var terminalTypes = new List<TerminalType>();
				var terminals = migrationElement.Terminal.ToList();
				terminals.ForEach(e => terminalTypes.Add(new TerminalType(new DateTimeOffset(2000, 1, 1, 0, 0, 0, new TimeSpan(0)),
																			null,
																			null,
																			migrationElement.CustomerNumber,
																			null,
																			e.Name,
																			(int)networkType)));


				var measurementPointId = new SqlParameter("MeasurementPointId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				
				try
				{
					Procedure.Run(metis2Entity.Connection, "[measurement].[uspMeasurementPointAdd]", new[]
					{
						new SqlParameter("UserName", SqlDbType.NVarChar)					{Value = DBNull.Value},
						new SqlParameter("MeasurementPoints", SqlDbType.Structured)			{Value = UserDefinedTable.GetDataTable<MeasurmentPointType>(measurementPointTypes), TypeName = "[dbo].[MeasurmentPointType]"},
						new SqlParameter("Terminals", SqlDbType.Structured)					{Value = UserDefinedTable.GetDataTable<TerminalType>(terminalTypes), TypeName = "[dbo].[TerminalType]"},
						new SqlParameter("ClientId", SqlDbType.Int)							{Value = clientId},
						new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset)		{Value = new DateTimeOffset(2000, 1, 1, 0, 0, 0, new TimeSpan(0))},
						measurementPointId
					}, resultCode);

					var newMeasurementPointId = measurementPointId.Value as int?;
					newMeasurementPoint = metis2Entity.MeasurementPoint.FirstOrDefault(e => e.Id == newMeasurementPointId.Value);

					if (newMeasurementPoint != null && newMeasurementPoint.IsActive != migrationElement.IsActive)
					{
						newMeasurementPoint.IsActive = migrationElement.IsActive;
						metis2Entity.SaveChanges();
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = migrationElement.CustomerNumber;

			return newMeasurementPoint;
		}

		protected override string GetElementStrId(Metis1.MeasurementPoint element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(MeasurementPoint element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Metis1.MeasurementPoint element)
		{
			return element.CustomerNumber;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Metis1.MeasurementPoint element)
		{
			return element.Id;
		}

		protected override int GetElementId(MeasurementPoint element)
		{
			return element.Id;
		}
	}
}
