﻿using System;
using System.Linq;
using AT.Utils;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Enums;
using aspnet_Roles = Metis1.aspnet_Roles;

namespace MetisDM.Tasks
{
	public class Roles : TaskBaseGeneric<aspnet_Roles, Metis2.aspnet_Roles, Guid, Guid>
	{
		public Roles(Settings settings, Migrate.Migrate migrate)
			: base(settings, migrate)
		{
		}

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.Role;
			}
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			MigrationElements = metis1Entity.aspnet_Roles.ToList();
		}

		protected override Metis2.aspnet_Roles AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, aspnet_Roles migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var application = metis2Entity.aspnet_Applications.FirstOrDefault(e => e.LoweredApplicationName == migrationElement.aspnet_Applications.LoweredApplicationName);

			if (application == null)
			{
				throw new Exception("Warning: role's application not found");
			}

			var newRole = metis2Entity.aspnet_Roles.FirstOrDefault(e => e.LoweredRoleName == migrationElement.LoweredRoleName);
			if (newRole == null)
			{
				newRole = new Metis2.aspnet_Roles
				{
					RoleId = MathExtension.SequentialGuid(),
					ApplicationId = application.ApplicationId,
					RoleName = migrationElement.RoleName,
					LoweredRoleName = migrationElement.RoleName.ToLower(),
					Description = migrationElement.Description
				};

				try
				{
					metis2Entity.AddToaspnet_Roles(newRole);
					metis2Entity.SaveChanges();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}

			description = migrationElement.RoleName;
			return newRole;
		}

		protected override string GetElementStrId(aspnet_Roles element)
		{
			return element.RoleId.ToString();
		}

		protected override string GetElementStrId(Metis2.aspnet_Roles element)
		{
			return element.RoleId.ToString();
		}

		protected override string GetElementName(aspnet_Roles element)
		{
			return element.RoleName;
		}

		protected override Guid ParseMetis1ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid ParseMetis2ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid GetElementId(aspnet_Roles element)
		{
			return element.RoleId;
		}

		protected override Guid GetElementId(Metis2.aspnet_Roles element)
		{
			return element.RoleId;
		}
	}
}
