﻿using Metis1;
using Metis2;
using Metis2.DataTypes;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using DeviceConnection = Metis2.DeviceConnection;

namespace MetisDM.Tasks
{
	public class MeasurementDeviceConnections : TaskBaseGeneric<MeterInstallation, DeviceConnection, int, int>
	{
		#region Properties

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.MeasurementDeviceConnection;
			}
		}

		#endregion Properties

		#region Fields

		private readonly TransmissionDevices _transmissionDevices;
		private readonly MeasurementDevices _measurementDevices;

		#endregion Fields

		#region Constructors

		public MeasurementDeviceConnections(Settings settings, Migrate.Migrate migrate, TransmissionDevices transmissionDevices, MeasurementDevices measurementDevices)
			: base(settings, migrate)
		{
			if (transmissionDevices == null)
				throw new ArgumentNullException("transmissionDevices");

			_transmissionDevices = transmissionDevices;
			_measurementDevices = measurementDevices;
		}

		#endregion Constructors

		#region Methods

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var onlySelectedMetertypes = false;

			if (onlySelectedMetertypes)
			{
				var query2 = metis1Entity.MeterInstallation.Where(e => !new[]
				{
					(int) Metis1.Enums.MeterTypes.HeatMeter,
					(int) Metis1.Enums.MeterTypes.HeatCostAllocator
				}.Contains(e.Meter.MeterModel.MeterType_Id));
				if (Settings.ClientIds != null && Settings.ClientIds.Any())
				{
					query2 = query2.Where(e => e.Meter.Client_Id != null && Settings.ClientIds.Contains((int)e.Meter.Client_Id));
				}
				MigrationElements = query2.ToList();
			}
			else
			{
				var query2 = metis1Entity.MeterInstallation.Where(e => true);
				if (Settings.ClientIds != null && Settings.ClientIds.Any())
				{
					query2 = query2.Where(e => e.Meter.Client_Id != null && Settings.ClientIds.Contains((int)e.Meter.Client_Id));
				}
				MigrationElements = query2.ToList();
			}
		}

		protected override DeviceConnection AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, MeterInstallation migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			description = null;

			DeviceConnection newElement = null;

			foreach (var deviceInstallation in migrationElement.Terminal.DeviceInstallation)
			{
				try
				{
					newElement = AddElementTerminal(metis2Entity, migrationElement, deviceInstallation, out description);
					if (newElement != null)
					{
						WriteToLogFile(migrationElement, newElement, description, false);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (ex.Message.Contains("Warning: "))
					{
						AddWarning(migrationElement, ex.Message);
					}
					else
					{
						Debug.WriteLine("-----------\nERRROR\n----------------\n\n{0}\n\n----------------------------------\n", ex);
						AddError(migrationElement, ex.Message);
					}
				}
			}

			return newElement;
		}

		protected override string GetElementStrId(MeterInstallation element)
		{
			return element != null ? element.Id.ToString(CultureInfo.InvariantCulture) : null;
		}

		protected override string GetElementStrId(DeviceConnection element)
		{
			return element != null ? element.Id.ToString(CultureInfo.InvariantCulture) : null;
		}

		protected override string GetElementName(MeterInstallation element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(MeterInstallation element)
		{
			return element.Id;
		}

		protected override int GetElementId(DeviceConnection element)
		{
			return element.Id;
		}

		private DeviceConnection AddElementTerminal(Metis2Entities metis2Entity, MeterInstallation migrationElement, DeviceInstallation deviceInstallation, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");
			if (deviceInstallation == null)
				throw new ArgumentNullException("deviceInstallation");

			var deviceId = _transmissionDevices.Mapping.ContainsKey(deviceInstallation.Interface.Device_Id)
									? _transmissionDevices.Mapping[deviceInstallation.Interface.Device_Id].Key
									: (int?)null;
			var meterId = _measurementDevices.Mapping.ContainsKey(migrationElement.Meter_Id)
									? _measurementDevices.Mapping[migrationElement.Meter_Id].Key
									: (int?)null;

			if (deviceId == null || deviceId == -1)
			{
				throw new Exception(String.Format("Warning: connection's transmission device (id: {0}) not found", deviceInstallation.Interface.Device_Id));
			}
			if (meterId == null || meterId == -1)
			{
				throw new Exception(String.Format("Warning: connection's measurement device (id: {0}) not found", migrationElement.Meter_Id));
			}

			var device = metis2Entity.Device.FirstOrDefault(e => e.Id == deviceId);
			var meter = metis2Entity.Device.FirstOrDefault(e => e.Id == meterId);

			if (device == null)
			{
				throw new Exception(String.Format("Warning: device connection's destination transmission device (id: {0}) not found", deviceId));
			}
			if (meter == null)
			{
				throw new Exception(String.Format("Warning: device connection's destination measurement device (id: {0}) not found", meterId));
			}

			var deviceTypeGroup = (DeviceTypeGroups)(device.DeviceType.DeviceTypeGroup_Id);
			var meterTypeGroup = (DeviceTypeGroups)(meter.DeviceType.DeviceTypeGroup_Id);

			if (deviceTypeGroup == DeviceTypeGroups.HeatMeterTransmissionDevices)
			{
				Console.WriteLine(deviceTypeGroup);
				if (meterTypeGroup == DeviceTypeGroups.WaterMeters)
				{
					Console.WriteLine(meterTypeGroup);
					var parentDeviceConnection =
						metis2Entity.DeviceConnection.Where(e => e.ParentDevice_Id == deviceId)
							.OrderByDescending(e => e.ConnectionDate)
							.FirstOrDefault();
					device = parentDeviceConnection != null ? parentDeviceConnection.Device1 : null;
				}
			}

			if (device == null)
			{
				throw new Exception(String.Format("Warning: device connection's transmission device (id: {0}) not found", deviceId));
			}
			if (meter == null)
			{
				throw new Exception(String.Format("Warning: device connection's measurement device (id: {0}) not found", meterId));
			}

			var deviceActivationDate = metis2Entity.Activation.Where(
				e => e.ClientResourceType_Id == (int)ClientResourceTypes.Device &&
					 e.ClientResource_Id == deviceId)
				.Select(e => e.From)
				.FirstOrDefault();

			var meterActivationDate = metis2Entity.Activation.Where(
				e => e.ClientResourceType_Id == (int)ClientResourceTypes.Device &&
					 e.ClientResource_Id == meterId)
				.Select(e => e.From)
				.FirstOrDefault();

			var connectionDate = new[] { DateTime.SpecifyKind(migrationElement.MountDate, DateTimeKind.Local),
										 DateTime.SpecifyKind(deviceInstallation.MountDate, DateTimeKind.Local),
										 deviceActivationDate,
										 DateTime.SpecifyKind(meterActivationDate.DateTime, DateTimeKind.Local) }.Max();

			var disconnectionDate = migrationElement.UnmountDate != null && deviceInstallation.UnmountDate != null
										? new[] {   DateTime.SpecifyKind(migrationElement.UnmountDate.Value.Date, DateTimeKind.Local),
													(DateTimeOffset)DateTime.SpecifyKind(deviceInstallation.UnmountDate.Value.Date, DateTimeKind.Local) }.Min()
										: migrationElement.UnmountDate != null
											? DateTime.SpecifyKind(migrationElement.UnmountDate.Value.Date, DateTimeKind.Local)
											: deviceInstallation.UnmountDate != null
												? (DateTimeOffset?)DateTime.SpecifyKind(deviceInstallation.UnmountDate.Value.Date, DateTimeKind.Local)
												: null;

			if (disconnectionDate != null && connectionDate.Date > disconnectionDate.Value.Date)
			{
				throw new Exception(String.Format("Warning: device connection child's dates incompatible with parent's ({0} - {1})", connectionDate.Date, disconnectionDate?.Date));
			}

			var newDeviceConnection = metis2Entity.DeviceConnection.FirstOrDefault(e => e.ParentDevice_Id == deviceId &&
																					e.ChildDevice_Id == meterId &&
																					e.ConnectionDate == connectionDate);
			if (newDeviceConnection == null)
			{
				var parentTransmissionNetwork = metis2Entity.TransmissionNetworkDevice.FirstOrDefault(e => e.Device_Id == deviceId);
				var parentDeviceActivation = metis2Entity.Activation.Where(e => e.ClientResourceType_Id == (int)ClientResourceTypes.Device && e.ClientResource_Id == deviceId).OrderByDescending(e => e.From).FirstOrDefault();

				var deviceInterface = device.DeviceType.Interface.FirstOrDefault(e => e.InterfaceDirection_Id == (int)InterfaceDirections.InputInterface &&
																						e.InterfaceType.InterfaceKind_Id == (int)InterfaceKinds.Connection &&
																						e.InterfaceNumber == deviceInstallation.Interface.InterfaceNumber);

				var meterInterface = meter.DeviceType.Interface.FirstOrDefault(e => e.InterfaceDirection_Id == (int)InterfaceDirections.OutputInterface &&
																						e.InterfaceType.InterfaceKind_Id == (int)InterfaceKinds.Connection);

				if (deviceInterface == null)
				{
					throw new Exception(String.Format("Warning: device conection device's (id: {0} {1}) input connection interface not found", device.ConnectionId, device.Id));
				}
				if (meterInterface == null)
				{
					throw new Exception(String.Format("Warning: device conection child's (id: {0} {1}) input connection interface not found", meter.ConnectionId, meter.Id));
				}
				if (deviceInterface.InterfaceType_Id != meterInterface.InterfaceType_Id)
				{
					throw new Exception(String.Format("Warning: device coonection child's interface type mismatch parent's interface type ({0} / {1})", deviceInterface.InterfaceType.Name, meterInterface.InterfaceType.Name));
				}

				if (!metis2Entity.ConnectionRule.Any(e => e.InputInterface_Id == deviceInterface.Id && e.OutputInterface_Id == meterInterface.Id))
				{
					metis2Entity.AddToConnectionRule(new ConnectionRule
					{
						DeviceType_Id = device.DeviceType_Id,
						InputInterface_Id = deviceInterface.Id,
						OutputInterface_Id = meterInterface.Id
					});
				}
				metis2Entity.SaveChanges();

				var deviceConnectionId = new SqlParameter("DeviceConnectionId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};

				var pulseWeight = (migrationElement.MeterModelPulse != null && migrationElement.MeterModelPulse.PulseWeight != null)
									? (decimal?)migrationElement.MeterModelPulse.PulseWeight.Value
									: null;
				var signalsPerRound = deviceInterface.OpticalInterfaceProperties != null
									? (int?)deviceInterface.OpticalInterfaceProperties.SignalsPerRound
									: null;
				var volumePerRound = (pulseWeight != null && signalsPerRound != null)
									? (decimal?)(Math.Round(pulseWeight.Value * signalsPerRound.Value) / 1000)
									: null;
				var opticalShieldProperties = volumePerRound != null
									? meterInterface.OpticalShieldProperties.FirstOrDefault(e => e.VolumePerRound == volumePerRound)
									: meterInterface.OpticalShieldProperties.FirstOrDefault();

				var pulseWeightObj = metis2Entity.PulseWeight.FirstOrDefault(e => e.Value == pulseWeight);
				var pulseWeightId = pulseWeightObj != null ? pulseWeightObj.Id : (int?)null;

				var deviceInterfaceConnectionTableTypes = new List<DeviceInterfaceConnectionTableType>
				{
					new DeviceInterfaceConnectionTableType( meterInterface.Id,
															null,
															deviceInterface.OpticalInterfaceProperties_Id,
															opticalShieldProperties != null ? (int?)opticalShieldProperties.Id : null,
															deviceInterface.Id,
															pulseWeightId)
				};

				try
				{
					Procedure.Run(metis2Entity.Connection, "[network].[uspTransmissionNetworkDeviceAdd]", new[]
					{
						new SqlParameter("TransmissionNetworkId", SqlDbType.Int)        {Value = parentTransmissionNetwork != null ? (object)parentTransmissionNetwork.TransmissionNetwork_Id : DBNull.Value},
						new SqlParameter("ChildDeviceId", SqlDbType.Int)                {Value = meter.Id},
						new SqlParameter("ParentDeviceId", SqlDbType.Int)               {Value = device.Id},
						new SqlParameter("ConnectedInterfaces", SqlDbType.Structured)   {Value = UserDefinedTable.GetDataTable<DeviceInterfaceConnectionTableType>(deviceInterfaceConnectionTableTypes), TypeName = "[dbo].[DeviceInterfaceConnectionTableType]"},
						new SqlParameter("ConnectionDate", SqlDbType.DateTimeOffset)    {Value = connectionDate},
						new SqlParameter("ClientId", SqlDbType.Int)                     {Value = parentDeviceActivation != null ? (object)parentDeviceActivation.Client_Id : DBNull.Value},
						new SqlParameter("UserId", SqlDbType.Int)                       {Value = DBNull.Value},
						deviceConnectionId
					}, resultCode);

					var newDeviceConnectionId = deviceConnectionId.Value as int?;
					newDeviceConnection = metis2Entity.DeviceConnection.FirstOrDefault(e => e.Id == newDeviceConnectionId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}

				if (disconnectionDate != null)
				{
					try
					{
						Procedure.Run(metis2Entity.Connection, "[network].[uspTransmissionNetworkDevicesDelete]", new[]
						{
							new SqlParameter("TransmissionNetworkId", SqlDbType.Int)        {Value = parentTransmissionNetwork != null ? (object)parentTransmissionNetwork.TransmissionNetwork_Id : DBNull.Value},
							new SqlParameter("DeviceIds", SqlDbType.Structured)             {Value = UserDefinedTable.GetDataTable(new []{meterId.Value}), TypeName = "[dbo].[IdTableType]"},
							new SqlParameter("DisconnectionDate", SqlDbType.DateTimeOffset) {Value = disconnectionDate},
							new SqlParameter("DeleteChildDevices", SqlDbType.Bit)           {Value = 1},
							new SqlParameter("ClientId", SqlDbType.Int)                     {Value = parentDeviceActivation != null ? (object)parentDeviceActivation.Client_Id : DBNull.Value},
							new SqlParameter("UserId", SqlDbType.Int)                       {Value = DBNull.Value},
						}, new SqlParameter("ResultCode", SqlDbType.Int) { Direction = ParameterDirection.Output });
					}
					catch (Exception ex)
					{
						Debug.WriteLine(ex.ToString());
					}
				}
			}

			description = String.Format("{0} - {1}", meterId, deviceId);
			return newDeviceConnection;
		}

		#endregion Methods
	}
}