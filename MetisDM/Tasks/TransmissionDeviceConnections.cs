﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using Metis2.Enums;
using Metis2.DataTypes;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using DeviceConnection = Metis1.DeviceConnection;

namespace MetisDM.Tasks
{
	public class TransmissionDeviceConnections : TaskBaseGeneric<DeviceConnection, Metis2.DeviceConnection, int, int>
	{
		private readonly TransmissionDevices _transmissionDevices;

		public TransmissionDeviceConnections(Settings settings, Migrate.Migrate migrate, Clients clients, TransmissionDevices transmissionDevices)
			: base(settings, migrate)
		{
			if (clients == null) throw new ArgumentNullException("clients");
			if (transmissionDevices == null)throw new ArgumentNullException("transmissionDevices");

			_transmissionDevices = transmissionDevices;
		}

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.TransmissionDeviceConnection;
			}
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.DeviceConnection.Where(e => new[]
			{
				(int) Metis1.Enums.DeviceTypes.AtKEthRs232,
				(int) Metis1.Enums.DeviceTypes.AtKGsmRs232,
				(int) Metis1.Enums.DeviceTypes.AtKGsmRs2323Gth,
				(int) Metis1.Enums.DeviceTypes.AtUptGsm01,
				(int) Metis1.Enums.DeviceTypes.AtUptGsm01Rs
			}.Contains(e.Device.DeviceType_Id));
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.Device.DeviceActivation.Any(f => Settings.ClientIds.Contains(f.Client_Id)));
			}
			MigrationElements = query.ToList();

			var query2 = metis1Entity.DeviceConnection.Where(e => new[]
			{
				(int) Metis1.Enums.DeviceTypes.AtWmbus06,
				(int) Metis1.Enums.DeviceTypes.AtWmbus061,
				(int) Metis1.Enums.DeviceTypes.CnRe5
			}.Contains(e.Device.DeviceType_Id));
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query2 = query2.Where(e => e.Device.DeviceActivation.Any(f => Settings.ClientIds.Contains(f.Client_Id)));
			}
			MigrationElements.AddRange(query2.ToList());

			var query3 = metis1Entity.DeviceConnection.Where(e => !new[]
			{
				(int) Metis1.Enums.DeviceTypes.AtKEthRs232,
				(int) Metis1.Enums.DeviceTypes.AtKGsmRs232,
				(int) Metis1.Enums.DeviceTypes.AtKGsmRs2323Gth,
				(int) Metis1.Enums.DeviceTypes.AtUptGsm01,
				(int) Metis1.Enums.DeviceTypes.AtUptGsm01Rs,
				(int) Metis1.Enums.DeviceTypes.AtWmbus06,
				(int) Metis1.Enums.DeviceTypes.AtWmbus061,
				(int) Metis1.Enums.DeviceTypes.CnRe5
			}.Contains(e.Device.DeviceType_Id));
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query3 = query3.Where(e => e.Device.DeviceActivation.Any(f => Settings.ClientIds.Contains(f.Client_Id)));
			}
			MigrationElements.AddRange(query3.ToList());
		}

		protected override Metis2.DeviceConnection AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, DeviceConnection migrationElement, out string description)
		{
			if (migrationElement == null)	throw new ArgumentNullException("migrationElement");

			var parentDeviceId = _transmissionDevices.Mapping.ContainsKey(migrationElement.Device_Id_Parent)
									? _transmissionDevices.Mapping[migrationElement.Device_Id_Parent].Key
									: (int?)null;
			var childDeviceId = _transmissionDevices.Mapping.ContainsKey(migrationElement.Device_Id_Child)
									? _transmissionDevices.Mapping[migrationElement.Device_Id_Child].Key
									: (int?)null;

			if (parentDeviceId == null || parentDeviceId == -1)
			{
				throw new Exception(String.Format("Warning: connection's parent device ({0}) not found", migrationElement.Device1.DeviceNumber));
			}
			if (childDeviceId == null || childDeviceId == -1)
			{
				throw new Exception(String.Format("Warning: connection's child device ({0}) not found", migrationElement.Device.DeviceNumber));
			}

			var newDeviceConnection = metis2Entity.DeviceConnection.FirstOrDefault(e => e.ParentDevice_Id == parentDeviceId &&
																					e.ChildDevice_Id == childDeviceId);
			if (newDeviceConnection == null)
			{
				var parentDevice = metis2Entity.Device.FirstOrDefault(e => e.Id == parentDeviceId);
				var childDevice = metis2Entity.Device.FirstOrDefault(e => e.Id == childDeviceId);

				var parentDeviceActivation = metis2Entity.Activation.Where(e => e.ClientResourceType_Id == (int)ClientResourceTypes.Device && e.ClientResource_Id == parentDeviceId).OrderByDescending(e => e.From).FirstOrDefault();
				var childActivation = metis2Entity.Activation.Where(e => e.ClientResourceType_Id == (int)ClientResourceTypes.Device && e.ClientResource_Id == childDeviceId).OrderByDescending(e => e.From).FirstOrDefault();

				var parentTransmissionNetwork = metis2Entity.TransmissionNetworkDevice.FirstOrDefault(e => e.Device_Id == parentDeviceId);

				var connectionDate = (parentDeviceActivation != null && childActivation != null)
					? parentDeviceActivation.From > childActivation.From
						? parentDeviceActivation.From
						: childActivation.From
					: new DateTimeOffset(1900, 1, 1, 0, 0, 0, new TimeSpan(0));

				if (parentDevice == null)
				{
					throw new Exception(String.Format("Warning: device connection's parent device (id: {0}) not found", parentDeviceId));
				}
				if (childDevice == null)
				{
					throw new Exception(String.Format("Warning: device connection's child device (id: {0}) not found", childDeviceId));
				}

				var parentInterface = parentDevice.DeviceType.Interface.FirstOrDefault(e => e.InterfaceDirection_Id == (int)InterfaceDirections.InputInterface &&
																							e.InterfaceType.InterfaceKind_Id == (int)InterfaceKinds.Connection);
				var childInterface = childDevice.DeviceType.Interface.FirstOrDefault(e => e.InterfaceDirection_Id == (int)InterfaceDirections.OutputInterface &&
																							e.InterfaceType.InterfaceKind_Id == (int)InterfaceKinds.Connection);
				if (parentInterface == null)
				{
					throw new Exception(String.Format("Warning: device coonection parent device's ({0}) input connection interface not found", parentDevice.ConnectionId));
				}
				if (childInterface == null)
				{
					throw new Exception(String.Format("Warning: device coonection child device's ({0}) input connection interface not found", childDevice.ConnectionId));
				}
		
				var deviceConnectionId = new SqlParameter("DeviceConnectionId", SqlDbType.Int)	{	Direction = ParameterDirection.Output	};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)	{	Direction = ParameterDirection.Output	};
				var deviceInterfaceConnectionTableTypes = new List<DeviceInterfaceConnectionTableType> { new DeviceInterfaceConnectionTableType(childInterface.Id, null,null, null, parentInterface.Id, null) };
						
				try
				{
					Procedure.Run(metis2Entity.Connection, "[network].[uspTransmissionNetworkDeviceAdd]", new[]
					{
						new SqlParameter("TransmissionNetworkId", SqlDbType.Int)		{Value = parentTransmissionNetwork != null ? (object)parentTransmissionNetwork.TransmissionNetwork_Id : DBNull.Value},
						new SqlParameter("ChildDeviceId", SqlDbType.Int)				{Value = childDeviceId},
						new SqlParameter("ParentDeviceId", SqlDbType.Int)				{Value = parentDeviceId},
						new SqlParameter("ConnectedInterfaces", SqlDbType.Structured)	{Value = UserDefinedTable.GetDataTable<DeviceInterfaceConnectionTableType>(deviceInterfaceConnectionTableTypes), TypeName = "[dbo].[DeviceInterfaceConnectionTableType]"},
						new SqlParameter("ConnectionDate", SqlDbType.DateTimeOffset)	{Value = connectionDate},
						new SqlParameter("ClientId", SqlDbType.Int)						{Value = parentDeviceActivation != null ? (object)parentDeviceActivation.Client_Id : DBNull.Value},
						new SqlParameter("UserId", SqlDbType.Int)						{Value = DBNull.Value},
						deviceConnectionId
					}, resultCode);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}

					throw;
				}

				var newDeviceConnectionId = deviceConnectionId.Value as int?;
				newDeviceConnection = metis2Entity.DeviceConnection.FirstOrDefault(e => e.Id == newDeviceConnectionId.Value);
			}

			description = String.Format("{0} - {1}", childDeviceId, parentDeviceId);


			return newDeviceConnection;
		}

		protected override string GetElementStrId(DeviceConnection element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Metis2.DeviceConnection element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(DeviceConnection element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(DeviceConnection element)
		{
			return element.Id;
		}

		protected override int GetElementId(Metis2.DeviceConnection element)
		{
			return element.Id;
		}
	}
}
