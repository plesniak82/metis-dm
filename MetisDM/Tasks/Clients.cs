﻿using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using System;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Client = Metis1.Client;

namespace MetisDM.Tasks
{
	public class Clients : TaskBaseGeneric<Client, vwClient, int, int>
	{
		#region Properties

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.Client;
			}
		}

		#endregion Properties

		#region Constructors

		public Clients(Settings settings, Migrate.Migrate migrate, bool onlyReadProgress = false)
			: base(settings, migrate, onlyReadProgress)
		{
		}

		#endregion Constructors

		#region Methods

		public string GenerateSql(IQueryable query)
		{
			if (query as ObjectQuery != null)
			{
				string sqlQuery = (query as ObjectQuery).ToTraceString();
				return sqlQuery;
			}
			return null;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.Client.Where(e => e.Name != "System");
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => Settings.ClientIds.Contains(e.Id));
			}
			MigrationElements = query.ToList();

			//System.Diagnostics.Debug.WriteLine(String.Format("------------------------\n{0}\n--------------------------------\n", GenerateSql(query)));
		}

		protected override vwClient AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Client migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var newClient = metis2Entity.vwClient.FirstOrDefault(e => e.Name == migrationElement.Name);
			if (newClient == null)
			{
				if (true)
				{
					/********************** uzupełnienie brakujących elementów ***********************************/
					if (string.IsNullOrEmpty(migrationElement.Email)) migrationElement.Email = String.Format("{0}@address.com", migrationElement.Name.Replace(" ", ""));
					/********************** /uzupełnienie brakujących elementów ***********************************/
				}

				if (String.IsNullOrEmpty(migrationElement.Email) || String.IsNullOrWhiteSpace(migrationElement.Email))
				{
					throw new Exception("Warning: client's email not found or empty");
				}

				var sourceNetworkTypes = migrationElement.ClientNetworkType.Select(e => e.NetworkType).ToList();
				var sourceNetworkTypeNames = sourceNetworkTypes.Select(e => e.ResourceKey).ToList();
				var networkTypeIds = metis2Entity.NetworkType.Where(e => sourceNetworkTypeNames.Contains(e.ResourceKey)).Select(e => e.Id).ToList();

				var sourceClientDeviceType = migrationElement.ClientDeviceType.Select(e => e.DeviceType).ToList();
				var sourceClientDeviceTypeName = sourceClientDeviceType.Select(e => e.Name).ToList();
				var deviceTypeIds = metis2Entity.DeviceType.Where(e => sourceClientDeviceTypeName.Contains(e.Name)).Select(e => e.Id).ToList();

				var clientId = new SqlParameter("ClientId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};

				try
				{
					Procedure.Run(metis2Entity.Connection, "[access].[uspClientAdd]", new[]
					{
						new SqlParameter("IsSystemClient", SqlDbType.Bit)               {Value = migrationElement.IsSystemClient},
						new SqlParameter("Name", SqlDbType.NVarChar)                    {Value = migrationElement.Name},
						new SqlParameter("Phone", SqlDbType.NVarChar)                   {Value = migrationElement.Phone},
						new SqlParameter("Fax", SqlDbType.NVarChar)                     {Value = migrationElement.Fax},
						new SqlParameter("Email", SqlDbType.NVarChar)                   {Value = migrationElement.Email},
						new SqlParameter("Description", SqlDbType.NVarChar)             {Value = migrationElement.Description},
						new SqlParameter("NetworkTypes", SqlDbType.Structured)          {Value = UserDefinedTable.GetDataTable(networkTypeIds.ToArray()), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("DeviceTypes", SqlDbType.Structured)           {Value = UserDefinedTable.GetDataTable(deviceTypeIds.ToArray()), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("Permissions", SqlDbType.Structured)           {Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("ReportTypes", SqlDbType.Structured)           {Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("ExportTypes", SqlDbType.Structured)           {Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("ExternalSystemTypes", SqlDbType.Structured)   {Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("UserId", SqlDbType.Int)                       {Value = DBNull.Value},
						clientId
					}, resultCode);

					var newClientId = clientId.Value as int?;
					newClient = metis2Entity.vwClient.FirstOrDefault(e => e.Id == newClientId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}

					throw;
				}
			}

			description = migrationElement.Name;
			return newClient;
		}

		protected override string GetElementStrId(Client element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(vwClient element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Client element)
		{
			return element.Name;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Client element)
		{
			return element.Id;
		}

		protected override int GetElementId(vwClient element)
		{
			return element.Id;
		}

		#endregion Methods
	}
}