﻿using Metis1;
using Metis2;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace MetisDM.Tasks
{
	public class Montages : TaskBaseGeneric<MeterInstallation, Montage, int, int>
	{
		#region Properties

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.Montage;
			}
		}

		#endregion Properties

		#region Fields

		private readonly TransmissionDevices _transmissionDevices;
		private readonly MeasurementDevices _measurementDevices;
		private readonly MeasurementPoints _measurementPoints;

		#endregion Fields

		#region Constructors

		public Montages(Settings settings, Migrate.Migrate migrate, TransmissionDevices transmissionDevices, MeasurementDevices measurementDevices, MeasurementPoints measurementPoints)
			: base(settings, migrate)
		{
			if (transmissionDevices == null)
				throw new ArgumentNullException("transmissionDevices");
			if (measurementDevices == null)
				throw new ArgumentNullException("measurementDevices");
			if (measurementPoints == null)
				throw new ArgumentNullException("measurementPoints");

			_transmissionDevices = transmissionDevices;
			_measurementDevices = measurementDevices;
			_measurementPoints = measurementPoints;
		}

		#endregion Constructors

		#region Methods

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.MeterInstallation.Where(e => true);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.Meter.Client_Id != null && Settings.ClientIds.Contains((int)e.Meter.Client_Id));
			}

			MigrationElements = query.ToList();
		}

		protected override Montage AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, MeterInstallation migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var measurementPointId = _measurementPoints.Mapping.ContainsKey(migrationElement.Terminal.MeasurementPoint_Id)
										? _measurementPoints.Mapping[migrationElement.Terminal.MeasurementPoint_Id].Key
										: (int?)null;
			if (measurementPointId == null)
			{
				throw new Exception(String.Format("Warning: montage's measurement point ({0}) not found", migrationElement.Terminal.MeasurementPoint.CustomerNumber));
			}

			var sourceDeviceId = -1;
			var deviceId = (int?)null;

			if (migrationElement.Id == 178)
			{
				Console.WriteLine("cdsfc");
			}

			if (migrationElement.Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatCostAllocator)
			{
				var tmpDevice = metis1Entity.Device.FirstOrDefault(e => new[]   {   (int)Metis1.Enums.DeviceTypes.Eitn305,
																					(int)Metis1.Enums.DeviceTypes.Eitn3051
																				}.Contains(e.DeviceType_Id) &&
																				e.DeviceNumber == migrationElement.Meter.ManufacturerNumber);
				if (tmpDevice == null)
				{
					throw new Exception(String.Format("Warning: montage's device heat cost allocator ({0}) not found in source database", migrationElement.Meter.ManufacturerNumber));
				}
				sourceDeviceId = tmpDevice.Id;
				deviceId = _transmissionDevices.Mapping.ContainsKey(sourceDeviceId)
							? _transmissionDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}
			else if (migrationElement.Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.GasMeter)
			{
				var terminalInstallationPeriods =
					metis1Entity.TerminalInstallationsView.FirstOrDefault(e => e.MeterInstallation_Id == migrationElement.Id);

				var deviceInstallation = terminalInstallationPeriods != null
					? metis1Entity.DeviceInstallation.FirstOrDefault(e => e.Id == terminalInstallationPeriods.DeviceInstallation_Id)
					: null;

				var interfaces = deviceInstallation != null
					? metis1Entity.Interface.FirstOrDefault(e => e.Id == deviceInstallation.Interface_Id)
					: null;

				var transmissionDevice = interfaces != null
					? metis1Entity.Device.FirstOrDefault(e => e.Id == interfaces.Device_Id)
					: null;

				var tmpDevice = transmissionDevice != null
					? metis1Entity.Device.FirstOrDefault(e => new[]
					{
						(int) Metis1.Enums.DeviceTypes.Uniflo_GxE,
						(int) Metis1.Enums.DeviceTypes.Uniflo_GxS,
						(int) Metis1.Enums.DeviceTypes.Unismart,
					}.Contains(e.DeviceType_Id) &&
															  e.DeviceNumber == transmissionDevice.DeviceNumber)
					: null;

				if (tmpDevice == null)
				{
					throw new Exception(String.Format("Warning: montage's device gas meter ({0}) not found in source database", migrationElement.Meter.ManufacturerNumber));
				}
				sourceDeviceId = tmpDevice.Id;
				deviceId = _transmissionDevices.Mapping.ContainsKey(sourceDeviceId)
							? _transmissionDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}
			else
			{
				sourceDeviceId = migrationElement.Meter_Id;
				deviceId = _measurementDevices.Mapping.ContainsKey(sourceDeviceId)
							? _measurementDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}

			if (migrationElement.Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatMeter)
			{
				var measurementPoint = metis2Entity.MeasurementPoint.FirstOrDefault(e => e.Id == measurementPointId);
				if (measurementPoint != null)
				{
					measurementPoint.MeasuredQuantity_Id = (int)Metis2.Enums.MeasuredQuantities.HeatEnergyUsage;
					metis2Entity.SaveChanges();
				}
			}

			if (deviceId == null || deviceId == -1)
			{
				throw new Exception(String.Format("Warning: montage's device ({0}) not found", migrationElement.Meter.ManufacturerNumber));
			}

			if (migrationElement.Id == 190)
			{
				Console.WriteLine("cds");
			}

			var mountDate = (DateTimeOffset)DateTime.SpecifyKind(migrationElement.MountDate, DateTimeKind.Local);
			var unmountDate = migrationElement.UnmountDate != null
				? (DateTimeOffset?)DateTime.SpecifyKind(migrationElement.UnmountDate.Value, DateTimeKind.Local)
				: null;

			var newMontage = metis2Entity.Montage.FirstOrDefault(e => e.Device_Id == deviceId &&
																		e.MountDate == mountDate);

			if (newMontage == null)
			{
				var terminal = metis2Entity.Terminal.FirstOrDefault(e => e.MeasurementPoint_Id == measurementPointId &&
																	e.Name == migrationElement.Terminal.Name);
				if (terminal == null)
				{
					throw new Exception(String.Format("Warning: montage's terminal ({0}) not found", migrationElement.Terminal.Name));
				}

				var device = metis2Entity.Device.FirstOrDefault(e => e.Id == deviceId);
				if (device == null)
				{
					throw new Exception(String.Format("Warning: montage's device (id: {0}) not found", deviceId));
				}

				var interfac = device.DeviceType.Interface.FirstOrDefault(e => e.InterfaceDirection_Id == (int)InterfaceDirections.InputInterface &&
																				e.InterfaceType.InterfaceKind_Id == (int)InterfaceKinds.Measurement);
				if (interfac == null)
				{
					throw new Exception(String.Format("Warning: montage device's ({0}) input measurement interface not found", device.ConnectionId));
				}

				var activation =
					metis2Entity.Activation.FirstOrDefault(
						e => e.ClientResourceType_Id == (int)Metis2.Enums.ClientResourceTypes.Device &&
								e.ClientResource_Id == deviceId);

				if (activation != null && activation.From > migrationElement.MountDate)
				{
					activation.From = migrationElement.MountDate;
					metis2Entity.SaveChanges();
				}

				var montageId = new SqlParameter("MountageId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[measurement].[uspMountageAdd]", new[]
					{
						new SqlParameter("UserName", SqlDbType.NVarChar)                {Value = DBNull.Value},
						new SqlParameter("DeviceId", SqlDbType.Int)                     {Value = deviceId.Value},
						new SqlParameter("InterfaceId", SqlDbType.Int)                  {Value = interfac.Id},
						new SqlParameter("TerminalId", SqlDbType.Int)                   {Value = terminal.Id},
						new SqlParameter("MountDate", SqlDbType.DateTimeOffset)         {Value = mountDate},
						new SqlParameter("UnmountDate", SqlDbType.DateTimeOffset)       {Value = unmountDate != null ? (object)unmountDate : DBNull.Value},
						new SqlParameter("InitialState", SqlDbType.Decimal)             {Value = migrationElement.InitialState},
						new SqlParameter("EndState", SqlDbType.Decimal)                 {Value = migrationElement.EndState != null ? (object)migrationElement.EndState : DBNull.Value},
						new SqlParameter("Resistance", SqlDbType.Decimal)               {Value = migrationElement.WireResistance},
						new SqlParameter("SealNumber", SqlDbType.NVarChar)              {Value = migrationElement.SealNumber},
						montageId
					}, resultCode);

					var newMontageId = montageId.Value as int?;
					newMontage = metis2Entity.Montage.FirstOrDefault(e => e.Id == newMontageId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = String.Format("{0} - {1}", migrationElement.MountDate, migrationElement.UnmountDate);

			return newMontage;
		}

		protected override string GetElementStrId(MeterInstallation element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Montage element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(MeterInstallation element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(MeterInstallation element)
		{
			return element.Id;
		}

		protected override int GetElementId(Montage element)
		{
			return element.Id;
		}

		#endregion Methods
	}
}