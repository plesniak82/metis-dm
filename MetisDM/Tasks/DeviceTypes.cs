﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using Metis2.DataTypes;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using DeviceType = Metis2.DeviceType;

namespace MetisDM.Tasks
{
	public class DeviceTypes : TaskBaseGeneric<MeterModel, DeviceType, int, int>
	{
		private readonly Clients _clients;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.DeviceType;
			}
		}

		public DeviceTypes(Settings settings, Migrate.Migrate migrate, Clients clients)
			: base(settings, migrate)
		{
			if (clients == null)
				throw new ArgumentNullException("clients");

			_clients = clients;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.MeterModel.Where(e => e.Client.Name != "System" &&
			                                               new[]
			                                               {
				                                               (int) Metis1.Enums.MeterTypes.HeatMeter,
				                                               (int) Metis1.Enums.MeterTypes.WaterMeter,
				                                               (int) Metis1.Enums.MeterTypes.HeatCostAllocator,
				                                               (int) Metis1.Enums.MeterTypes.GasMeter
			                                               }.Contains(e.MeterType_Id));
			
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.Client_Id != null && Settings.ClientIds.Contains((int)e.Client_Id));
			}

			MigrationElements = query.ToList();
		}

		protected override DeviceType AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, MeterModel migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var deviceTypeName = Settings.DeviceMapping.Where(e => e[0] == migrationElement.Name).Select(e => e[1]).FirstOrDefault();
			if (deviceTypeName != null)
			{
				migrationElement.Name = deviceTypeName;
			}

			var newDeviceType = metis2Entity.DeviceType.FirstOrDefault(e => e.Name == migrationElement.Name);
			if (newDeviceType != null)
			{
				var clientId = migrationElement.Client_Id != null && _clients.Mapping.ContainsKey((int) migrationElement.Client_Id)
					? _clients.Mapping[(int) migrationElement.Client_Id].Key
					: (int?) null;

				try
				{
					Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceActivate]", new[]
					{
						new SqlParameter("ClientId", SqlDbType.Int) {Value = clientId},
						new SqlParameter("ClientResourceTypeId", SqlDbType.Int) {Value = (int) ClientResourceTypes.DeviceType},
						new SqlParameter("ClientResourceId", SqlDbType.Int) {Value = newDeviceType.Id},
						new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset) {Value = DateTimeOffset.MinValue},
						new SqlParameter("ActivationId", SqlDbType.Int) {Direction = ParameterDirection.Output}
					}, new SqlParameter("ResultCode", SqlDbType.Int) {Direction = ParameterDirection.Output});
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}
			}
			else if (migrationElement.MeterType.Id == (int)Metis1.Enums.MeterTypes.WaterMeter)
			{
				var clientId = migrationElement.Client_Id != null && _clients.Mapping.ContainsKey((int)migrationElement.Client_Id)
									? _clients.Mapping[(int)migrationElement.Client_Id].Key
									: (int?)null;
				var waterMeterParameter = migrationElement.WaterMeterParameter.FirstOrDefault();
				var pulseWeigths = migrationElement.MeterModelPulse.Select(f => f.PulseWeight.Value).ToList();

				var interfacePulseTable = new List<InterfacePulseTable>();
				metis2Entity.PulseWeight.Where(e => pulseWeigths.Contains(e.Value) && e.Unit_Id == 1).ToList().ForEach(e => interfacePulseTable.Add(new InterfacePulseTable(null,
																																										null,
																																										1,
																																										null,
																																										false,
																																										e.Id
																																										)));
				var interfaceEventParameterTable = new List<InterfaceEventParameterTable>();
				var deviceModelTableType = new List<DeviceModelTableType>();

				var meterInstallations = metis1Entity.MeterInstallation.Where(e => e.Meter.MeterModel_Id == migrationElement.Id).ToList();
				var activationDate = meterInstallations.Any() ? (DateTimeOffset?)meterInstallations.Min(e => e.MountDate) : null;

				if (clientId == null || clientId == -1)
				{
					throw new Exception(String.Format("Warning: device type's client ({0}) not found", migrationElement.Client.Name));
				}
				if (!interfacePulseTable.Any())
				{
					throw new Exception("Warning: device type's pulse weight not found");
				}
				if (activationDate == null)
				{
					throw new Exception("Warning: device type's activation date unable to be specified");
				}

				var deviceTypeId = new SqlParameter("DeviceTypeId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[device].[uspClientDeviceTypeWatermeterAdd]", new[]
					{
						new SqlParameter("Name", SqlDbType.NVarChar)						{Value = migrationElement.Name},
						new SqlParameter("ClientDescription", SqlDbType.NVarChar)			{Value = DBNull.Value},
						new SqlParameter("ClientId", SqlDbType.Int)							{Value = clientId},
						new SqlParameter("Dn", SqlDbType.Decimal)							{Value = waterMeterParameter != null ? (object)waterMeterParameter.DN : 0},
						new SqlParameter("Q1", SqlDbType.Decimal)							{Value = waterMeterParameter != null ? (object)waterMeterParameter.Q1 : 0},
						new SqlParameter("Q2", SqlDbType.Decimal)							{Value = waterMeterParameter != null ? (object)waterMeterParameter.Q2 : 0},
						new SqlParameter("Q3", SqlDbType.Decimal)							{Value = waterMeterParameter != null ? (object)waterMeterParameter.Q3 : 0},
						new SqlParameter("Q4", SqlDbType.Decimal)							{Value = waterMeterParameter != null ? (object)waterMeterParameter.Q4 : 0},
						new SqlParameter("OutputInterfaceCount", SqlDbType.Int)				{Value = 1},
						new SqlParameter("PulseWeights", SqlDbType.Structured)				{Value = UserDefinedTable.GetDataTable<InterfacePulseTable>(interfacePulseTable), TypeName = "[dbo].[InterfacePulseTable]"},
						new SqlParameter("InterfaceEventParameters", SqlDbType.Structured)	{Value = UserDefinedTable.GetDataTable<InterfaceEventParameterTable>(interfaceEventParameterTable), TypeName = "[dbo].[InterfaceEventParameterTable]"},
						new SqlParameter("DeviceModels", SqlDbType.Structured)				{Value = UserDefinedTable.GetDataTable<DeviceModelTableType>(deviceModelTableType), TypeName = "[dbo].[DeviceModelTableType]"},
						new SqlParameter("SerialNumberPatternId", SqlDbType.Int)			{Value = DBNull.Value},
						new SqlParameter("UserId", SqlDbType.Int)							{Value = DBNull.Value},
						new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset)		{Value = activationDate},
						deviceTypeId
					}, resultCode);

					var newDeviceTypeId = deviceTypeId.Value as int?;
					newDeviceType = metis2Entity.DeviceType.FirstOrDefault(e => e.Id == newDeviceTypeId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}
			else
			{
				throw new Exception(String.Format("Warning: device type's ({0}) corresponding device type not found in destination database", migrationElement.Name));
			}

			description = migrationElement.Name;
			return newDeviceType;
		}

		protected override string GetElementStrId(MeterModel element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(DeviceType element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(MeterModel element)
		{
			return element.Name;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(MeterModel element)
		{
			return element.Id;
		}

		protected override int GetElementId(DeviceType element)
		{
			return element.Id;
		}
	}
}
