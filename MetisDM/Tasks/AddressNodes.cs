﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis1.Enums;
using Metis2;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using AddressNode = Metis1.AddressNode;

namespace MetisDM.Tasks
{
	public class AddressNodes : TaskBaseGeneric<AddressNode, Metis2.AddressNode, int, int>
	{
		private readonly Clients _clients;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.AddressStructure;
			}
		}

		public AddressNodes(Settings settings, Migrate.Migrate migrate, Clients clients, bool onlyReadProgress = false)
			: base(settings, migrate)
		{
			_clients = clients;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.AddressNode.Where(e => //(e.AddressNode2 == null || e.AddressType.Name == "City") && 
				e.Client.Name != "System" &&
				!new[]
				{
					(int) AddressNodeTypes.Country,
					(int) AddressNodeTypes.Region1,
					(int) AddressNodeTypes.Region2,
					(int) AddressNodeTypes.Region3,
					(int) AddressNodeTypes.Region4
				}.Contains(e.AddressType.Id));

			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => Settings.ClientIds.Contains(e.Client_Id));
			}
			MigrationElements = query.OrderBy(e => e.AddressType_Id).ToList();
		}

		protected override Metis2.AddressNode AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, AddressNode migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var parentAddressNodeId = migrationElement.AddressNode_Id != null && Mapping.ContainsKey(migrationElement.AddressNode_Id.Value)
											? Mapping[migrationElement.AddressNode_Id.Value].Key
											: (int?)null;

			var addressNodeType = Mappings.AddressNodeTypeToAddressNodeType((AddressNodeTypes)migrationElement.AddressType_Id);
			if (addressNodeType == null)
			{
				throw new Exception(String.Format("Warning: source address node type ({0}) not found in destination database", migrationElement.AddressType.Name));
			}

			Metis2.AddressNode newAddressNode;
			if (migrationElement.AddressNode_Id == null || new[]{	(int)AddressNodeTypes.Country, 
																	(int)AddressNodeTypes.Region1, 
																	(int)AddressNodeTypes.Region2, 
																	(int)AddressNodeTypes.Region3,
																	(int)AddressNodeTypes.Region4
																	}.Contains(migrationElement.AddressNode2.AddressType.Id))
			{
				newAddressNode = metis2Entity.AddressNode.FirstOrDefault(e => e.Name == migrationElement.Name &&
																		e.AddressNodeType.Id == (int)addressNodeType &&
																		e.Client.PersonalData.Any(f => f.Name == migrationElement.Client.Name));
			}
			else
			{
				if (parentAddressNodeId == null || parentAddressNodeId == -1)
				{
					throw new Exception(String.Format("Warning: adres node's parent node ({0}) not found", migrationElement.AddressNode2.Name));
				}

				var parentAddressNode = metis2Entity.AddressNode.FirstOrDefault(e => e.Id == parentAddressNodeId);
				if (parentAddressNode == null)
				{
					throw new Exception(String.Format("Warning: adres node's parent node ({0}) not found", migrationElement.AddressNode2.Name));
				}

				if ((parentAddressNode.AddressNodeType_Id == (int)Metis2.Enums.AddressNodeTypes.Street && addressNodeType != Metis2.Enums.AddressNodeTypes.Building) ||
					(parentAddressNode.AddressNodeType_Id == (int)Metis2.Enums.AddressNodeTypes.Building && addressNodeType != Metis2.Enums.AddressNodeTypes.Local && addressNodeType != Metis2.Enums.AddressNodeTypes.Staircase) ||
					(parentAddressNode.AddressNodeType_Id == (int)Metis2.Enums.AddressNodeTypes.Staircase && addressNodeType != Metis2.Enums.AddressNodeTypes.Local))
				{
					throw new Exception(String.Format("Warning: invalid adres node hierarchy ({0} under {1})", addressNodeType, ((Metis2.Enums.AddressNodeTypes)(parentAddressNode.AddressNodeType_Id))));
				}

				newAddressNode = metis2Entity.AddressNode.FirstOrDefault(e => e.Name == migrationElement.Name &&
																		e.AddressNodeType.Id == (int)addressNodeType &&
																		e.AddressNode2.Id == parentAddressNodeId &&
																		e.Client.PersonalData.Any(f => f.Name == migrationElement.Client.Name));
			}

			if (newAddressNode == null)
			{
				var clientId = _clients.Mapping.ContainsKey(migrationElement.Client_Id)
									? _clients.Mapping[migrationElement.Client_Id].Key
									: (int?)null;


				if (clientId == null || clientId == -1)
				{
					throw new Exception(String.Format("Warning: adres node's client ({0}) not found", migrationElement.Client.Name));
				}

				var addressNodeId = new SqlParameter("AddressNodeId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};

				try
				{
					Procedure.Run(metis2Entity.Connection, "[measurement].[uspAddresNodeAdd]", new[]
					{
						new SqlParameter("ClientId", SqlDbType.Int)					{Value = clientId},
						new SqlParameter("UserName", SqlDbType.NVarChar)			{Value = DBNull.Value},
						new SqlParameter("AddressNodeTypeId", SqlDbType.Int)		{Value = (int)addressNodeType.Value},
						new SqlParameter("ParentAddressNodeId", SqlDbType.NVarChar)	{Value = parentAddressNodeId != null ? (object)parentAddressNodeId : DBNull.Value},
						new SqlParameter("Name", SqlDbType.NVarChar)				{Value = migrationElement.Name},
						new SqlParameter("ZipCode", SqlDbType.NVarChar)				{Value = migrationElement.ZipCode},
						new SqlParameter("FloorNumber", SqlDbType.NVarChar)			{Value = migrationElement.FloorNumber},
						addressNodeId
					}, resultCode);

					var newAddressNodeId = addressNodeId.Value as int?;
					newAddressNode = metis2Entity.AddressNode.FirstOrDefault(e => e.Id == newAddressNodeId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw ex;
				}
			}

			description = migrationElement.Name;
			return newAddressNode;
		}

		protected override string GetElementStrId(AddressNode element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Metis2.AddressNode element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(AddressNode element)
		{
			return element.Name;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(AddressNode element)
		{
			return element.Id;
		}

		protected override int GetElementId(Metis2.AddressNode element)
		{
			return element.Id;
		}
	}
}
