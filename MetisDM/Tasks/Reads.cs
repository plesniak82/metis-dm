﻿using Metis1;
using Metis1.Enums;
using Metis2;
using Metis2.Enums;
using Metis2.Writter;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using MetisDM.Tasks.Read;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace MetisDM.Tasks
{
	public class Reads : TaskBaseGeneric<Metis1.Read, Metis2.Read, int, int>
	{
		#region Properties

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.Reads;
			}
		}

		#endregion Properties

		#region Fields

		private readonly TransmissionDevices _transmissionDevices;
		private readonly Metis1.Enums.DeviceTypes _sourceDeviceType;
		private readonly AT.Models.Devices.Types _destinationDeviceType;

		//private List<Metis2.Read> _newReadList;
		//private List<InterfacesPath> _newInterfacesPathList;
		//private List<ReadDevice> _newReadDeviceList;
		//private List<ReadParameter> _newReadParameterList;
		//private List<ReadEvent> _newReadEventList;
		private bool _importToEmptyTable;

		private int readId = 0;
		private int interfacePathId = 0;
		private int readDeviceId = 0;
		private int readParameterId = 0;
		private int readEventId = 0;

		private int LastReadId = -1;
		private int LastDestinationReadId = -1;

		private IQueryable<Metis1.Read> MigrationElementsQuery;

		#endregion Fields

		#region Constructors

		public Reads(Settings settings, Migrate.Migrate migrate, TransmissionDevices transmissionDevices, bool importToEmptyTable, Metis1.Enums.DeviceTypes deviceType)
			: base(settings, migrate)
		{
			_transmissionDevices = transmissionDevices;
			_importToEmptyTable = importToEmptyTable;
			_sourceDeviceType = deviceType;

			var deviceTypeToDeviceTypeModel = Mappings.DeviceTypeToDeviceTypeModel((Metis1.Enums.DeviceTypes)_sourceDeviceType);
			if (deviceTypeToDeviceTypeModel == null)
			{
				throw new Exception(String.Format("Warning: source device type ({0}) not found in destination database", deviceType));
			}
			_destinationDeviceType = deviceTypeToDeviceTypeModel.Value.Key;
		}

		#endregion Constructors

		#region Methods

		public override void OnRun()
		{
			if (ProgressContent != null && ProgressContent.Any() && ProgressContent[0].Count == 4)
			{
				int.TryParse(ProgressContent[0][1], out LastReadId);
				int.TryParse(ProgressContent[0][2], out LastDestinationReadId);
			}

			using (var metis1Entity = new Metis1Entities(Settings.SourceDatabaseConnectionString, 3000))
			{
				SetMigrationElementsQuery(metis1Entity);
				DoneElements = 0;
				TotalElements = MigrationElementsQuery.Count();
				RaiseProgressChanged(DoneElements, TotalElements);
			}

			if (readId <= LastDestinationReadId || readId == 0)
			{
			}

			while (true)
			{
				try
				{
					Console.WriteLine(DateTime.Now);
					using (var metis1Entity = new Metis1Entities(Settings.SourceDatabaseConnectionString, 30000))
					{
						SetMigrationElementsQuery(metis1Entity);
						var start = DateTime.Now;
						if (TotalElements > 0)
						{
							Console.WriteLine(TotalElements);
						}

						var maxPackSize = 1000;
						var takeNumber = (TotalElements - DoneElements) > maxPackSize ? maxPackSize : (TotalElements - DoneElements);
						MigrationElements = MigrationElementsQuery.Take(takeNumber).ToList();
						Debug.WriteLine("Get ------------------------- {0} - {1}", DateTime.Now.Subtract(start), MigrationElements.Count);
						if (!MigrationElements.Any())
						{
							break;
						}

						LastReadId = MigrationElements.Max(e => e.Id);

						using (var metis2Entity = new Metis2Entities(Settings.TargetDatabaseConnectionString, 30000))
						{
							start = DateTime.Now;
							foreach (var migrationElement in MigrationElements)
							{
								try
								{
									String description;
									var newElement = AddElement(metis1Entity, metis2Entity, migrationElement, out description);
									if (newElement != null)
									{
										WriteToLogFile(migrationElement, newElement, description, true);
									}
								}
								catch (Exception ex)
								{
									Debug.WriteLine(ex.ToString());
									if (ex.Message.Contains("Warning"))
									{
										AddWarning(migrationElement, ex.Message);
									}
									else
									{
										Debug.WriteLine("-----------\nERRROR\n----------------\n\n{0}\n\n----------------------------------\n", ex);
										AddError(migrationElement, ex.Message);
										MessageBox.Show(ex.ToString(), "Errors", MessageBoxButtons.OK);
									}
								}

								RaiseProgressChanged(++DoneElements, TotalElements);
							}

							MigrationElements.Clear();
							MigrationElements = null;
						}
					}
					Thread.Sleep(TimeSpan.FromSeconds(10));
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
				}
			}
		}

		public string GenerateSql(IQueryable query)
		{
			if (query as ObjectQuery != null)
			{
				string sqlQuery = (query as ObjectQuery).ToTraceString();
				return sqlQuery;
			}
			return null;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			SetMigrationElementsQuery(metis1Entity);
			MigrationElements = MigrationElementsQuery.ToList();
			//String query = GenerateSql(MigrationElements);
			//Debug.WriteLine(query);
		}

		protected override string GetTaskName()
		{
			return String.Format("Reads ({0})", _sourceDeviceType);
		}

		protected override Metis2.Read AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Metis1.Read migrationElement, out string description)
		{
			try
			{
				if (migrationElement == null)
					throw new ArgumentNullException("migrationElement");

				var isLogged = Migrate.ReadTypeDict.Any(e => e.Id == migrationElement.ReadType_Id &&
															 e.ReadKind_Id == (int)ReadKinds.ReadKind_Logged);

				var measurementDate = new DateTimeOffset(migrationElement.MeterDate.Value,
					new DateTimeOffset(migrationElement.MeterDate.Value).Offset);

				Random rnd = new Random();
				if (new[]
				{
					(int?) Metis1.Enums.DeviceTypes.Eitn305,
					(int?) Metis1.Enums.DeviceTypes.Eitn3051
				}.Contains(migrationElement.DeviceType_Id))
				{
					if (!isLogged)
					{
						var systemDate = new DateTimeOffset(migrationElement.InsertDate.Value,
							new DateTimeOffset(migrationElement.InsertDate.Value).Offset);

						if (systemDate.Date > measurementDate.Date)
						{
							measurementDate = systemDate.Date.AddSeconds(-1 * rnd.Next(1, 600));
						}
						else
						{
							measurementDate = new DateTimeOffset(measurementDate.Year,
								measurementDate.Month,
								measurementDate.Day,
								systemDate.Hour,
								systemDate.Minute,
								systemDate.Second,
								systemDate.Millisecond,
								systemDate.Offset);
						}
					}
					else
					{
						//measurementDate = measurementDate.AddSeconds(-1);
					}
				}

				List<ReadParameterData> readParameters;
				List<ReadEventData> readEvents;
				GetParametersEvents(migrationElement, isLogged, out readParameters, out readEvents);
				var newRead = GetNewRead(migrationElement, measurementDate, isLogged, readParameters, readEvents);

				if ((readParameters != null && readParameters.Any()) ||
					(readEvents != null && readEvents.Any()))
				{
					var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
					{
						Direction = ParameterDirection.Output
					};
					var readId = new SqlParameter("ReadId", SqlDbType.Int)
					{
						Direction = ParameterDirection.Output
					};

					var deviceNumberInt = 0;
					string deviceNumberStr = null;
					if (int.TryParse(migrationElement.DeviceNumber, out deviceNumberInt))
					{
						deviceNumberStr = deviceNumberInt.ToString();
					}
					else
					{
						deviceNumberStr = migrationElement.DeviceNumber;
					}

					try
					{
						Procedure.Run(metis2Entity.Connection, "[reads].[uspReadAdd]", new[]
						{
							new SqlParameter("TransmissionId", SqlDbType.UniqueIdentifier) {Value = DBNull.Value},
							new SqlParameter("DeviceTypeId", SqlDbType.Int) {Value = (int)_destinationDeviceType},
							new SqlParameter("ConnectionId", SqlDbType.NVarChar) {Value = deviceNumberStr},
							new SqlParameter("SystemDate", SqlDbType.DateTimeOffset) {Value = newRead.SystemDate},
							new SqlParameter("MeasurementDate", SqlDbType.DateTimeOffset) {Value = newRead.MeasurementDate},
							new SqlParameter("IsLogged", SqlDbType.Bit) {Value = newRead.IsLogged},
							new SqlParameter("ReadSourceId", SqlDbType.Int) {Value = newRead.ReadSource_Id},

							new SqlParameter("ReadParameters", SqlDbType.Structured)    {   Value = UserDefinedTable.GetDataTable<ReadParameterData>(readParameters),   TypeName = "[reads].[ReadParameterTypeType]"    },
							new SqlParameter("ReadEvents", SqlDbType.Structured)        {   Value = UserDefinedTable.GetDataTable<ReadEventData>(readEvents),   TypeName = "[reads].[ReadEventType]"    },

							new SqlParameter("TransmissionNetworkId", SqlDbType.Int )               {Value = DBNull.Value},
							new SqlParameter("TransmissionNetworkStage", SqlDbType.Int )            {Value = DBNull.Value},

							readId
						}, resultCode);
					}
					catch (Exception ex)
					{
						Debug.WriteLine(ex.ToString());
						throw ex;
					}

					description = migrationElement.DeviceNumber;
					return newRead;
				}

				description = migrationElement.DeviceNumber;
				return newRead;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				throw ex;
			}
		}

		protected override string GetElementStrId(Metis1.Read element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Metis2.Read element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Metis1.Read element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override int ParseMetis1ElementId(string str)
		{
			var result = -1;
			int.TryParse(str, out result);
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			var result = -1;
			int.TryParse(str, out result);
			return result;
		}

		protected override int GetElementId(Metis1.Read element)
		{
			return element.Id;
		}

		protected override int GetElementId(Metis2.Read element)
		{
			return element.Id;
		}

		private void SetMigrationElementsQuery(Metis1Entities metis1Entity)
		{
			var readTypeIds = Migrate.ReadTypeDict.Where(e => e.DeviceType_Id == (int)_sourceDeviceType &&
															new[]
															{
																(int?)ReadKinds.ReadKind_Instant,
																(int?)ReadKinds.ReadKind_Logged
															}.Contains(e.ReadKind_Id))
												.Select(e => e.Id).ToList();

			if (Settings.OnlyLastDailyreads && false)
			{
				if (Settings.ClientIds != null && Settings.ClientIds.Any() && false)
				{
					var deviceTypes =
						metis1Entity.ClientDeviceType.Where(e => Settings.ClientIds.Contains(e.Client_Id))
							.Select(e => (int?)e.DeviceType_Id);

					var groupedReadIds = metis1Entity.Read.Join(metis1Entity.DeviceActivation,
																	read => read.Device_Id,
																	deviceActivation => deviceActivation.Device_Id,
																	(read, deviceActivation) => new { Read = read, DeviceActivation = deviceActivation })
															.Where(e => Settings.ClientIds.Contains(e.DeviceActivation.Client_Id) &&
																		   deviceTypes.Contains(e.Read.DeviceType_Id) &&
																		   readTypeIds.Contains(e.Read.ReadType_Id) &&
																		   e.Read.DeviceInterface != null &&
																		   e.Read.Id > LastReadId)
															.Select(e => e.Read)
															.OrderBy(e => e.Id)
															.GroupBy(e => new
															{
																e.Device_Id,
																e.DeviceInterface,
																e.ReadType_Id,
																((DateTime)(e.MeterDate)).Year,
																((DateTime)(e.MeterDate)).Month,
																((DateTime)(e.MeterDate)).Day
															})
															.Select(f => f.Max(e => e.Id)); //.ToList();

					MigrationElementsQuery = metis1Entity.Read.Where(e => groupedReadIds.Contains(e.Id)).OrderBy(e => e.Id);

					MigrationElementsQuery = metis1Entity.Read.Join(metis1Entity.Device,
						read => read.Device_Id,
						device => device.Id,
						(read, device) => read)
						.Where(e => deviceTypes.Contains(e.DeviceType_Id) &&
									readTypeIds.Contains(e.ReadType_Id) &&
									e.DeviceInterface != null &&
									e.Id > LastReadId)
						.OrderBy(e => e.Id);
				}
				else
				{
					//var deviceIds = new List<int>
					//{
					//	10855, 10856, 10857, 10886, 31819, 31820, 31826, 31827, 31865, 31866, 31867, 31868, 31872, 31873, 31874, 31875, 31815, 31816, 31817, 31818, 31857, 31858, 31859, 31860, 31861, 31862, 31863, 31864, 29073, 31821, 31822, 31823, 31824, 31825, 31828, 31829, 31830, 31831, 31832, 31869, 31870, 31842, 31843, 31844, 31845, 31849, 31850, 31851, 31852, 31895, 31896, 31897, 31853, 31854, 31855, 31856, 31876, 31877, 31878, 31879, 34122, 31833, 31834, 61104, 61105, 31838, 31839, 31840, 31841, 31847, 31891, 31892, 31893, 31894, 31898, 31899, 31900, 31901, 31835, 31836, 31837, 31880, 31881, 31882, 31883, 31884, 31885, 31886, 61102, 61103, 59984, 61112, 61113, 61116, 61117, 31887, 31888, 31889, 31890, 61092, 61093, 61084, 61085, 61088, 61089, 61070, 61071, 61094, 61095, 61096, 61097, 61074, 61075, 61078, 61079, 61100, 61101, 61072, 61073, 61076, 61077, 61098, 61099, 61080, 61081, 61114, 61115, 61086, 61087, 61090, 61091, 61082, 61083, 61106, 61107, 61108, 61109, 61110, 61111, 213, 351, 541, 60115, 59324
					//};

					var dateTo = DateTime.Now.AddYears(-1);

					var groupedReadIds = metis1Entity.Read.Where(
						e =>    //deviceIds.Contains((int)e.Device_Id) &&
								e.MeterDate <= dateTo &&
								readTypeIds.Contains(e.ReadType_Id) &&
								e.DeviceInterface != null &&
								e.Id > LastReadId).GroupBy(e => new
								{
									e.Device_Id,
									e.DeviceInterface,
									e.ReadType_Id,
									((DateTime)(e.MeterDate)).Year,
									((DateTime)(e.MeterDate)).Month,
									((DateTime)(e.MeterDate)).Day
								}
						)
						.Select(f => f.Max(e => e.Id)); //.ToList();

					MigrationElementsQuery = metis1Entity.Read.Where(e => groupedReadIds.Contains(e.Id)).OrderBy(e => e.Id);
				}
			}
			else
			{
				if (Settings.ClientIds != null && Settings.ClientIds.Any() && false)
				{
					var deviceTypes =
						metis1Entity.ClientDeviceType.Where(e => Settings.ClientIds.Contains(e.Client_Id))
							.Select(e => (int?)e.DeviceType_Id);

					MigrationElementsQuery = metis1Entity.Read.Join(metis1Entity.DeviceActivation,
																		read => read.Device_Id,
																		deviceActivation => deviceActivation.Device_Id,
																		(read, deviceActivation) => new { Read = read, DeviceActivation = deviceActivation })
																.Where(e => Settings.ClientIds.Contains(e.DeviceActivation.Client_Id) &&
																			   deviceTypes.Contains(e.Read.DeviceType_Id) &&
																			   readTypeIds.Contains(e.Read.ReadType_Id) &&
																			   e.Read.DeviceInterface != null &&
																			   e.Read.Id > LastReadId)
																.Select(e => e.Read)
																.OrderBy(e => e.Id);
				}
				else
				{
					//var deviceIds = new List<int>();
					////{
					////	213, 351, 541, 10855, 10856, 10857, 10886, 29073, 31815, 31816, 31817, 31818, 31819, 31820, 31821, 31822, 31823, 31824, 31825, 31826, 31827, 31828, 31829, 31830, 31831, 31832, 31833, 31834, 31835, 31836, 31837, 31838, 31839, 31840, 31841, 31842, 31843, 31844, 31845, 31847, 31849, 31850, 31851, 31852, 31853, 31854, 31855, 31856, 31857, 31858, 31859, 31860, 31861, 31862, 31863, 31864, 31865, 31866, 31867, 31868, 31869, 31870, 31872, 31873, 31874, 31875, 31876, 31877, 31878, 31879, 31880, 31881, 31882, 31883, 31884, 31885, 31886, 31887, 31888, 31889, 31890, 31891, 31892, 31893, 31894, 31895, 31896, 31897, 31898, 31899, 31900, 31901, 34122, 59324, 59984, 60115, 61070, 61071, 61072, 61073, 61074, 61075, 61076, 61077, 61078, 61079, 61080, 61081, 61082, 61083, 61084, 61085, 61086, 61087, 61088, 61089, 61090, 61091, 61092, 61093, 61094, 61095, 61096, 61097, 61098, 61099, 61100, 61101, 61102, 61103, 61104, 61105, 61106, 61107, 61108, 61109, 61110, 61111, 61112, 61113, 61114, 61115, 61116, 61117
					////};

					//var dateFrom = new DateTime(2015, 01, 01, 00, 00, 00);
					//if (deviceIds != null && deviceIds.Any() && false)
					//{
					//	MigrationElementsQuery = metis1Entity.Read.Where(e => readTypeIds.Contains(e.ReadType_Id) &&
					//														e.DeviceInterface != null &&
					//														deviceIds.Contains((int)e.Device_Id) &&
					//														e.Id > LastReadId
					//														&& e.MeterDate >= dateFrom
					//	//&& e.MeterDate <= dateTo
					//	).OrderBy(e => e.Id);
					//}
					//else
					var dateFrom = new DateTime(2017, 01, 01, 00, 00, 00);
					var dateTo = new DateTime(2018, 01, 01, 00, 00, 00);
					{
						MigrationElementsQuery = metis1Entity.Read.Where(e => readTypeIds.Contains(e.ReadType_Id) &&
																				e.DeviceInterface != null &&
																				e.Id > LastReadId &&
																				e.MeterDate >= dateFrom &&
																				e.MeterDate < dateTo
																		).OrderBy(e => e.Id);
					}
				}
			}
			//MigrationElementsQuery = metis1Entity.Read.Where(e => e.Id == 5049082); //.ToList();
		}

		private void GetParametersEvents(Metis1.Read migrationElement, bool isLogged, out List<ReadParameterData> readParameters, out List<ReadEventData> readEvents)
		{
			readParameters = null;
			readEvents = null;
			if (new[]   {   (int?)Metis1.Enums.DeviceTypes.AtWmbus01,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus04,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus05,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus07,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus08,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus09,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus10,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus11,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus112,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus12,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus161,
							(int?)Metis1.Enums.DeviceTypes.AtWmbus162,
							(int?)Metis1.Enums.DeviceTypes.AtUptGsm01,
						}.Contains(migrationElement.DeviceType_Id))
			{
				readParameters = WaterMeter.GetParameters(migrationElement, isLogged);
				readEvents = WaterMeter.GetEvents(migrationElement);
			}
			else if (new[]  {   (int?)Metis1.Enums.DeviceTypes.Eitn305,
								(int?)Metis1.Enums.DeviceTypes.Eitn3051
						}.Contains(migrationElement.DeviceType_Id))
			{
				readParameters = HeatDivisor.GetParameters(migrationElement, isLogged);
				readEvents = HeatDivisor.GetEvents(migrationElement);
			}
			else if (new[]  {   (int?)Metis1.Enums.DeviceTypes.Uniflo_GxE,
								(int?)Metis1.Enums.DeviceTypes.Uniflo_GxS,
								(int?)Metis1.Enums.DeviceTypes.Unismart
						}.Contains(migrationElement.DeviceType_Id))
			{
				readParameters = GasMeter.GetParameters(migrationElement, isLogged);
				readEvents = GasMeter.GetEvents(migrationElement);
			}
			else if (new[]  {   (int?)Metis1.Enums.DeviceTypes.AtWmbusMr01,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr02,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr03,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr04,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr05,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr06,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr07,
								(int?)Metis1.Enums.DeviceTypes.AtWmbusMr09
						}.Contains(migrationElement.DeviceType_Id))
			{
				readParameters = HeatMeter.GetParameters(migrationElement, isLogged);
				readEvents = HeatMeter.GetEvents(migrationElement);
			}
			else if (new[]  {   (int?)Metis1.Enums.DeviceTypes.CnRe5
						}.Contains(migrationElement.DeviceType_Id))
			{
				readParameters = TemperatureSensor.GetParameters(migrationElement, isLogged);
				readEvents = TemperatureSensor.GetEvents(migrationElement);
			}

			if ((readParameters == null || !readParameters.Any()) &&
				(readEvents == null || !readEvents.Any()))
			{
				throw new Exception(String.Format("Warning: neither read's ({0}) parameters nor events found", migrationElement.Id));
			}
		}

		private Metis2.Read GetNewRead(Metis1.Read migrationElement, DateTimeOffset measurementDate, bool isLogged, List<ReadParameterData> readParameters, List<ReadEventData> readEvents)
		{
			//stworzenie nowego odczytu
			var newDbRead = new Metis2.Read
			{
				Id = ++readId,
				Transmission_Id = null,
				SystemDate = new DateTimeOffset(migrationElement.InsertDate.Value, new DateTimeOffset(migrationElement.InsertDate.Value).Offset),
				MeasurementDate = measurementDate,
				IsLogged = isLogged,
				ReadSource_Id = (int)ReadSources.Device
			};

			return newDbRead;
		}

		#endregion Methods
	}
}