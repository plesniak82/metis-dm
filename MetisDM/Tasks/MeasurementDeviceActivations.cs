﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using Metis2.DataTypes;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;

namespace MetisDM.Tasks
{
	public class MeasurementDeviceActivations : TaskBaseGeneric<MeterInstallation, Activation, int, int>
	{
		private readonly Clients _clients;
		private readonly MeasurementDevices _measurementDevices;
		private readonly TransmissionDevices _transmissionDevices;

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.MeasurementDeviceActivation;
			}
		}

		public MeasurementDeviceActivations(Settings settings, Migrate.Migrate migrate, Clients clients, TransmissionDevices transmissionDevices, MeasurementDevices measurementDevices)
			: base(settings, migrate)
		{
			if (clients == null)
				throw new ArgumentNullException("clients");
			if (measurementDevices == null)
				throw new ArgumentNullException("measurementDevices");

			_clients = clients;
			_transmissionDevices = transmissionDevices;
			_measurementDevices = measurementDevices;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query =
				metis1Entity.MeterInstallation	.Where(e =>	e.Meter.Client_Id != metis1Entity.Client.Where(f => f.Name == "System").Select(f => f.Id).FirstOrDefault() &&
															e.Meter.MeterModel.MeterType_Id != (int)Metis1.Enums.MeterTypes.HeatCostAllocator &&
															e.Meter.MeterModel.MeterType_Id != (int)Metis1.Enums.MeterTypes.GasMeter);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.Meter.Client_Id != null && Settings.ClientIds.Contains((int)e.Meter.Client_Id));
			}

			MigrationElements = query.ToList();
		}

		protected override Activation AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, MeterInstallation migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var clientId = migrationElement.Meter.Client_Id != null && _clients.Mapping.ContainsKey((int)migrationElement.Meter.Client_Id)
									? _clients.Mapping[(int)migrationElement.Meter.Client_Id].Key
									: (int?)null;

			var sourceDeviceId = -1;
			var deviceId = (int?)null;

			if (migrationElement.Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatCostAllocator)
			{
				var tmpDevice = metis1Entity.Device.FirstOrDefault(e => new[]	{	(int)Metis1.Enums.DeviceTypes.Eitn305,
																					(int)Metis1.Enums.DeviceTypes.Eitn3051
																				}.Contains(e.DeviceType_Id) &&
																				e.DeviceNumber == migrationElement.Meter.ManufacturerNumber);
				if (tmpDevice == null)
				{
					throw new Exception(String.Format("Warning: montage's device heat cost allocator ({0}) not found in source database", migrationElement.Meter.ManufacturerNumber));
				}
				sourceDeviceId = tmpDevice.Id;
				deviceId = _transmissionDevices.Mapping.ContainsKey(sourceDeviceId)
							? _transmissionDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}
			else if (migrationElement.Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.GasMeter)
			{
				var terminalInstallationPeriods =
					metis1Entity.TerminalInstallationsView.FirstOrDefault(e => e.MeterInstallation_Id == migrationElement.Id);

				var deviceInstallation = terminalInstallationPeriods != null
					? metis1Entity.DeviceInstallation.FirstOrDefault(e => e.Id == terminalInstallationPeriods.DeviceInstallation_Id)
					: null;

				var interfaces = deviceInstallation != null
					? metis1Entity.Interface.FirstOrDefault(e => e.Id == deviceInstallation.Interface_Id)
					: null;

				var transmissionDevice = interfaces != null
					? metis1Entity.Device.FirstOrDefault(e => e.Id == interfaces.Device_Id)
					: null;

				var tmpDevice = transmissionDevice != null
					? metis1Entity.Device.FirstOrDefault(e => new[]
					{
						(int) Metis1.Enums.DeviceTypes.Uniflo_GxE,
						(int) Metis1.Enums.DeviceTypes.Uniflo_GxS,
						(int) Metis1.Enums.DeviceTypes.Unismart,
					}.Contains(e.DeviceType_Id) && e.DeviceNumber == transmissionDevice.DeviceNumber)
					: null;

				if (tmpDevice == null)
				{
					throw new Exception(String.Format("Warning: montage's device gas meter ({0}) not found in source database", migrationElement.Meter.ManufacturerNumber));
				}
				sourceDeviceId = tmpDevice.Id;
				deviceId = _transmissionDevices.Mapping.ContainsKey(sourceDeviceId)
							? _transmissionDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}
			else
			{
				sourceDeviceId = migrationElement.Meter_Id;
				deviceId = _measurementDevices.Mapping.ContainsKey(sourceDeviceId)
							? _measurementDevices.Mapping[sourceDeviceId].Key
							: (int?)null;
			}


			if (clientId == null || clientId == -1)
			{
				throw new Exception(String.Format("Warning: device activation's client ({0}) not found", migrationElement.Meter.Client_Id));
			}
			if (deviceId == null || deviceId == -1)
			{
				throw new Exception(String.Format("Warning: device activation's device ({0}) not found", migrationElement.Meter.ManufacturerNumber));
			}


			var newActivation = metis2Entity.Activation.Where(e =>  e.Client_Id == clientId &&
																	e.ClientResourceType_Id == (int)ClientResourceTypes.Device &&
																	e.ClientResource_Id == deviceId &&
																	e.From <= migrationElement.MountDate).OrderByDescending(e => e.From).FirstOrDefault();
			if (newActivation == null)
			{
				var activationId = new SqlParameter("ActivationId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceActivate]", new[]
					{
						new SqlParameter("ClientId", SqlDbType.Int)						{Value = clientId},
						new SqlParameter("ClientResourceTypeId", SqlDbType.Int)			{Value = (int)ClientResourceTypes.Device},
						new SqlParameter("ClientResourceId", SqlDbType.Int)				{Value = deviceId},
						new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset)	{Value = migrationElement.MountDate},
						activationId
					}, resultCode);

					var newActivationId = activationId.Value as int?;
					newActivation = metis2Entity.Activation.FirstOrDefault(e => e.Id == newActivationId.Value);

					if (migrationElement.UnmountDate != null)
					{
						var clientResource = new List<ClientResource> { new ClientResource(deviceId.Value, (int)ClientResourceTypes.Device) };
						resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
						{
							Direction = ParameterDirection.Output
						};
						Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceDeactivate]", new[]
						{
							new SqlParameter("DeactivationDate", SqlDbType.DateTimeOffset)	{Value = migrationElement.UnmountDate},
							new SqlParameter("Resources", SqlDbType.Structured)				{Value = UserDefinedTable.GetDataTable<ClientResource>(clientResource), TypeName = "[dbo].[ClientResource]"},
							new SqlParameter("ClientId", SqlDbType.Int)						{Value = clientId}
						}, resultCode);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = String.Format("{0} - {1}", migrationElement.MountDate, migrationElement.UnmountDate);

			return newActivation;
		}

		protected override string GetElementStrId(MeterInstallation element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Activation element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(MeterInstallation element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(MeterInstallation element)
		{
			return element.Id;
		}

		protected override int GetElementId(Activation element)
		{
			return element.Id;
		}
	}
}
