﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using AT.Models.Devices;
using Metis1;
using Metis2;
using Metis2.Enums;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using Device = Metis1.Device;

namespace MetisDM.Tasks
{
	public class TransmissionDevices : TaskBaseGeneric<Device, Metis2.Device, int, int>
	{
		private List<KeyValuePair<int, int>> _clientDeviceTypeActivation;
		private readonly Clients _clients;
		private readonly AddressNodes _addressNodes;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.TransmissionDevice;
			}
		}

		public TransmissionDevices(Settings settings, Migrate.Migrate migrate, Clients clients, AddressNodes addressNodes, bool onlyReadProgress = false)
			: base(settings, migrate, onlyReadProgress)
		{
			if (addressNodes == null)
				throw new ArgumentNullException("addressNodes");

			_clients = clients;
			_addressNodes = addressNodes;
			_clientDeviceTypeActivation = new List<KeyValuePair<int, int>>();
		}


		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.Device.Where(e => e.DeviceType_Id != (int)Metis1.Enums.DeviceTypes.AtWmbus05);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.DeviceActivation.Any(f => Settings.ClientIds.Contains(f.Client_Id)));
			}

			MigrationElements = query.ToList();//.Where(e => new[]{
			//															(int) Metis1.Enums.DeviceTypes.AtKEthRs232,
			//															(int) Metis1.Enums.DeviceTypes.AtKGsmRs232,
			//															(int) Metis1.Enums.DeviceTypes.AtKGsmRs2323Gth,
			//															(int) Metis1.Enums.DeviceTypes.AtUptGsm01,
			//															(int) Metis1.Enums.DeviceTypes.AtUptGsm01Rs,
			//															(int) Metis1.Enums.DeviceTypes.AtWmbus06,
			//															(int) Metis1.Enums.DeviceTypes.AtWmbus061
			//														}.Contains(e.DeviceType_Id)
			//														||
			//														metis1Entity.DeviceInstallation.Any(f => f.Interface.Device_Id == e.Id)).ToList();
		}

		protected override Metis2.Device AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Device migrationElement, out string description)
		{
			if (migrationElement == null) throw new ArgumentNullException("migrationElement");

			var deviceTypeModel = Mappings.DeviceTypeToDeviceTypeModel((Metis1.Enums.DeviceTypes)migrationElement.DeviceType_Id);
			if (deviceTypeModel == null)
			{
				throw new Exception(String.Format("Warning: source device type ({0}) not found in destination database", migrationElement.DeviceType.Name));
			}
			var deviceType = deviceTypeModel.Value.Key;

			var addressNodeId = migrationElement.AddressNode_Id != null && _addressNodes.Mapping.ContainsKey((int)migrationElement.AddressNode_Id)
									? _addressNodes.Mapping[(int)migrationElement.AddressNode_Id].Key
									: (int?)null;
			if (migrationElement.AddressNode_Id != null && addressNodeId == null || addressNodeId == -1)
			{
				throw new Exception(String.Format("Warning: transmission device's address node (id: {0}) not found", migrationElement.AddressNode_Id));
			}
			//wyeliminowanie wiodących zer z numerów radiowych
			var deviceNumber = (int) 0;
			if (int.TryParse(migrationElement.DeviceNumber, out deviceNumber))
			{
				migrationElement.DeviceNumber = deviceNumber.ToString();
			}

			Metis2.Device newDevice = null;

			if (newDevice == null)
			{
				var deviceId = new SqlParameter("DeviceId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					var trimmedDeviceNumber = migrationElement.DeviceNumber.Trim();
					trimmedDeviceNumber = trimmedDeviceNumber.Length > 20 ? trimmedDeviceNumber.Substring(0, 20) : trimmedDeviceNumber;

					if (deviceType == Types.AtWmbus061 || deviceType == Types.AtWmbus06th)
					{
						var index = 2;
						while (true)
						{
							if (metis2Entity.Device.Any(e => e.DeviceType_Id == (int) deviceType && e.ConnectionId == trimmedDeviceNumber))
							{
								trimmedDeviceNumber = String.Format("{0}_{1}", trimmedDeviceNumber.Substring(0, 17), index++);
							}
							else
							{
								break;
							}
						}
					}

					Procedure.Run(metis2Entity.Connection, "[device].[uspDeviceAdd]", new[]
					{
						new SqlParameter("DeviceTypeId", SqlDbType.Int)					{Value = (int)deviceType},
						new SqlParameter("DeviceModelId", SqlDbType.Int)				{Value = DBNull.Value}, //deviceModel != null ? (object)(int)deviceModel : DBNull.Value},
						new SqlParameter("ConnectionIdNumber", SqlDbType.NVarChar)		{Value = trimmedDeviceNumber},
						new SqlParameter("SerialNumber", SqlDbType.NVarChar)			{Value = trimmedDeviceNumber},
						new SqlParameter("UserId", SqlDbType.Int)						{Value = DBNull.Value},
						new SqlParameter("AddressNodeId", SqlDbType.Int)				{Value = (object)addressNodeId ?? DBNull.Value},
						new SqlParameter("InteraceToLocalization", SqlDbType.Int)		{Value = DBNull.Value},
						deviceId
					}, resultCode);

					var newDeviceId = deviceId.Value as int?;
					newDevice = metis2Entity.Device.FirstOrDefault(e => e.Id == newDeviceId.Value);
				}
				catch (Exception ex)
				{
					newDevice = metis2Entity.Device.FirstOrDefault(e => e.ConnectionId == migrationElement.DeviceNumber &&
																		e.DeviceType_Id == (int)deviceType);
					if (newDevice == null)
					{
						Debug.WriteLine(ex.ToString());
						if (resultCode.Value as int? < 0)
						{
							throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
						}
						throw;
					}
				}
			}

			//aktywacja typu urządzenia
			if (newDevice != null)
			{
				if (!metis2Entity.Activation.Any(e => e.ClientResourceType_Id == 2 &&
				                                      e.ClientResource_Id == newDevice.Id))
				{
					var activation = migrationElement.DeviceActivation.OrderByDescending(e => e.From).FirstOrDefault();
					//if (activation != null)
					//{
						var clientId = activation != null && _clients.Mapping.ContainsKey(activation.Client_Id)
							? _clients.Mapping[activation.Client_Id].Key
							: (int?) null;

						if (clientId == null)
						{
							var nonSystemClient = metis2Entity.Client.FirstOrDefault(e => !e.IsSystemClient);
							clientId = nonSystemClient != null ? (int?)nonSystemClient.Id : null;
						}

						if (clientId != null &&
						    !_clientDeviceTypeActivation.Any(e => e.Key == clientId.Value && e.Value == newDevice.DeviceType_Id))
						{
							_clientDeviceTypeActivation.Add(new KeyValuePair<int, int>(clientId.Value, newDevice.DeviceType_Id));

							if (!metis2Entity.Activation.Any(e => e.Client_Id == clientId &&
							                                      e.ClientResourceType_Id == (int) ClientResourceTypes.DeviceType &&
							                                      e.ClientResource_Id == newDevice.DeviceType_Id))
							{
								var clientDeviceType = metis2Entity.ClientDeviceType.FirstOrDefault(e => e.Client_Id == clientId &&
								                                                                         e.DeviceType_Id ==
								                                                                         newDevice.DeviceType_Id);

								if (clientDeviceType == null)
								{
									clientDeviceType = new Metis2.ClientDeviceType
									{
										Client_Id = clientId.Value,
										DeviceType_Id = newDevice.DeviceType_Id
									};
									metis2Entity.AddToClientDeviceType(clientDeviceType);
									metis2Entity.SaveChanges();
								}

								Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceActivate]", new[]
								{
									new SqlParameter("ClientId", SqlDbType.Int) {Value = clientId},
									new SqlParameter("ClientResourceTypeId", SqlDbType.Int) {Value = (int) ClientResourceTypes.DeviceType},
									new SqlParameter("ClientResourceId", SqlDbType.Int) {Value = newDevice.DeviceType_Id},
									new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset) {Value = DateTimeOffset.MinValue},
									new SqlParameter("ActivationId", SqlDbType.Int) {Direction = ParameterDirection.Output}
								}, new SqlParameter("ResultCode", SqlDbType.Int) {Direction = ParameterDirection.Output});
							}
						}
					//}
					//else
					//{
					//	if (!metis2Entity.Activation.Any(e => e.ClientResourceType_Id == 2 && e.ClientResource_Id == newDevice.Id))
					//	{
					//		throw new Exception(String.Format("Warning: transmission device's ({0} : {1}) activation not found",
					//			migrationElement.DeviceType.Name, migrationElement.DeviceNumber));
					//	}
					//}
				}
			}

			description = migrationElement.DeviceNumber;
			return newDevice;
		}

		protected override string GetElementStrId(Device element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Metis2.Device element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Device element)
		{
			return element.DeviceNumber;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Device element)
		{
			return element.Id;
		}

		protected override int GetElementId(Metis2.Device element)
		{
			return element.Id;
		}
	}
}
