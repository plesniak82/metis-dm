﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using MetisDM.Classes;
using MetisDM.Database;
using Metis1;
using Metis2;
using Metis2.DataTypes;
using Metis2.Enums;
using MetisDM.Enums;

namespace MetisDM.Tasks
{
	public class TransmissionDeviceActivations : TaskBaseGeneric<DeviceActivation, Activation, int, int>
	{
		private readonly Clients _clients;
		private readonly TransmissionDevices _transmissionDevices;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.TransmissionDeviceActivation;
			}
		}

		public TransmissionDeviceActivations(Settings settings, Migrate.Migrate migrate, Clients clients, TransmissionDevices transmissionDevices)
			: base(settings, migrate)
		{
			if (clients == null)
				throw new ArgumentNullException("clients");
			if (transmissionDevices == null)
				throw new ArgumentNullException("transmissionDevices");

			_clients = clients;
			_transmissionDevices = transmissionDevices;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.DeviceActivation.Where(e => e.Client.Name != "System");
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => Settings.ClientIds.Contains(e.Client_Id));
			}
			MigrationElements = query.ToList();
		}

		protected override Activation AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, DeviceActivation migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var clientId = _clients.Mapping.ContainsKey(migrationElement.Client_Id)
									? _clients.Mapping[migrationElement.Client_Id].Key
									: (int?)null;

			var deviceId = _transmissionDevices.Mapping.ContainsKey(migrationElement.Device_Id)
									? _transmissionDevices.Mapping[migrationElement.Device_Id].Key
									: (int?)null;

			var dateFrom = migrationElement.From;
			if (new[]	{	(int)Metis1.Enums.DeviceTypes.Eitn305,
							(int)Metis1.Enums.DeviceTypes.Eitn3051
						}.Contains(migrationElement.Device.DeviceType_Id))
			{
				var meter = metis1Entity.Meter.FirstOrDefault(e => e.ManufacturerNumber == migrationElement.Device.DeviceNumber &&
																	e.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatCostAllocator);
				if (meter != null)
				{
					var meterInstallations = metis1Entity.MeterInstallation.Where(e => e.Meter_Id == meter.Id).ToList();
					if (meterInstallations.Any())
					{
						dateFrom = meterInstallations.Max(e => e.MountDate);
					}
				}
			}
			else if (new[]	{	(int)Metis1.Enums.DeviceTypes.AtWmbusMr01,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr02,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr03,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr04,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr05,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr06,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr07,
								(int)Metis1.Enums.DeviceTypes.AtWmbusMr09
							}.Contains(migrationElement.Device.DeviceType_Id))
			{
				var meter = metis1Entity.Meter.FirstOrDefault(e => e.ManufacturerNumber == migrationElement.Device.DeviceNumber &&
																	e.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatMeter);
				if (meter != null)
				{
					var meterInstallations = metis1Entity.MeterInstallation.Where(e => e.Meter_Id == meter.Id).ToList();
					if (meterInstallations.Any())
					{
						dateFrom = meterInstallations.Max(e => e.MountDate);
					}
				}
			}

			if (clientId == null || clientId == -1)
			{
				throw new Exception(String.Format("Warning: device activation's client ({0}) not found", migrationElement.Client.Name));
			}
			if (deviceId == null || deviceId == -1)
			{
				throw new Exception(String.Format("Warning: device activation's device ({0}) not found", migrationElement.Device.DeviceNumber));
			}


			var newActivation = metis2Entity.Activation.FirstOrDefault(e => e.Client_Id == clientId &&
																		e.ClientResourceType_Id == (int)ClientResourceTypes.Device &&
																		e.ClientResource_Id == deviceId &&
																		e.From == dateFrom);
			if (newActivation == null)
			{
				var activationId = new SqlParameter("ActivationId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceActivate]", new[]
					{
						new SqlParameter("ClientId", SqlDbType.Int)						{Value = clientId},
						new SqlParameter("ClientResourceTypeId", SqlDbType.Int)			{Value = (int)ClientResourceTypes.Device},
						new SqlParameter("ClientResourceId", SqlDbType.Int)				{Value = deviceId},
						new SqlParameter("ActivationDate", SqlDbType.DateTimeOffset)	{Value = dateFrom},
						activationId
					}, resultCode);

					var newActivationId = activationId.Value as int?;
					newActivation = metis2Entity.Activation.FirstOrDefault(e => e.Id == newActivationId.Value);

					if (migrationElement.To != null)
					{
						var clientResource = new List<ClientResource> { new ClientResource(deviceId.Value, (int)ClientResourceTypes.Device) };
						resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
						{
							Direction = ParameterDirection.Output
						};
						Procedure.Run(metis2Entity.Connection, "[access].[uspClientResourceDeactivate]", new[]
						{
							new SqlParameter("DeactivationDate", SqlDbType.DateTimeOffset)	{Value = migrationElement.To},
							new SqlParameter("Resources", SqlDbType.Structured)				{Value = UserDefinedTable.GetDataTable<ClientResource>(clientResource), TypeName = "[dbo].[ClientResource]"},
							new SqlParameter("ClientId", SqlDbType.Int)						{Value = clientId}
						}, resultCode);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = String.Format("{0} - {1}", migrationElement.From, migrationElement.To);

			return newActivation;
		}

		protected override string GetElementStrId(DeviceActivation element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Activation element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(DeviceActivation element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(DeviceActivation element)
		{
			return element.Id;
		}

		protected override int GetElementId(Activation element)
		{
			return element.Id;
		}
	}
}
