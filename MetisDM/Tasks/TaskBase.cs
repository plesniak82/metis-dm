﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Windows.Threading;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Enums;

namespace MetisDM.Tasks
{
	public abstract class TaskBase : ValidateViewModel
	{
		protected String LogFilename;
		protected String ProgressFilename;

		private bool _onlyReadProgress;

		protected List<List<String>> LogContent;
		protected List<List<String>> ProgressContent;

		public delegate void ScriptProgressHandler(object sender, int done, int total);
		public event ScriptProgressHandler ProgressChanged;

		public delegate void CompletedHandler(object sender, ResultStatus result);
		public event CompletedHandler Completed;

		private DateTime _startDate;

		protected Migrate.Migrate Migrate;

		public abstract MigrationTaskType MigrationTaskType
		{
			get;
		}

		private string _details;
		public string Details
		{
			get
			{
				return _details;
			}
			set
			{
				_details = value;
				Dispatcher.CurrentDispatcher.Invoke(new Action(() => RaisePropertyChanged("Details")));
			}
		}


		private DateTime? _lastProgressUpdate;

		private bool _isBusy;
		public bool IsBusy
		{
			get
			{
				return _isBusy;
			}
			set
			{
				_isBusy = value;
				RaisePropertyChanged("IsBusy");
			}
		}

		//private MigrationTaskType _migrationStep;
		//public MigrationTaskType MigrationStep
		//{
		//    get { return _migrationStep; }
		//    set
		//    {
		//        _migrationStep = value;
		//        RaisePropertyChanged("MigrationStep");
		//    }
		//}


		public string TaskName
		{
			get
			{
				return GetTaskName();
			}
		}

		protected Settings Settings
		{
			get;
			private set;
		}

		private TaskState _state = TaskState.WaitingForQueue;
		public TaskState State
		{
			get
			{
				return _state;
			}
			set
			{
				_state = value;
				RaisePropertyChanged("State");
			}
		}

		private int _progress;
		public int Progress
		{
			get
			{
				return _progress;
			}
			set
			{
				_progress = value;
				Invoke(() => RaisePropertyChanged("Progress"));
			}
		}

		protected TaskBase(Settings settings, Migrate.Migrate migrate, bool onlyReadProgress = false)
		{
			Settings = settings;
			Migrate = migrate;
			_onlyReadProgress = onlyReadProgress;
		}

		protected virtual string GetTaskName()
		{
			var name = GetType().FullName.Split('.');
			return name.Last();
		}


		protected void ReportProgress(BackgroundWorker worker, int lineNumber, int count)
		{
			var completed = lineNumber == count;
			var currentProgress = count != 0 ? (int)(100 * lineNumber / (decimal)count) : 0;
			if (completed)
			{
			}
			else if (!_lastProgressUpdate.HasValue)
			{
			}
			else if (DateTime.Now.Subtract(_lastProgressUpdate.Value).TotalMilliseconds >= 250 && currentProgress != Progress)
			{
			}
			else
			{
				return;
			}
			_lastProgressUpdate = DateTime.Now;
			Progress = currentProgress;
			worker.ReportProgress(Progress);
		}
		protected void RaiseCompleted()
		{
			if (Completed != null)
			{
				Completed.Invoke(this, new ResultStatus(Errors));
				Completed = null;
			}
		}
		protected void RaiseProgressChanged(int done, int total)
		{
			if (ProgressChanged != null)
			{
				var dateTime = new DateTime(DateTime.Now.Subtract(_startDate).Ticks);
				Details = String.Format("{0,14}   ({1}/{2})", dateTime.Day <= 1 ? dateTime.ToString("HH:mm:ss") : String.Format("{0}.{1}", dateTime.Day - 1, dateTime.ToString("HH:mm:ss")), done, total);
				ProgressChanged.Invoke(this, done, total);
			}
		}


		protected void Invoke(Action action)
		{
			if (Dispatcher.CurrentDispatcher.CheckAccess())
			{
				action.Invoke();
			}
			else
			{
				Dispatcher.CurrentDispatcher.BeginInvoke(new Action(action.Invoke));
			}
		}

		//zapisywanie logow do pliku
		protected void ErrorCheck(SqlParameter resultCode)
		{
			if (resultCode != null && resultCode.Value is int && (resultCode.Value as int?) != 0)
			{
				throw new DomainException(resultCode.Value.ToString(), (int)resultCode.Value);
			}
		}

		public override void Cleanup()
		{
			base.Cleanup();
			Completed = null;
			ProgressChanged = null;
		}

		protected String ErrorCodeToMsg(int? code)
		{
			if (code != null)
			{
				var msg = Migrate.ErrorCodeDefaultValues.FirstOrDefault(e => e.Code == code.Value);

				if (msg != null)
				{
					return String.Format("{0} ({1})", msg.DefaultValue, code);
				}
				return String.Format("{0}", code);
			}
			return null;
		}


		public void Run()
		{
			try
			{
				State = TaskState.InProgress;
				_startDate = DateTime.Now;

				IsBusy = true;

				var folder = String.Format(@"{0}\[{1};{2}]-[{3};{4}]",  Settings.LogFolder,
																		Settings.SourceDatabaseServerAddress,
																		Settings.SourceDatabaseName,
																		Settings.TargetDatabaseServerAddress,
																		Settings.TargetDatabaseName);

				LogFilename = String.Format(@"{0}\{1}.csv", folder, GetTaskName());
				ProgressFilename = String.Format(@"{0}\{1}_progress.csv", folder, GetTaskName());

				LogContent = ReadCsvFile(LogFilename, !_onlyReadProgress);
				ProgressContent = ReadCsvFile(ProgressFilename, false);

				ParseLogContent();

				try
				{
					if (!_onlyReadProgress)
					{
						OnRun();
					}
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.ToString());
					Console.WriteLine(DateTime.Now);
					AddError(ex.Message);
				}
				finally
				{
					IsBusy = false;
				}
				State = HasWarnings ? TaskState.Warnings : TaskState.Successful;
				State = HasErrors ? TaskState.Errors : TaskState.Successful;
				RaiseCompleted();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				throw;
			}
		}
		public abstract void ParseLogContent();
		public virtual void OnRun()
		{
			try
			{
				using (var metis1Entity = new Metis1Entities(Settings.SourceDatabaseConnectionString, 3000))
				{
					using (var metis2Entity = new Metis2Entities(Settings.TargetDatabaseConnectionString, 3000))
					{
						ExecuteRun(metis1Entity, metis2Entity);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			
		}

		protected abstract void ExecuteRun(Metis1Entities metis1Entity, Metis2Entities metis2Entity);

		private List<List<String>> ReadCsvFile(String filename, bool deleteAfterRead)
		{
			var result = new List<List<String>>();
			if (File.Exists(filename))
			{
				var contents = File.ReadAllText(filename).Split('\n').ToList();
				contents.ForEach(e => result.Add(e.Split(';').ToList()));
			}

			if (deleteAfterRead)
			{
				File.Delete(filename);
			}

			return result;
		}
	}
}
