﻿using Metis2.Writter;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MetisDM.Tasks.Read
{
	public static class HeatDivisor
	{
		#region Methods

		public static List<ReadParameterData> GetParameters(Metis1.Read read, bool isLogged)
		{
			var result = new List<ReadParameterData>();

			if (isLogged)
			{
				if (read.oid3 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Bearing, read.oid3.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Year));
				if (read.oid6 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.AverageTemperature, read.oid6.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Year));
			}
			else
			{
				try
				{
					if (read.oid1 != null)
					{
						DateTime date;
						date = DateTime.ParseExact(read.oid1.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
						result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.SettlementDate, date.ToString("yyyy-MM-dd HH:mm:ss"), null, (int)AT.Models.Reads.BillingPeriodType.Year));
					}
				}
				catch
				{
				}
				//if (read.oid3 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.PreviousPeriodBearing, read.oid3.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Year));
				if (read.oid4 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Bearing, read.oid4.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Year));
				//if (read.oid6 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.PreviousPeriodAverageTemperature, read.oid6.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Year));
				if (read.oid8 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.AverageTemperature, read.oid8.Value.ToString(CultureInfo.InvariantCulture), null, (int)AT.Models.Reads.BillingPeriodType.Month));
			}

			return result;
		}

		public static List<ReadEventData> GetEvents(Metis1.Read read)
		{
			var result = new List<ReadEventData>();

			DateTime? sealRemovalDate = null;

			if (read.oid2 != null) sealRemovalDate = DateTime.ParseExact(read.oid2.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);

			if (sealRemovalDate != null)
			{
				result.Add(new ReadEventData(null, (int)AT.Models.Events.Types.DeviceRemoved, null, sealRemovalDate, null, null, null, null, true, null, null, false));
			}

			return result;
		}

		#endregion Methods
	}
}