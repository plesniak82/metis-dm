﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace MetisDM.Tasks.Read.BulkInsert
{
	public static class Bulk
	{
		public static DataColumn GetDataColumn<T>( String propertyName ) 
		{
			foreach (var pi in typeof( T ).GetProperties() ) 
			{
				if (pi.Name == propertyName ) 
				{
					var columnType = pi.PropertyType;

					if (columnType.IsGenericType && columnType.GetGenericTypeDefinition() == typeof(Nullable<>))
					{
						columnType = columnType.GetGenericArguments()[0];
					}

					return new DataColumn(propertyName, columnType);
				}
			}
			return null;
		}

		public static Boolean BulkInsertDataObjectList<T>(String connectionString, String tableName, List<String> columnNames, List<T> dataList ) 
		{
			var dataTable = new DataTable(tableName);
			foreach (var columnName in columnNames)
			{
				var dataColumn = GetDataColumn<T>(columnName);
				if (dataColumn != null)
				{
					dataTable.Columns.Add(dataColumn.ColumnName, dataColumn.DataType);
				}
			}

			foreach (var rec in dataList ) 
			{
				var row = dataTable.NewRow();
				for (var x = 0; x < columnNames.Count; x++ ) 
				{
					var prop = typeof( T ).GetProperty( columnNames[ x ] );
					var val = prop != null ? prop.GetValue(rec, null) : null;
					row[x] = val ?? DBNull.Value;
				}
				dataTable.Rows.Add( row );
			}

			// Bulk Insert the datatable to the database.
			using (var connection = new SqlConnection(connectionString))
			{
				connection.Open();
				using (var cmd = new SqlCommand(String.Format("SET IDENTITY_INSERT [reads].[{0}] ON", tableName), connection))
				{
					cmd.BeginExecuteNonQuery();
				}

				var bulkCopy = new SqlBulkCopy(connection);

				for (var z = 0; z < columnNames.Count; z++ ) 
				{
					bulkCopy.ColumnMappings.Add(z, z);
				}

				bulkCopy.DestinationTableName = tableName;
				
				bulkCopy.BatchSize = 2000;
				bulkCopy.NotifyAfter = 50000;
				bulkCopy.WriteToServer( dataTable );

				using (var cmd = new SqlCommand(String.Format("SET IDENTITY_INSERT [reads].[{0}] OFF", tableName), connection))
				{
					cmd.BeginExecuteNonQuery();
				}

				connection.Close();
			}

			return true;
		}
	}
}
