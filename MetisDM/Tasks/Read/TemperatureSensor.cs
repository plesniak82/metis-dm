﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Metis2.Writter;

namespace MetisDM.Tasks.Read
{
	public static class TemperatureSensor
	{
		public static List<ReadParameterData> GetParameters(Metis1.Read read, bool isLogged)
		{
			var result = new List<ReadParameterData>();

			var readPath = "1";
			if (read.DeviceInterface != null)
			{
				switch (read.DeviceInterface.Value)
				{
					case 1 : readPath = "1"; break;
					case 2 : readPath = "2"; break;
					case 3 : readPath = "3"; break;
					case 4 : readPath = "4"; break;
				}
			}

			if (read.oid1 != null) result.Add(new ReadParameterData(readPath, (int)AT.Models.Parameters.Types.Resistance, read.oid1.Value.ToString(CultureInfo.InvariantCulture), null, null));
		
			return result;
		}

		public static List<ReadEventData> GetEvents(Metis1.Read read)
		{
			var result = new List<ReadEventData>();			
			return result;
		}
	}
}
