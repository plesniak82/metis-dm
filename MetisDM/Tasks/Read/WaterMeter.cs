﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Metis2.Writter;

namespace MetisDM.Tasks.Read
{
	public static class WaterMeter
	{
		public static List<ReadParameterData> GetParameters(Metis1.Read read, bool isLogged)
		{
			var result = new List<ReadParameterData>();

			var readPath = "1.1";
			if (read.DeviceInterface != null)
			{
				switch (read.DeviceInterface.Value)
				{
					case 1 : readPath = "1.1"; break;
					case 2 : readPath = "2.1"; break;
					case 3 : readPath = "3.1"; break;
				}
			}

			decimal pulseWeigth = 0;

			switch (read.DeviceType_Id)
			{
				case (int)Metis1.Enums.DeviceTypes.AtWmbus01: pulseWeigth = (decimal)0.25; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus07: pulseWeigth = (decimal)2.5; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus08: pulseWeigth = (decimal)0.333; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus09: pulseWeigth = (decimal)33.333; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus10: pulseWeigth = (decimal)333.333; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus11: pulseWeigth = (decimal)33.333; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus112: pulseWeigth = (decimal)33.333; break;
				//case (int)Metis1.Enums.DeviceTypes.AtWmbus12: pulseWeigth = (decimal)0.333; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus161: pulseWeigth = (decimal)1; break;
				case (int)Metis1.Enums.DeviceTypes.AtWmbus162: pulseWeigth = (decimal)1; break;
				default: pulseWeigth = read.oid1 != null ? read.oid1.Value : 0;
					break;
			}

			if (read.oid1 != null) result.Add(new ReadParameterData(readPath, (int)AT.Models.Parameters.Types.PulseWeight, pulseWeigth.ToString(CultureInfo.InvariantCulture), null, null));
			if (read.oid2 != null) result.Add(new ReadParameterData(readPath, (int)AT.Models.Parameters.Types.Pulses, read.oid2.Value.ToString(CultureInfo.InvariantCulture), null, null));
			if (read.oid8 != null) result.Add(new ReadParameterData(readPath, (int)AT.Models.Parameters.Types.ReversePulses, read.oid8.Value.ToString(CultureInfo.InvariantCulture), null, null));
			
			return result;
		}

		public static List<ReadEventData> GetEvents(Metis1.Read read)
		{
			var result = new List<ReadEventData>();

			if (read.oid3 != null)
			{
				var failures = (int)read.oid3;

				DateTime? magneticFieldDate		= null;
				long? magneticFieldDuration		= null;

				DateTime? overlayRemovalDate	= null;
				long? overlayRemovalDuration	= null;
				
				DateTime? backFlowDate			= null;
				long? backFlowDuration			= null;
				
				DateTime? leakDate				= null;
				DateTime? maximalFlowDate		= null;

				if (read.oid11 != null)	magneticFieldDuration = (long?)read.oid11;
				if (read.oid12 != null)	magneticFieldDate = DateTime.ParseExact(read.oid12.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
				if (read.oid13 != null)	overlayRemovalDuration = (long?)read.oid13;
				if (read.oid14 != null)	overlayRemovalDate = DateTime.ParseExact(read.oid14.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
				if (read.oid15 != null)	backFlowDuration = (long?)read.oid16;
				if (read.oid16 != null)	backFlowDate = DateTime.ParseExact(read.oid16.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
				if (read.oid17 != null)	leakDate = DateTime.ParseExact(read.oid17.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
				if (read.oid18 != null)	maximalFlowDate = DateTime.ParseExact(read.oid18.Value.ToString(CultureInfo.InvariantCulture).Substring(0, 14), "yyyyMMddHHmmss", null);
			
				if ((magneticFieldDuration != null && magneticFieldDuration != 0)|| magneticFieldDate != null)
				{
					result.Add(new ReadEventData(	null,
													(int)AT.Models.Events.Types.MagneticField,
													null,
													magneticFieldDate,
													null,
													null,
													magneticFieldDuration,
													null,
													true,
													null,
													null,
													false));
				}
				if ((overlayRemovalDuration != null && overlayRemovalDuration != 0) || overlayRemovalDate != null)
				{
					result.Add(new ReadEventData(	null,
													(int)AT.Models.Events.Types.DeviceRemoved,
													null,
													overlayRemovalDate,
													null,
													null,
													overlayRemovalDuration,
													null,
													true,
													null,
													null,
													false));
				}
				if ((backFlowDuration != null && backFlowDuration != 0) || backFlowDate != null)
				{
					result.Add(new ReadEventData(	"1.1",
													(int)AT.Models.Events.Types.ReverseFlow,
													null,
													backFlowDate,
													null,
													null,
													backFlowDuration,
													null,
													true,
													null,
													null,
													false));
				}
				if (leakDate != null)
				{
					result.Add(new ReadEventData(	"1.1",
													(int)AT.Models.Events.Types.Leakage,
													null,
													leakDate,
													null,
													null,
													null,
													null,
													true,
													null,
													null,
													false));
				}
				if (maximalFlowDate != null)
				{
					result.Add(new ReadEventData(	"1.1",
													(int)AT.Models.Events.Types.MaxFlow,
													null,
													maximalFlowDate,
													null,
													null,
													null,
													null,
													true,
													null,
													null,
													false));
				}

				for(var i=0; i<16; i++)
				{
					AT.Models.Events.Types? alarmType = null;
					var path = (String)null;

					switch (i)
					{
						case 0 :	alarmType = AT.Models.Events.Types.ReverseFlow;					path = "1.1";	break;
						case 1 :	alarmType = AT.Models.Events.Types.MaxFlow;						path = "1.1";	break;
						case 2 :	alarmType = AT.Models.Events.Types.Leakage;						path = "1.1";	break;
						case 3 :	if (read.DeviceType_Id == (int)Metis1.Enums.DeviceTypes.AtWmbus04 ||
										read.DeviceType_Id == (int)Metis1.Enums.DeviceTypes.AtWmbus08 ||
										read.DeviceType_Id == (int)Metis1.Enums.DeviceTypes.AtWmbus09 ||
										read.DeviceType_Id == (int)Metis1.Enums.DeviceTypes.AtWmbus10 ||
										read.DeviceType_Id == (int)Metis1.Enums.DeviceTypes.AtWmbus11)
									{
										alarmType = AT.Models.Events.Types.MeasurementWithoutChange;						
									}break;
						case 4 :	alarmType = AT.Models.Events.Types.DeviceRemoved;					break;
						case 5 :	alarmType = AT.Models.Events.Types.MagneticField;					break;
						case 6 :	alarmType = AT.Models.Events.Types.WorkingTimeExceedBatteryLife;	break;
						case 7 :	alarmType = AT.Models.Events.Types.DetectorsError;					break;
						case 8 :	alarmType = AT.Models.Events.Types.LightExposure;					break;
						case 9 :	alarmType = AT.Models.Events.Types.ResetPor;						break;
						case 10 :	alarmType = AT.Models.Events.Types.ResetExtr;						break;
						case 11 :	alarmType = AT.Models.Events.Types.ResetBor;						break;
						case 12 :	alarmType = AT.Models.Events.Types.ResetIwdg;						break;
						case 13 :	alarmType = AT.Models.Events.Types.LowBatteryState;					break;
						case 14 :	alarmType = AT.Models.Events.Types.MinFlow;						path = "1.1";	break;
						//case 15 :	
					}

					if ((failures & (int)Math.Pow(2,i)) > 0 && alarmType != null && result.All(e => e.EventTypeId != (int)alarmType))
					{
						result.Add(new ReadEventData(path,	(int)alarmType, null, null, null, null, null, null, true, null, null, false));
														
					}
				}
			}
			
			return result;
		}
	}
}
