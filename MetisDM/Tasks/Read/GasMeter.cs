﻿using System.Collections.Generic;
using System.Globalization;
using Metis2.Writter;

namespace MetisDM.Tasks.Read
{
	public static class GasMeter
	{
		public static List<ReadParameterData> GetParameters(Metis1.Read read, bool isLogged)
		{
			var result = new List<ReadParameterData>();

			if (read.oid3 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Energy, read.oid3.Value.ToString(CultureInfo.InvariantCulture), null, null));
			if (read.oid11 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Volume, read.oid11.Value.ToString(CultureInfo.InvariantCulture), null, null));
			if (read.oid12 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Volume, read.oid12.Value.ToString(CultureInfo.InvariantCulture), 1, null));
			if (read.oid13 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.Volume, read.oid13.Value.ToString(CultureInfo.InvariantCulture), 2, null));
			if (read.oid14 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.VolumeCorrected, read.oid14.Value.ToString(CultureInfo.InvariantCulture), null, null));

			return result;
		}

		public static List<ReadEventData> GetEvents(Metis1.Read read)
		{
			var result = new List<ReadEventData>();
			return result;
		}
	}
}
