﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MetisDM.Tasks.Read
{
	public static class ReadIdManager
	{
		private static int _readId;
		private static int _readParameterId;
		private static int _readEventId;
		private static int _readInterfacePathId;
		private static int _doneElements; 
		private static int _lastSourceReadId; 

		private static readonly object ReadIdLock = new object();
		private static readonly object ReadParameterIdLock = new object();
		private static readonly object ReadEventIdLock = new object();
		private static readonly object ReadInterfacePathIdLock = new object();
		private static readonly object DoneElementsLock = new object();
		private static readonly object SourceReadIdLock = new object();

		public static void Init(int lastReadId, int lastParameterId, int lastEventId, int lastInterfacePathId, int lastSourceReadId)
		{
			_readId = lastReadId;
			_readParameterId = lastParameterId;
			_readEventId = lastEventId;
			_readInterfacePathId = lastInterfacePathId;
			_lastSourceReadId = lastSourceReadId;
			_doneElements = 0;
		}

		public static int GetReadId()
		{
			lock (ReadIdLock)
			{
				_readId++;
				return _readId;
			}
		}
		public static int GetReadParameterId()
		{
			lock (ReadParameterIdLock)
			{
				_readParameterId++;
				return _readParameterId;
			}
		}
		public static int GetReadEventId()
		{
			lock (ReadEventIdLock)
			{
				_readEventId++;
				return _readEventId;
			}
		}
		public static int GetReadInterfacePathId()
		{
			lock (ReadInterfacePathIdLock)
			{
				_readInterfacePathId++;
				return _readInterfacePathId;
			}
		}
		public static int IncrementDoneElements()
		{
			lock (DoneElementsLock)
			{
				_doneElements++;
				return _doneElements;
			}
		}
		public static int SetLastSourceReadId(int readId)
		{
			lock (SourceReadIdLock)
			{
				if (readId > _lastSourceReadId)
				{
					_lastSourceReadId = readId;
				}
				return _lastSourceReadId;
			}
		}
	}
}
