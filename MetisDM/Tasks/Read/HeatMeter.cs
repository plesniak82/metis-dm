﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Metis2.Writter;

namespace MetisDM.Tasks.Read
{
	public static class HeatMeter
	{
		public static List<ReadParameterData> GetParameters(Metis1.Read read, bool isLogged)
		{
			var result = new List<ReadParameterData>();

			if (new int?[] {1,3,6}.Contains(read.DeviceInterface))
			{
				if (read.oid1 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.Power, read.oid1.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid3 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.ThermalEnergy, read.oid3.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid4 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.ErrorCode, read.oid4.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid5 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.ExternalTemperature, read.oid5.Value.ToString(CultureInfo.InvariantCulture), null, null));
				//if (read.oid7 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.DeviceDate, read.oid7.Value.ToString(CultureInfo.InvariantCulture), null, null));
				//if (read.oid8 != null) result.Add(new ReadParameterData(null, (int)AT.Models.Parameters.Types.BatteryWorkTime, read.oid8.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid10 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.ReturnTemperature, read.oid10.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid11 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.MaximumFlow, read.oid11.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid13 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.FlowTemperature, read.oid13.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid14 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.MaximumPower, read.oid14.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid16 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.Temperature, read.oid16.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid17 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.Volume, read.oid17.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid18 != null) result.Add(new ReadParameterData("1.2.1", (int)AT.Models.Parameters.Types.Volume, read.oid18.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid19 != null) result.Add(new ReadParameterData("1.3.1", (int)AT.Models.Parameters.Types.Volume, read.oid19.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid20 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.Flow, read.oid20.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid21 != null) result.Add(new ReadParameterData("1", (int)AT.Models.Parameters.Types.CientNumber, read.oid21.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid22 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.MassFlow, read.oid22.Value.ToString(CultureInfo.InvariantCulture), null, null));
				if (read.oid23 != null) result.Add(new ReadParameterData("1.1", (int)AT.Models.Parameters.Types.Mass, read.oid23.Value.ToString(CultureInfo.InvariantCulture), null, null));
			}
			
			return result;
		}

		public static List<ReadEventData> GetEvents(Metis1.Read read)
		{
			var result = new List<ReadEventData>();
			return result;
		}
	}
}
