﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using Metis2.DataTypes;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;

namespace MetisDM.Tasks
{
	public class TransmissionNetworkSchedule : TaskBaseGeneric<Metis1.Schedule, Metis2.Schedule, int, int>
	{
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.TransmissionNetworkSchedule;
			}
		}

		public TransmissionNetworkSchedule(Settings settings, Migrate.Migrate migrate)
			: base(settings, migrate)
		{
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.Schedule.Where(e =>	e.ScheduleMapping.Any(f => f.Device_Id != null));
			MigrationElements = query.ToList();
		}

		protected override Metis2.Schedule AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Metis1.Schedule migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var scheduleMapping = migrationElement.ScheduleMapping.FirstOrDefault();
			var deviceConnection = scheduleMapping != null 
				? scheduleMapping.Device.DeviceConnection.FirstOrDefault()
				: null;
			var transmissionNetworkName = deviceConnection != null
				? deviceConnection.Device1.DeviceNumber
				: null;

			var transmissionNetwork = transmissionNetworkName != null
				? metis2Entity.TransmissionNetwork.FirstOrDefault(e => e.Name == transmissionNetworkName)
				: null;

			var schedule = transmissionNetwork != null
				? transmissionNetwork.Schedule
				: null;

			if (transmissionNetwork != null)
			{
				var scheduleId = new SqlParameter("NewScheduleId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var scheduleItems = new List<ScheduleItemType>
				{
					new ScheduleItemType(
						migrationElement.DayOfMonth > 0 ? migrationElement.DayOfMonth : null,
						migrationElement.DayOfWeek,
						null,
						migrationElement.HourOfDay,
						0,
						migrationElement.TimeInterval != null ? (decimal?) migrationElement.TimeInterval.Value.TotalSeconds : null,
						migrationElement.IsActive,
						null,
						migrationElement.MinuteOfHour,
						null,
						1,
						null,
						schedule != null ? (int) schedule.Id : 0)
				};
				var value = UserDefinedTable.GetDataTable<ScheduleItemType>(scheduleItems);
				try
				{
					Procedure.Run(metis2Entity.Connection, "[schedule].[uspScheduleUpdate]", new[]
					{
						new SqlParameter("UserName", SqlDbType.NVarChar)			{Value = DBNull.Value},
						new SqlParameter("ScheduleId", SqlDbType.NVarChar)			{Value = schedule != null ? (object)schedule.Id : DBNull.Value},
						new SqlParameter("ItemsToDelete", SqlDbType.Structured)		{Value = UserDefinedTable.GetDataTable<IdTableType>(new List<int>()), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("ItemsToInsert", SqlDbType.Structured)		{Value = value, TypeName = "[dbo].[ScheduleItemType]"},									
						scheduleId
					}, resultCode);

					var newScheduleId = scheduleId.Value as int?;
					schedule = metis2Entity.Schedule.FirstOrDefault(e => e.Id == newScheduleId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}
			
			description = String.Format("{0}", transmissionNetworkName);
			return schedule;
		}

		protected override string GetElementStrId(Metis1.Schedule element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Metis2.Schedule element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Metis1.Schedule element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Metis1.Schedule element)
		{
			return element.Id;
		}

		protected override int GetElementId(Metis2.Schedule element)
		{
			return element.Id;
		}
	}
}
