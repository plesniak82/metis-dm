﻿using System;
using System.Linq;
using AT.Utils;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Enums;
using aspnet_Applications = Metis1.aspnet_Applications;

namespace MetisDM.Tasks
{
	class Applications : TaskBaseGeneric<aspnet_Applications, Metis2.aspnet_Applications, Guid, Guid>
	{
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.Application;
			}
		}

		public Applications(Settings settings, Migrate.Migrate migrate)
			: base(settings, migrate)
		{
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			MigrationElements = metis1Entity.aspnet_Applications.ToList();
		}

		protected override Metis2.aspnet_Applications AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, aspnet_Applications migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var newApplication = metis2Entity.aspnet_Applications.FirstOrDefault(e => e.LoweredApplicationName == migrationElement.LoweredApplicationName);
			if (newApplication == null)
			{
				newApplication = new Metis2.aspnet_Applications
				{
					ApplicationId = MathExtension.SequentialGuid(),
					ApplicationName = migrationElement.ApplicationName,
					LoweredApplicationName = migrationElement.ApplicationName.ToLower(),
					Description = migrationElement.Description
				};

				try
				{
					metis2Entity.AddToaspnet_Applications(newApplication);
					metis2Entity.SaveChanges();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}

			description = migrationElement.ApplicationName;
			return newApplication;
		}

		protected override string GetElementStrId(aspnet_Applications element)
		{
			return element.ApplicationId.ToString();
		}

		protected override string GetElementStrId(Metis2.aspnet_Applications element)
		{
			return element.ApplicationId.ToString();
		}

		protected override string GetElementName(aspnet_Applications element)
		{
			return element.ApplicationName;
		}

		protected override Guid ParseMetis1ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid ParseMetis2ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid GetElementId(aspnet_Applications element)
		{
			return element.ApplicationId;
		}

		protected override Guid GetElementId(Metis2.aspnet_Applications element)
		{
			return element.ApplicationId;
		}
	}
}
