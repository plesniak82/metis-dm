﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using aspnet_Users = Metis1.aspnet_Users;
using vw_aspnet_Users = Metis2.vw_aspnet_Users;

namespace MetisDM.Tasks
{
	public class Users : TaskBaseGeneric<aspnet_Users, vw_aspnet_Users, Guid, Guid>
	{
		public Users(Settings settings, Migrate.Migrate migrate)
			: base(settings, migrate)
		{
		}

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.User;
			}
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.aspnet_Users.Where(e => true);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				var userIds = metis1Entity.ClientUsers.Where(e => Settings.ClientIds.Contains(e.ClientId)).Select(e => e.UserId).ToList();
				query = query.Where(e => userIds.Contains(e.UserId));
			}
			MigrationElements = query.ToList();
		}

		protected override vw_aspnet_Users AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, aspnet_Users migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var newUser = metis2Entity.vw_aspnet_Users.FirstOrDefault(e => e.LoweredUserName == migrationElement.LoweredUserName);
			if (newUser == null)
			{
				//parsowanie profilu uzytkownika aspnet_Porfile w celu wydobycia informacji o nim
				String comment = null;
				String firstName = null;
				String lastName = null;
				String phone = null;

				var profile = migrationElement.aspnet_Profile;
				if (profile == null)
				{
					throw new Exception("Warning: user's profile not found in source database");
				}

				var profileSections = profile != null ? profile.PropertyNames.Split(':') : new string[] { };
				for (int i = 0; i < profileSections.Count(); i += 4)
				{
					if (profileSections.Count() < i + 3)
					{
						break;
					}

					var index = -1;
					var length = -1;

					Int32.TryParse(profileSections[i + 2], out index);
					Int32.TryParse(profileSections[i + 3], out length);

					if (index > -1 && length > 0)
					{
						if (profileSections[i] == "Comment")
							comment = profile.PropertyValuesString.Substring(index, length);
						if (profileSections[i] == "FirstName")
							firstName = profile.PropertyValuesString.Substring(index, length);
						if (profileSections[i] == "LastName")
							lastName = profile.PropertyValuesString.Substring(index, length);
						if (profileSections[i] == "Phone")
							phone = profile.PropertyValuesString.Substring(index, length);
					}
				}

				if (firstName == null)
				{
					firstName = "Jan";
					//throw new Exception("Warning: user's first name not specified");
				}
				if (lastName == null)
				{
					lastName = "Kowalski";
					//throw new Exception("Warning: user's last name not specified");
				}

				//pobranie nazwy aplikacji
				var applicationName = migrationElement.aspnet_Applications.ApplicationName;
				//pobranie listy nazw rol
				var roleNames = String.Join(",", migrationElement.aspnet_Roles.Select(e => e.RoleName).ToList());
				//pobranie PropertyNamesString
				var propertyNamesString = migrationElement.aspnet_Profile.PropertyValuesString;
				//pobranie PropertyValuesString
				var propertyValuesString = migrationElement.aspnet_Profile.PropertyValuesString;
				//pobranie Client
				var migrationClients = migrationElement.ClientUsers.Select(e => e.Client).ToList();
				var migrationClient = migrationClients.FirstOrDefault();
				var migrationClientName = migrationClient != null ? migrationClient.Name : null;
				var migrationClientNames = migrationClients.Select(e => e.Name).ToList();
				var vwClient = migrationClientName != null ? metis2Entity.vwClient.FirstOrDefault(e => e.Name == migrationClientName) : null;
				var addedClients = metis2Entity.vwClient.Where(e => migrationClientNames.Contains(e.Name)).ToList();
				var addedClientIds = addedClients.Select(e => e.Id).ToList();

				var isSystemAdministrator = roleNames.Contains("SystemAdministrator");
				var isClientAdministrator = roleNames.Contains("ClientAdministrator");

				if (vwClient == null && !isClientAdministrator && !isSystemAdministrator)
				{
					throw new Exception("Warning: neither user's client found nor user is system administrator nor client administrator");
				}

				if (!addedClientIds.Any() && isClientAdministrator && !isSystemAdministrator)
				{
					throw new Exception("Warning: user has no underlying clients though being client administrator");
				}

				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[access].[uspUserAdd]", new[]
					{
						new SqlParameter("ApplicationName", SqlDbType.NVarChar)			{Value = applicationName},
						new SqlParameter("UserName", SqlDbType.NVarChar)				{Value = migrationElement.UserName},
						new SqlParameter("FirstName", SqlDbType.NVarChar)				{Value = firstName},
						new SqlParameter("LastName", SqlDbType.NVarChar)				{Value = lastName},
						new SqlParameter("Comment", SqlDbType.NText)					{Value = comment != null ? (object)comment : DBNull.Value},
						new SqlParameter("Email", SqlDbType.NVarChar)					{Value = migrationElement.aspnet_Membership.Email},
						//new SqlParameter("AddressLine1", SqlDbType.NVarChar)			{Value = DBNull.Value},
						//new SqlParameter("AddressLine2", SqlDbType.NVarChar)			{Value = DBNull.Value},
						new SqlParameter("Phone", SqlDbType.NVarChar)					{Value = phone != null ? (object)phone : DBNull.Value},
						new SqlParameter("Password", SqlDbType.NVarChar)				{Value = migrationElement.aspnet_Membership.Password},
						new SqlParameter("PassordSalt", SqlDbType.NVarChar)				{Value = migrationElement.aspnet_Membership.PasswordSalt},
						new SqlParameter("RolesNames", SqlDbType.NVarChar)				{Value = roleNames},
						new SqlParameter("IsSendingFilesByWebservice", SqlDbType.Bit)	{Value = 0},
						new SqlParameter("DefaultLanguage", SqlDbType.NVarChar)			{Value = DBNull.Value},
						new SqlParameter("IsClientAdministrator", SqlDbType.Bit)		{Value = !isSystemAdministrator && isClientAdministrator},
						new SqlParameter("IsSystemAdministrator", SqlDbType.Bit)		{Value = isSystemAdministrator},
						new SqlParameter("PropertyNamesString", SqlDbType.NVarChar)		{Value = propertyNamesString},
						new SqlParameter("PropertyValuesString", SqlDbType.NVarChar)	{Value = propertyValuesString},
						new SqlParameter("RemovedClients", SqlDbType.Structured)		{Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("AddedClients", SqlDbType.Structured)			{Value = UserDefinedTable.GetDataTable(addedClientIds.ToArray()), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("RemovedPermissions", SqlDbType.Structured)	{Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("AddedPermissions", SqlDbType.Structured)		{Value = UserDefinedTable.GetDataTable(new int[0]), TypeName = "[dbo].[IdTableType]"},
						new SqlParameter("ClientId", SqlDbType.Int)						{Value = vwClient != null ? (object)vwClient.Id : DBNull.Value},
						new SqlParameter("HasOnlyStaticRules", SqlDbType.Bit)			{Value = 0}
					}, resultCode);

					newUser = metis2Entity.vw_aspnet_Users.FirstOrDefault(e => e.LoweredUserName == migrationElement.LoweredUserName);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = migrationElement.UserName;
			return newUser;
		}

		protected override string GetElementStrId(aspnet_Users element)
		{
			return element != null ? element.UserId.ToString() : Guid.Empty.ToString();
		}

		protected override string GetElementStrId(vw_aspnet_Users element)
		{
			return element != null ? element.UserId.ToString() : Guid.Empty.ToString();
		}

		protected override string GetElementName(aspnet_Users element)
		{
			return element.UserName;
		}

		protected override Guid ParseMetis1ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid ParseMetis2ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override Guid GetElementId(aspnet_Users element)
		{
			return element.UserId;
		}

		protected override Guid GetElementId(vw_aspnet_Users element)
		{
			return element.UserId;
		}
	}
}
