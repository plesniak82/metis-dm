﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Metis1;
using Metis2;
using MetisDM.Classes;

namespace MetisDM.Tasks
{
	public abstract class TaskBaseGeneric<T1, T2, T3, T4> : TaskBase, IDisposable
	{
		public Dictionary<T3, KeyValuePair<T4, String>> Mapping;
		protected List<T1> MigrationElements;
		protected int DoneElements;
		protected int TotalElements;

		protected TaskBaseGeneric(Settings settings, Migrate.Migrate migrate, bool onlyReadProgress = false)
			: base(settings, migrate, onlyReadProgress)
		{
			if (settings == null)
				throw new ArgumentNullException("settings");
			if (migrate == null)
				throw new ArgumentNullException("migrate");

			Mapping = new Dictionary<T3, KeyValuePair<T4, string>>();
		}

		~TaskBaseGeneric()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (Mapping != null)	Mapping.Clear();
				if (MigrationElements != null)	MigrationElements.Clear();
			}

			Mapping = null;
			MigrationElements = null;
		}

		//wykonanie importu		
		public override void ParseLogContent()
		{
			foreach (var line in LogContent)
			{
				if (line.Count >= 4)
				{
					if (line[1] != (-1).ToString(CultureInfo.InvariantCulture) && line[1] != Guid.Empty.ToString())
					{
						var id1 = ParseMetis1ElementId(line[1]);
						var id2 = ParseMetis2ElementId(line[2]);

						if (!Mapping.ContainsKey(id1))
						{
							Mapping.Add(id1, new KeyValuePair<T4, string>(id2, line[3]));
						}
						else
						{
							Console.WriteLine(String.Format("parsing file error - duplicate key: {0}", id1));
						}
					}
				}
			}
		}
		protected override void ExecuteRun(Metis1Entities metis1Entity, Metis2Entities metis2Entity)
		{
			SetMigrationElements(metis1Entity);

			DoneElements = 0;
			TotalElements = MigrationElements.Count();
			RaiseProgressChanged(DoneElements, TotalElements);
			foreach (var migrationElement in MigrationElements)
			{
				try
				{
					if (!Mapping.ContainsKey(GetElementId(migrationElement)) || Mapping[GetElementId(migrationElement)].Key.ToString() == "-1")
					{
						String description;
						var newElement = AddElement(metis1Entity, metis2Entity, migrationElement, out description);
						if (newElement != null)
						{
							WriteToLogFile(migrationElement, newElement, description, false);
						}
					}
					else
					{
						WriteToLogFile(GetElementStrId(migrationElement), Mapping[GetElementId(migrationElement)].Key.ToString(), Mapping[GetElementId(migrationElement)].Value.TrimEnd(), false);
					}
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (ex.Message.Contains("Warning"))
					{
						AddWarning(migrationElement, ex.Message);
					}
					else
					{
						Debug.WriteLine("-----------\nERRROR\n----------------\n\n{0}\n\n----------------------------------\n", ex);
						AddError(migrationElement, ex.Message);
					}
				}

				RaiseProgressChanged(++DoneElements, TotalElements);
			}

			MigrationElements.Clear();
			MigrationElements = null;
		}
		protected abstract void SetMigrationElements(Metis1Entities metis1Entity);
		protected abstract T2 AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, T1 migrationElement, out string description);
		protected abstract String GetElementStrId(T1 element);
		protected abstract String GetElementStrId(T2 element);
		protected abstract T3 GetElementId(T1 element);
		protected abstract T4 GetElementId(T2 element);
		protected abstract String GetElementName(T1 element);
		protected abstract T3 ParseMetis1ElementId(String str);
		protected abstract T4 ParseMetis2ElementId(String str);


		//zapisywanie logow do pliku
		protected void WriteToLogFile(T1 migrationElement, T2 newElement, String description, bool progressOnly)
		{
			WriteToLogFile(GetElementStrId(migrationElement), GetElementStrId(newElement), description, progressOnly);
			if (Mapping.ContainsKey(GetElementId(migrationElement)))
			{
				Mapping[GetElementId(migrationElement)] = new KeyValuePair<T4, string>(GetElementId(newElement), Mapping[GetElementId(migrationElement)].Value);
			}
			else
			{
				Mapping.Add(GetElementId(migrationElement), new KeyValuePair<T4, string>(GetElementId(newElement), description));
			}
		}
		protected void WriteToLogFile(String metis1Id, String metis2Id, String description, bool progressOnly)
		{
			if (!Directory.Exists(Settings.LogFolder))
			{
				Directory.CreateDirectory(Settings.LogFolder);
			}

			if (!progressOnly)
			{
				File.AppendAllLines(LogFilename, new[] { String.Format("{0};{1};{2};{3}", MigrationTaskType, metis1Id, metis2Id, description) });
			}

			File.WriteAllText(ProgressFilename, String.Format("{0};{1};{2};{3}", MigrationTaskType, metis1Id, metis2Id, description));
		}

		protected void AddWarning(T1 metis1Id, String warning)
		{
			if (Mapping.ContainsKey(GetElementId(metis1Id)))
			{
				Mapping[GetElementId(metis1Id)] = new KeyValuePair<T4, string>(ParseMetis2ElementId("-1"), warning);
			}
			else
			{
				Mapping.Add(GetElementId(metis1Id), new KeyValuePair<T4, string>(ParseMetis2ElementId("-1"), warning));
			}

			WriteToLogFile(GetElementStrId(metis1Id), "-1", String.Format("({0}): {1}", GetElementName(metis1Id), warning), false);
			AddWarning(warning);
		}
		protected void AddError(T1 metis1Id, String error)
		{
			if (Mapping.ContainsKey(GetElementId(metis1Id)))
			{
				Mapping[GetElementId(metis1Id)] = new KeyValuePair<T4, string>(ParseMetis2ElementId("-1"), error);
			}
			else
			{
				Mapping.Add(GetElementId(metis1Id), new KeyValuePair<T4, string>(ParseMetis2ElementId("-1"), error));
			}

			WriteToLogFile(GetElementStrId(metis1Id), "-1", String.Format("({0}): {1}", GetElementName(metis1Id), error), false);
			//AddError(error);
		}
	}
}
