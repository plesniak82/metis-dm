﻿using System;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Enums;
using aspnet_Users = Metis1.aspnet_Users;

namespace MetisDM.Tasks
{
	public class UserMeasurementPoints : TaskBaseGeneric<aspnet_Users, SelectionRuleSet, Guid, int>
	{
		private readonly Clients _clients;
		private readonly Users _users;
		private readonly MeasurementPoints _measurementPoints;

		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.UserMeasurementPoint;
			}
		}

		public UserMeasurementPoints(Settings settings, Migrate.Migrate migrate, Clients clients, Users users, MeasurementPoints measurementPoints)
			: base(settings, migrate)
		{
			if (clients == null) throw new ArgumentNullException("clients");
			if (users == null) throw new ArgumentNullException("users");
			if (measurementPoints == null) throw new ArgumentNullException("_measurementPoints");

			_clients = clients;
			_users = users;
			_measurementPoints = measurementPoints;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.aspnet_Users.Where(e => true);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				var userIds = metis1Entity.ClientUsers.Where(e => Settings.ClientIds.Contains(e.ClientId)).Select(e => e.UserId).ToList();
				query = query.Where(e => userIds.Contains(e.UserId));
			}
			MigrationElements = query.ToList();
		}

		protected override SelectionRuleSet AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, aspnet_Users migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var userId = _users.Mapping.ContainsKey(migrationElement.UserId)
									? _users.Mapping[migrationElement.UserId].Key
									: Guid.Empty;

			if (userId == null || userId == Guid.Empty)
			{
				throw new Exception(String.Format("Warning: measurementPoint's user ({0}) not found", migrationElement.UserName));
			}

			var user = metis2Entity.User.FirstOrDefault(e => e.UserId == userId);
			if (user == null)
			{
				throw new Exception(String.Format("Warning: measurementPoint's asp net user ({0}) not found", migrationElement.UserName));
			}
#if  DEBUG
			if (user.Id == 18)
			{
				Console.Write("pkl");
			}
#endif

			SelectionRuleSet newSelectionRuleSet = null;
			var clients = metis2Entity.ClientUser.Where(e => e.User_Id == user.Id).ToList();
			foreach (var client in clients)
			{
				newSelectionRuleSet = metis2Entity.UserSelectionRuleSet.Where(e => e.User_Id == user.Id && e.SelectionRuleSet.Client_Id == client.Id).Select(e => e.SelectionRuleSet).FirstOrDefault();
				if (newSelectionRuleSet == null)
				{
					newSelectionRuleSet = new SelectionRuleSet
					{
						Client_Id = client.Id,
						HasOnlyStaticRules = false,
						AllPointsForClient = false
					};
					metis2Entity.AddToSelectionRuleSet(newSelectionRuleSet);
					metis2Entity.SaveChanges();
				}

				if (newSelectionRuleSet != null)
				{
					string newSelectionRuleName = String.Format("UserSelectionRuleSet{0}_{1}", client.Id, user.Id);
					SelectionRule newSelectionRule = null;

					newSelectionRule = metis2Entity.SelectionRule.FirstOrDefault(e => e.Name == newSelectionRuleName);
					if (newSelectionRule == null)
					{
						newSelectionRule = new SelectionRule
						{
							Name = newSelectionRuleName,
							SelectionRuleSet = newSelectionRuleSet,
							IsStatic = true,
							IsActive = true
						};
						metis2Entity.AddToSelectionRule(newSelectionRule);
						metis2Entity.SaveChanges();
					}

					var srcgroupIds = metis1Entity.UsersInGroups.Where(e => e.User_Id == migrationElement.UserId).Select(e => e.Group_Id).ToList();
					var srcuserGroupIds = metis1Entity.UsersGroup.Where(e => srcgroupIds.Contains(e.Id)).Select(e => e.Id).ToList();
					var srcmeasurementPoints = metis1Entity.UsersGroupMeasurementPoint.Where(e => srcuserGroupIds.Contains(e.UsersGroup_Id)).Select(e => e.MeasurementPoint).ToList();

					foreach (var measurementPoint in srcmeasurementPoints)
					{
						var descMeasurementPointId = _measurementPoints.Mapping.ContainsKey(measurementPoint.Id)
														? _measurementPoints.Mapping[measurementPoint.Id].Key
														: (int?)null;

						if (descMeasurementPointId != null)
						{
							SelectionRuleMeasurementPoint selectionRuleMeasurementPoint = null;

							selectionRuleMeasurementPoint =
								metis2Entity.SelectionRuleMeasurementPoint.FirstOrDefault(e => e.SelectionRule_Id == newSelectionRule.Id &&
								                                                               e.MeasurementPoint_Id == descMeasurementPointId
									                                                               .Value);

							if (selectionRuleMeasurementPoint == null)
							{
								metis2Entity.AddToSelectionRuleMeasurementPoint(new SelectionRuleMeasurementPoint
								{
									SelectionRule = newSelectionRule,
									MeasurementPoint_Id = descMeasurementPointId.Value
								});
								metis2Entity.SaveChanges();
							}
						}
					}

					metis2Entity.SaveChanges();

					srcgroupIds.Clear();
					srcuserGroupIds.Clear();
					srcmeasurementPoints.Clear();
				}
			}
			clients.Clear();

			description = migrationElement.UserName;

			if (newSelectionRuleSet != null)
			{
				return newSelectionRuleSet;
			}
			else
			{
				throw new Exception(String.Format("Warning: no selection rule set imported for ({0}) not found", migrationElement.UserName));
			}

		}

		protected override string GetElementStrId(aspnet_Users element)
		{
			return element.UserId.ToString();
		}

		protected override string GetElementStrId(SelectionRuleSet element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(aspnet_Users element)
		{
			return element.UserName;
		}

		protected override Guid ParseMetis1ElementId(string str)
		{
			Guid result;
			if (!Guid.TryParse(str, out result))
			{
				return Guid.Empty;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override Guid GetElementId(aspnet_Users element)
		{
			return element.UserId;
		}

		protected override int GetElementId(SelectionRuleSet element)
		{
			return element.Id;
		}
	}
}
