﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;
using Device = Metis2.Device;

namespace MetisDM.Tasks
{
	public class MeasurementDevices : TaskBaseGeneric<Meter, Device, int, int>
	{
		private readonly DeviceTypes _deviceTypes;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.MeasurementDevice;
			}
		}

		public MeasurementDevices(Settings settings, Migrate.Migrate migrate, DeviceTypes deviceTypes)
			: base(settings, migrate)
		{
			if (deviceTypes == null)
				throw new ArgumentNullException("deviceTypes");

			_deviceTypes = deviceTypes;
		}


		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			//Meter.MeterModel.MeterType_Id == (int)Metis1.Enums.MeterTypes.HeatCostAllocator
			var query = metis1Entity.Meter.Where(e =>	e.MeterModel.MeterType_Id != (int)Metis1.Enums.MeterTypes.HeatCostAllocator &&
														e.MeterModel.MeterType_Id != (int)Metis1.Enums.MeterTypes.GasMeter);
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.Client_Id != null && Settings.ClientIds.Contains((int)e.Client_Id));
			}
			MigrationElements = query.ToList();
		}

		protected override Device AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Meter migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var deviceTypeId = _deviceTypes.Mapping.ContainsKey(migrationElement.MeterModel_Id)
									? _deviceTypes.Mapping[migrationElement.MeterModel_Id].Key
									: (int?)null;
			if (deviceTypeId == null || deviceTypeId == -1)
			{
				throw new Exception(String.Format("Warning: source device type ({0}) not found in destination database", migrationElement.MeterModel.Name));
			}

			var newDevice = metis2Entity.Device.FirstOrDefault(e => e.ConnectionId == migrationElement.ManufacturerNumber &&
																	e.DeviceType_Id == deviceTypeId);
																	
			if (newDevice == null)
			{
				var deviceId = new SqlParameter("DeviceId", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)
				{
					Direction = ParameterDirection.Output
				};
				try
				{
					var trimmedManufacturerNumber = migrationElement.ManufacturerNumber.Trim();

					Procedure.Run(metis2Entity.Connection, "[device].[uspDeviceAdd]", new[]
					{
						new SqlParameter("DeviceTypeId", SqlDbType.Int)					{Value = deviceTypeId},
						new SqlParameter("DeviceModelId", SqlDbType.Int)				{Value = DBNull.Value},
						new SqlParameter("ConnectionIdNumber", SqlDbType.NVarChar)		{Value = trimmedManufacturerNumber},
						new SqlParameter("SerialNumber", SqlDbType.NVarChar)			{Value = trimmedManufacturerNumber},
						new SqlParameter("UserId", SqlDbType.Int)						{Value = DBNull.Value},
						new SqlParameter("AddressNodeId", SqlDbType.Int)				{Value = DBNull.Value},
						new SqlParameter("InteraceToLocalization", SqlDbType.Int)		{Value = DBNull.Value},
						deviceId
					}, resultCode);

					var newDeviceId = deviceId.Value as int?;
					newDevice = metis2Entity.Device.FirstOrDefault(e => e.Id == newDeviceId.Value);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}
			}

			description = migrationElement.ManufacturerNumber;
			return newDevice;
		}

		protected override string GetElementStrId(Meter element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(Device element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Meter element)
		{
			return element.ManufacturerNumber;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Meter element)
		{
			return element.Id;
		}

		protected override int GetElementId(Device element)
		{
			return element.Id;
		}
	}
}
