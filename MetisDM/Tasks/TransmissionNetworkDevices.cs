﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Database;
using MetisDM.Enums;

namespace MetisDM.Tasks
{
	public class TransmissionNetworkDevices : TaskBaseGeneric<Metis1.Device, TransmissionNetworkDevice, int, int>
	{
		private readonly Clients _clients;
		private readonly TransmissionDevices _transmissionDevices;
		public override MigrationTaskType MigrationTaskType
		{
			get
			{
				return MigrationTaskType.TransmissionNetworkDevice;
			}
		}

		public TransmissionNetworkDevices(Settings settings, Migrate.Migrate migrate, Clients clients, TransmissionDevices transmissionDevices)
			: base(settings, migrate)
		{
			if (clients == null)
				throw new ArgumentNullException("clients");
			if (transmissionDevices == null)
				throw new ArgumentNullException("transmissionDevices");

			_clients = clients;
			_transmissionDevices = transmissionDevices;
		}

		protected override void SetMigrationElements(Metis1Entities metis1Entity)
		{
			var query = metis1Entity.Device.Where(e => new[]	{
																	(int) Metis1.Enums.DeviceTypes.AtKEthRs232,
																	(int) Metis1.Enums.DeviceTypes.AtKGsmRs232,
																	(int) Metis1.Enums.DeviceTypes.AtKGsmRs2323Gth,
																	(int) Metis1.Enums.DeviceTypes.AtUptGsm01,
																	(int) Metis1.Enums.DeviceTypes.AtUptGsm01Rs,
																}.Contains(e.DeviceType_Id));
			
			if (Settings.ClientIds != null && Settings.ClientIds.Any())
			{
				query = query.Where(e => e.DeviceActivation.Any(f => Settings.ClientIds.Contains(f.Client_Id)));
			}
			
			MigrationElements = query.ToList();
		}

		protected override TransmissionNetworkDevice AddElement(Metis1Entities metis1Entity, Metis2Entities metis2Entity, Metis1.Device migrationElement, out string description)
		{
			if (migrationElement == null)
				throw new ArgumentNullException("migrationElement");

			var deviceId = _transmissionDevices.Mapping.ContainsKey(migrationElement.Id)
									? _transmissionDevices.Mapping[migrationElement.Id].Key
									: (int?)null;
			if (deviceId == null || deviceId == -1)
			{
				throw new Exception(String.Format("Warning: device network's device ({0}) not found", migrationElement.DeviceNumber));
			}

			var deviceActivation = metis2Entity.Activation.FirstOrDefault(e => e.ClientResourceType_Id == 2 && e.ClientResource_Id == deviceId);
			if (deviceActivation == null)
			{
				throw new Exception(String.Format("Warning: device's activation ({0}) not found", migrationElement.DeviceNumber));
			}

			var clientId = deviceActivation.Client_Id;
			if (clientId == -1)
			{
				throw new Exception(String.Format("Warning: device networks's client ({0}) not found", migrationElement.DeviceNumber));
			}

			var transmissionNetwork = metis2Entity.TransmissionNetwork.FirstOrDefault(e => e.Name == migrationElement.DeviceNumber);
			if (transmissionNetwork == null)
			{
				throw new Exception(String.Format("Warning: device's network ({0}) not found", migrationElement.DeviceNumber));
			}

			var transmissionNetworkDevice = metis2Entity.TransmissionNetworkDevice.FirstOrDefault(e => e.Device_Id == deviceId &&
			                                                                                           e.TransmissionNetwork_Id == transmissionNetwork.Id);
			if (transmissionNetworkDevice == null)
			{
				var deviceConnectionId = new SqlParameter("DeviceConnectionId", SqlDbType.Int)	{	Direction = ParameterDirection.Output	};
				var resultCode = new SqlParameter("ResultCode", SqlDbType.Int)	{	Direction = ParameterDirection.Output	};
				try
				{
					Procedure.Run(metis2Entity.Connection, "[network].[uspTransmissionNetworkDeviceAddRoot]", new[]
					{
						new SqlParameter("TransmissionNetworkId", SqlDbType.Int)		{	Value = transmissionNetwork.Id	},
						new SqlParameter("DeviceId", SqlDbType.Int)						{	Value = deviceId	},
						new SqlParameter("ClientId", SqlDbType.Int)						{	Value = clientId	},
						new SqlParameter("UserId", SqlDbType.Int)						{	Value = DBNull.Value	},
						deviceConnectionId
					}, resultCode)
					;
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.ToString());
					if (resultCode.Value as int? < 0)
					{
						throw new Exception(String.Format("Warning: {0}", ErrorCodeToMsg(resultCode.Value as int?)));
					}
					throw;
				}

				transmissionNetworkDevice = metis2Entity.TransmissionNetworkDevice.FirstOrDefault(e => e.Device_Id == deviceId &&
			                                                                                           e.TransmissionNetwork_Id == transmissionNetwork.Id);
			}
			
			description = String.Format("{0}", migrationElement.DeviceNumber);
			return transmissionNetworkDevice;
		}

		protected override string GetElementStrId(Metis1.Device element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementStrId(TransmissionNetworkDevice element)
		{
			return element.Id.ToString(CultureInfo.InvariantCulture);
		}

		protected override string GetElementName(Metis1.Device element)
		{
			return null;
		}

		protected override int ParseMetis1ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int ParseMetis2ElementId(string str)
		{
			int result;
			if (!int.TryParse(str, out result))
			{
				return -1;
			}
			return result;
		}

		protected override int GetElementId(Metis1.Device element)
		{
			return element.Id;
		}

		protected override int GetElementId(TransmissionNetworkDevice element)
		{
			return element.Id;
		}
	}
}
