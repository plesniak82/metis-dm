﻿using Metis1;
using Metis2;
using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Clients = MetisDM.Tasks.Clients;
using DeviceTypes = MetisDM.Tasks.DeviceTypes;
using MessageBox = Xceed.Wpf.Toolkit.MessageBox;

namespace MetisDM.Migrate
{
	public class Migrate
	{
		#region Delegates

		public delegate void ProgressChangedEventHandler(object sender, MigrationTaskType installationStep, TaskState taskState, int done, int total);

		public delegate void CompletedEventHandler(object sender);

		#endregion Delegates

		#region Events

		public event ProgressChangedEventHandler ProgressChanged;

		public event EventHandler CancelCompleted;

		public event CompletedEventHandler Completed;

		#endregion Events

		#region Properties

		public bool IsBusy { get; set; }

		public bool HasErrors
		{
			get
			{
				return MigrationTasks.Any(e => e.GetErrors().Any());
			}
		}

		public bool HasWarnings
		{
			get
			{
				return MigrationTasks.Any(e => e.GetWarnings().Any());
			}
		}

		public List<TaskBase> MigrationTasks { get; private set; }

		protected TaskBase CurrentMigrationTask
		{
			get
			{
				return _currentMigrationTask;
			}
			set
			{
				_currentMigrationTask = value;
				Dispatcher.CurrentDispatcher.Invoke(new Action(
					() =>
					{
						if (ProgressChanged != null)
						{
							ProgressChanged(this, CurrentMigrationTask.MigrationTaskType, CurrentMigrationTask.State, 0, 0);
						}
					}
					));
			}
		}

		#endregion Properties

		#region Fields

		public List<vwErrorCodeDefaultValues> ErrorCodeDefaultValues;
		internal List<ReadType> ReadTypeDict;
		protected BackgroundWorker Worker;
		protected Settings Settings;
		private TaskBase _currentMigrationTask;

		#endregion Fields

		#region Constructors

		public Migrate(Settings settings)
		{
			Settings = settings;
			Worker = new BackgroundWorker { WorkerReportsProgress = true };
			Worker.ProgressChanged += WorkerProgressChanged;
			Worker.RunWorkerCompleted += RunWorkerCompleted;
			Worker.WorkerSupportsCancellation = true;
			Worker.DoWork += WorkerDoWork;

			bool emptyReadTable;
			using (var ent = new Metis2Entities(settings.TargetDatabaseConnectionString, 3000))
			{
				ErrorCodeDefaultValues = ent.vwErrorCodeDefaultValues.ToList();
				emptyReadTable = !ent.Read.Any();
			}

			var clientTask = new Clients(Settings, this, !Settings.MigrateStructure);
			var applicationTask = new Applications(Settings, this);
			var roleTask = new Roles(Settings, this);
			var userTask = new Users(Settings, this);
			var addressNodeTask = new AddressNodes(Settings, this, clientTask, !Settings.MigrateStructure);
			var deviceTypeTask = new DeviceTypes(Settings, this, clientTask);
			var transmissionDeviceTask = new TransmissionDevices(Settings, this, clientTask, addressNodeTask, !Settings.MigrateStructure);
			var measurementDeviceTask = new MeasurementDevices(Settings, this, deviceTypeTask);
			var transmissionDeviceActivationTask = new TransmissionDeviceActivations(Settings, this, clientTask, transmissionDeviceTask);
			var transmissionNetworkTask = new TransmissionNetworks(Settings, this, clientTask, transmissionDeviceTask);
			var transmissionNetworkSchedule = new TransmissionNetworkSchedule(Settings, this);
			var transmissionNetworkDeviceTask = new TransmissionNetworkDevices(Settings, this, clientTask, transmissionDeviceTask);
			var measurementDeviceActivationTask = new MeasurementDeviceActivations(Settings, this, clientTask, transmissionDeviceTask, measurementDeviceTask);
			var measurementPointTask = new MeasurementPoints(Settings, this, clientTask, addressNodeTask);
			var userMeasurementPointTask = new UserMeasurementPoints(Settings, this, clientTask, userTask, measurementPointTask);
			var montageTask = new Montages(Settings, this, transmissionDeviceTask, measurementDeviceTask, measurementPointTask);
			var transmissionDeviceConnectionTask = new TransmissionDeviceConnections(Settings, this, clientTask, transmissionDeviceTask);
			var measurementDeviceConnectionTask = new MeasurementDeviceConnections(Settings, this, transmissionDeviceTask, measurementDeviceTask);

			MigrationTasks = new List<TaskBase>();
			if (Settings.MigrateStructure)
			{
				MigrationTasks.Add(clientTask);
				MigrationTasks.Add(applicationTask);
				MigrationTasks.Add(roleTask);
				MigrationTasks.Add(userTask);
				MigrationTasks.Add(addressNodeTask);
				MigrationTasks.Add(deviceTypeTask);
				MigrationTasks.Add(transmissionDeviceTask);
				MigrationTasks.Add(measurementDeviceTask);
				MigrationTasks.Add(transmissionDeviceActivationTask);
				MigrationTasks.Add(transmissionNetworkTask);
				MigrationTasks.Add(transmissionNetworkSchedule);
				MigrationTasks.Add(transmissionNetworkDeviceTask);
				MigrationTasks.Add(measurementDeviceActivationTask);
				MigrationTasks.Add(measurementPointTask);
				MigrationTasks.Add(userMeasurementPointTask);
				MigrationTasks.Add(montageTask);
				MigrationTasks.Add(transmissionDeviceConnectionTask);
				MigrationTasks.Add(measurementDeviceConnectionTask);
				//MigrationTasks.Add(installationSetTask);
			}
			if (Settings.MigrateReads)
			{
				if (!Settings.MigrateStructure)
				{
					MigrationTasks.Add(clientTask);
					MigrationTasks.Add(addressNodeTask);
					MigrationTasks.Add(transmissionDeviceTask);
					MigrationTasks.Add(transmissionDeviceActivationTask);
					MigrationTasks.Add(transmissionNetworkDeviceTask);
					MigrationTasks.Add(transmissionDeviceConnectionTask);
				}
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtUptGsm01));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtUptGsm01Rs));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus01));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus04));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus05));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus07));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus08));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus09));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus10));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus11));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus112));
				//MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus12));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus161));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbus162));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr01));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr02));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr03));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr04));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr05));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr06));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr07));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.AtWmbusMr09));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.Eitn305));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.Eitn3051));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.Uniflo_GxE));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.Uniflo_GxS));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.Unismart));
				MigrationTasks.Add(new Reads(Settings, this, transmissionDeviceTask, emptyReadTable, Metis1.Enums.DeviceTypes.CnRe5));
			}
		}

		#endregion Constructors

		#region Methods

		public void CheckLogDirectory()
		{
			var folder = String.Format(@"{0}\[{1};{2}]-[{3};{4}]", Settings.LogFolder,
																	Settings.SourceDatabaseServerAddress,
																	Settings.SourceDatabaseName,
																	Settings.TargetDatabaseServerAddress,
																	Settings.TargetDatabaseName);
			if (!Directory.Exists(folder))
			{
				Directory.CreateDirectory(folder);
			}
		}

		public void Run()
		{
			if (!IsBusy)
			{
				IsBusy = true;
				LoadDictionaries();
				Worker.RunWorkerAsync();
			}
		}

		public void Cancel()
		{
			if (Worker.IsBusy)
			{
				Worker.CancelAsync();
			}
			else
			{
				if (CancelCompleted != null)
				{
					CancelCompleted(this, null);
				}
			}
		}

		public virtual void Dispose()
		{
			if (Worker.IsBusy)
			{
				Cancel();
			}
			else
			{
				if (Worker != null)
				{
					Worker.Dispose();
					Worker = null;
				}
				_currentMigrationTask = null;

				CancelCompleted = null;
				Completed = null;
				ProgressChanged = null;
			}
		}

		protected void WorkerDoWork(object sender, DoWorkEventArgs e)
		{
			LoopTasks(e);
		}

		protected void LoopTasks(DoWorkEventArgs e)
		{
			var migrationTasks = MigrationTasks.Where(f => f != null).OrderBy(s => (int)s.MigrationTaskType).ToList();
			foreach (var migrationTask in migrationTasks)
			{
				migrationTask.State = TaskState.InProgress;
				CurrentMigrationTask = migrationTask;
				migrationTask.ProgressChanged += (s, p, r) => WorkerProgressChanged(s, new ProgressChangedEventArgs(r != 0 ? p * 100 / r : 0, new[] { p, r }));
				migrationTask.Run();
				if (CurrentMigrationTask.HasErrors)
					break;
				if (Worker.CancellationPending)
				{
					e.Cancel = true;
					return;
				}
			}
		}

		private void LoadDictionaries()
		{
			using (var ent = new Metis1Entities(Settings.SourceDatabaseConnectionString, 3000))
			{
				ReadTypeDict = ent.ReadType.ToList();
			}
		}

		private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			IsBusy = false;
			if (e.Error != null)
			{
				MessageBox.Show(e.Error.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				if (CancelCompleted != null)
				{
					CancelCompleted(this, null);
				}
			}
			else
			{
				if (e.Cancelled)
				{
					if (CancelCompleted != null)
					{
						CancelCompleted(this, null);
					}
				}
				else
				{
					if (Completed != null)
					{
						Completed(this);
					}
				}
			}
		}

		private void WorkerProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			if (ProgressChanged != null)
			{
				Dispatcher.CurrentDispatcher.Invoke(new Action(() => ProgressChanged(this, CurrentMigrationTask.MigrationTaskType, CurrentMigrationTask.State, ((int[])(e.UserState))[0], ((int[])(e.UserState))[1])));
			}
		}

		#endregion Methods
	}
}