﻿using System;
using System.Data.Objects;
using System.Linq;
using Metis1;
using MetisDM.Classes;
using Metis2;

namespace MetisDM.Herplers
{
    class SqlHelper
    {
        public delegate void ScriptProgressHandler(object sender, int percent);
        public static event ScriptProgressHandler ProgressChanged;

        public static Result TestSourceServerConnection(String connectionString)
        {
            try
            {
                using (var db = new Metis1Entities(connectionString))
                {
						                var query1 = @"
IF EXISTS(select * FROM sys.views where name = 'Address_Node_To_Address_Detail_Ids')
	DROP VIEW [ui].[Address_Node_To_Address_Detail_Ids]";
					
					var query2 = @"
CREATE VIEW [ui].[Address_Node_To_Address_Detail_Ids]
AS
	/****** Script for SelectTopNRows command from SSMS  ******/
	SELECT 
			[T1].[Id] AS [AddressNode_Id],
			CASE		[T8].[AddressType_Id] WHEN 1 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 1 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 1 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 1 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 1 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 1 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 1 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 1 THEN [T1].[Name]
			END END END END END END END END AS Country,
			CASE		[T8].[AddressType_Id] WHEN 1 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 1 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 1 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 1 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 1 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 1 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 1 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 1 THEN [T1].[Id]
			END END END END END END END END AS Country_Id,
			CASE		[T8].[AddressType_Id] WHEN 2 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 2 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 2 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 2 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 2 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 2 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 2 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 2 THEN [T1].[Name]
			END END END END END END END END AS Region1,
			CASE		[T8].[AddressType_Id] WHEN 2 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 2 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 2 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 2 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 2 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 2 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 2 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 2 THEN [T1].[Id]
			END END END END END END END END AS Region1_Id,
			CASE		[T8].[AddressType_Id] WHEN 3 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 3 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 3 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 3 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 3 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 3 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 3 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 3 THEN [T1].[Name]
			END END END END END END END END AS Region2,
			CASE		[T8].[AddressType_Id] WHEN 3 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 3 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 3 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 3 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 3 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 3 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 3 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 3 THEN [T1].[Id]
			END END END END END END END END AS Region2_Id,
			CASE		[T8].[AddressType_Id] WHEN 4 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 4 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 4 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 4 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 4 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 4 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 4 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 4 THEN [T1].[Name]
			END END END END END END END END AS Region3,
			CASE		[T8].[AddressType_Id] WHEN 4 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 4 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 4 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 4 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 4 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 4 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 4 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 4 THEN [T1].[Id]
			END END END END END END END END AS Region3_Id,
			CASE		[T8].[AddressType_Id] WHEN 5 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 5 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 5 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 5 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 5 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 5 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 5 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 5 THEN [T1].[Name]
			END END END END END END END END AS Region4,
			CASE		[T8].[AddressType_Id] WHEN 5 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 5 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 5 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 5 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 5 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 5 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 5 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 5 THEN [T1].[Id]
			END END END END END END END END AS Region4_Id,
			CASE		[T8].[AddressType_Id] WHEN 6 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 6 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 6 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 6 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 6 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 6 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 6 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 6 THEN [T1].[Name]
			END END END END END END END END AS City,
			CASE		[T8].[AddressType_Id] WHEN 6 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 6 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 6 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 6 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 6 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 6 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 6 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 6 THEN [T1].[Id]
			END END END END END END END END AS City_Id,
			CASE		[T8].[AddressType_Id] WHEN 7 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 7 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 7 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 7 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 7 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 7 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 7 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 7 THEN [T1].[Name]
			END END END END END END END END AS Street,
			CASE		[T8].[AddressType_Id] WHEN 7 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 7 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 7 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 7 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 7 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 7 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 7 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 7 THEN [T1].[Id]
			END END END END END END END END AS Street_Id,
			CASE		[T8].[AddressType_Id] WHEN 8 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 8 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 8 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 8 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 8 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 8 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 8 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 8 THEN [T1].[Name]
			END END END END END END END END AS Building,
			CASE		[T8].[AddressType_Id] WHEN 8 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 8 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 8 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 8 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 8 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 8 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 8 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 8 THEN [T1].[Id]
			END END END END END END END END AS Building_Id,
			CASE		[T8].[AddressType_Id] WHEN 9 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 9 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 9 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 9 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 9 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 9 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 9 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 9 THEN [T1].[Name]
			END END END END END END END END AS Staircase,
			CASE		[T8].[AddressType_Id] WHEN 9 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 9 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 9 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 9 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 9 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 9 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 9 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 9 THEN [T1].[Id]
			END END END END END END END END AS StairCase_Id,
			CASE		[T8].[AddressType_Id] WHEN 10 THEN [T8].[Name]
			ELSE CASE	[T7].[AddressType_Id] WHEN 10 THEN [T7].[Name] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 10 THEN [T6].[Name]
			ELSE CASE	[T5].[AddressType_Id] WHEN 10 THEN [T5].[Name]
			ELSE CASE	[T4].[AddressType_Id] WHEN 10 THEN [T4].[Name]
			ELSE CASE	[T3].[AddressType_Id] WHEN 10 THEN [T3].[Name]
			ELSE CASE	[T2].[AddressType_Id] WHEN 10 THEN [T2].[Name]
			ELSE CASE	[T1].[AddressType_Id] WHEN 10 THEN [T1].[Name]
			END END END END END END END END AS [Local],
			CASE		[T8].[AddressType_Id] WHEN 10 THEN [T8].[Id]
			ELSE CASE	[T7].[AddressType_Id] WHEN 10 THEN [T7].[Id] 
			ELSE CASE	[T6].[AddressType_Id] WHEN 10 THEN [T6].[Id]
			ELSE CASE	[T5].[AddressType_Id] WHEN 10 THEN [T5].[Id]
			ELSE CASE	[T4].[AddressType_Id] WHEN 10 THEN [T4].[Id]
			ELSE CASE	[T3].[AddressType_Id] WHEN 10 THEN [T3].[Id]
			ELSE CASE	[T2].[AddressType_Id] WHEN 10 THEN [T2].[Id]
			ELSE CASE	[T1].[AddressType_Id] WHEN 10 THEN [T1].[Id]
			END END END END END END END END AS Local_Id
	FROM [ui].[AddressNode] AS T1
	LEFT JOIN [ui].[AddressNode] AS T2 ON (T1.AddressNode_Id IS NOT NULL AND T1.AddressNode_Id = T2.Id)
	LEFT JOIN [ui].[AddressNode] AS T3 ON (T2.AddressNode_Id IS NOT NULL AND T2.AddressNode_Id = T3.Id)
	LEFT JOIN [ui].[AddressNode] AS T4 ON (T3.AddressNode_Id IS NOT NULL AND T3.AddressNode_Id = T4.Id)
	LEFT JOIN [ui].[AddressNode] AS T5 ON (T4.AddressNode_Id IS NOT NULL AND T4.AddressNode_Id = T5.Id)
	LEFT JOIN [ui].[AddressNode] AS T6 ON (T5.AddressNode_Id IS NOT NULL AND T5.AddressNode_Id = T6.Id)
	LEFT JOIN [ui].[AddressNode] AS T7 ON (T6.AddressNode_Id IS NOT NULL AND T6.AddressNode_Id = T7.Id)
	LEFT JOIN [ui].[AddressNode] AS T8 ON (T7.AddressNode_Id IS NOT NULL AND T7.AddressNode_Id = T8.Id)
";
	                db.ExecuteStoreCommand(query1, null);
					db.ExecuteStoreCommand(query2, null);
                    return new Result(db.DatabaseExists());
                }
            }
            catch (Exception e)
            {
                return new Result(e.Message);
            }
        }

		public static Result TestDestinationServerConnection(String connectionString)
        {
            try
            {
                using (var db = new Metis2Entities(connectionString))
                {
					return new Result(db.DatabaseExists());
                }
            }
            catch (Exception e)
            {
                return new Result(e.Message);
            }
        }

        //public static Result CheckDatabaseIsEmpty(Settings instance)
        //{
        //    try
        //    {
        //        var connectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", instance.DatabaseServerAddress, instance.DatabaseName, instance.DatabaseUserName, instance.DatabaseUserPassword);
        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            connection.Open();
        //            var command = connection.CreateCommand();
        //            command.CommandText = "SELECT TOP 1 1 FROM [sys].[objects] WHERE is_ms_shipped != 1";
        //            var reader = command.ExecuteReader();
        //            return new Result(!reader.HasRows);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

        public static Result CheckTargetDatabaseVersionIsCorrect(Settings settings, string version)
        {
            try
            {
                //var connectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", settings.TargetDatabaseServerAddress, settings.TargetDatabaseName, settings.TargetDatabaseName, settings.TargetDatabaseUserPassword);
                using (var ent = new Metis2Entities(settings.TargetDatabaseConnectionString, 3000))
                {
					var ver = ent.SystemSetting.Where(e => e.SystemSettingType_Id == 23).Select(e => e.Value).FirstOrDefault();
					return ver != version ? new Result("Incorrect database version") : new Result(true);
                }
            }
            catch (Exception e)
            {
                return new Result(e.Message);
            }
        }

        //public static Result CreateDatabaseIfNotExists(Settings instance)
        //{
        //    if (!instance.CreateDatabaseIfNotExists) return new Result();
        //    var result = CheckDatabaseExists(instance);
        //    if (result.Value.HasValue && result.Value.Value) return new Result();
        //    try
        //    {
        //        var connectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", instance.DatabaseServerAddress, "master", instance.DatabaseUserName, instance.DatabaseUserPassword);
        //        using (var connection = new SqlConnection(connectionString))
        //        {
        //            connection.Open();
        //            var command = connection.CreateCommand();
        //            command.CommandText = string.Format("CREATE DATABASE {0}", instance.DatabaseName);
        //            command.ExecuteNonQuery();
        //            return new Result(true);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

        //private static string GetCreateDatabaseStructureScript(Settings instance)
        //{
        //    var script = Properties.Resources.CreateDatabaseStructure;
        //    script = script.Replace(_stringToRemove1, string.Empty);
        //    script = script.Replace(_stringToRemove2, string.Empty);
        //    script = script.Replace("$(DatabaseName)", instance.DatabaseName);
        //    script = script.Replace("$(StrStart)", @"'<?xml version=''1.0'' ?><Metis>");
        //    script = script.Replace("$(StrEnd)", @"</Metis>'");
        //    script = script.Replace("$(__IsSqlCmdEnabled)", "True");
        //    script = script.Replace(_commentCmdSqlFrom, _commentCmdSqlTo);
        //    return script;
        //}

        //public static Result InitializeDatabase(Settings instance)
        //{
        //    Thread.Sleep(10000);
        //    try
        //    {
        //        var connectionString = string.Format("data source={0};initial catalog={1};user id={2};password={3};", instance.DatabaseServerAddress, instance.DatabaseName, instance.DatabaseUserName, instance.DatabaseUserPassword);
        //        using (var connection = new SqlConnection(connectionString) )
        //        {
        //            connection.Open();
        //            var script = GetCreateDatabaseStructureScript(instance);
        //            var statements = script.Split(new[] {_goStatement}, StringSplitOptions.None).Where(e => !string.IsNullOrWhiteSpace(e)).ToList();
        //            var i = 0;
        //            foreach (var statement in statements)
        //            {
        //                var cmd = connection.CreateCommand();
        //                cmd.CommandText = statement.Trim();
        //                cmd.CommandTimeout = 600;
        //                cmd.ExecuteNonQuery();
        //                i++;
        //                if (/*i % 100 == 0 && */ProgressChanged != null)
        //                {
        //                    var percent = i/statements.Count*100;
        //                    ProgressChanged.Invoke(null, percent);
        //                }
        //            }
        //            ProgressChanged = null;
        //            return new Result();
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        ProgressChanged = null;
        //        return new Result(e.Message);
        //    }
        //}

    }
}
