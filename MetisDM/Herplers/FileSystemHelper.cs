﻿using System;
using System.IO;
using System.Linq;
using MetisDM.Classes;

namespace MetisDM.Herplers
{
    public class FileSystemHelper
    {
        private const string ConfigFile = @"<configuration>
	<section name=""server"">
		<setting name=""interface"" type=""string"">{0}:{1}</setting>
	</section>
	<section name=""database"">
		<setting name=""main"" type=""string"">{2}; {3}; {4}; {5}; {6}</setting>
	</section>
	<section name=""log"">
		<setting name=""directory"" type=""string"">{7}</setting>
	</section>
</configuration>";

        public static bool CheckFolderExists(string path)
        {
            return Directory.Exists(path);
        }

        public static bool CheckFileExists(string path)
        {
            return File.Exists(path);
        }

        public static bool CheckParentFolderExists(string path)
        {
            var directory = Directory.GetParent(path);
            return directory.Exists;
        }

        public static bool CheckFolderIsEmpty(string path)
        {
            string[] files = Directory.GetFiles(path);
            string[] dirs = Directory.GetDirectories(path);
            return !files.Any() && !dirs.Any();
        }

        public static Result CreateFolder(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
                return new Result(true);
            }
            catch (Exception e)
            {
                return new Result(e.Message);
            }
        }

        //private static void CopyLibs(string path)
        //{
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.Models.dll"), Resources.AT_Models_dll);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.Models.pdb"), Resources.AT_Models_pdb);
        //    File.WriteAllText(string.Format("{0}\\{1}", path, "AT.Models.xml"), Resources.AT_Models_xml);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.Prototypes.dll"), Resources.AT_Prototypes_dll);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.Prototypes.pdb"), Resources.AT_Prototypes_pdb);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.dll"), Resources.AT_dll);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "AT.pdb"), Resources.AT_pdb);
        //    File.WriteAllText(string.Format("{0}\\{1}", path, "AT.xml"), Resources.AT_xml);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "St.dll"), Resources.St_dll);
        //    File.WriteAllBytes(string.Format("{0}\\{1}", path, "St.pdb"), Resources.St_pdb);
        //}

        //public static Result CopySaFiles(Settings instance)
        //{
        //    try
        //    {
        //        var path = instance.SaInstallationFolder;
        //        CopyLibs(path);

        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisSaService.exe"), Resources.MetisSaService_exe);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisSaService.pdb"), Resources.MetisSaService_pdb);
        //        File.WriteAllText(string.Format("{0}\\{1}", path, "MetisSaService.exe.config"), Resources.MetisSaService_exe_config);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "Sa.dll"), Resources.Sa_dll);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "Sa.pdb"), Resources.Sa_pdb);
        //        return new Result(true);
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

        //public static Result CreateSaConfiguration(Settings instance)
        //{
        //    try
        //    {
        //        var path = instance.SaInstallationFolder;
        //        var serverAddressParts = instance.DatabaseServerAddress.Split('\\');
        //        var content = string.Format(_configFile, instance.StIpAddress,
        //                                                 instance.StPort,
        //                                                 serverAddressParts[0],
        //                                                 serverAddressParts.Count() == 2 ? serverAddressParts[1] : string.Empty,
        //                                                 instance.DatabaseName,
        //                                                 instance.DatabaseUserName,
        //                                                 instance.DatabaseUserPassword,
        //                                                 instance.SaLogsFolder);
        //        File.WriteAllText(string.Format("{0}\\{1}", path, "config.xml"), content);
        //        return new Result(true);
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

        //public static Result CopyStFiles(Settings instance)
        //{
        //    try
        //    {
        //        var path = instance.StInstallationFolder;
        //        CopyLibs(path);

        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisStService.exe"), Resources.MetisStService_exe);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisStService.pdb"), Resources.MetisStService_pdb);
        //        File.WriteAllText(string.Format("{0}\\{1}", path, "MetisStService.exe.config"), Resources.MetisStService_exe_config);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisStService.vshost.exe"), Resources.MetisStService_vshost_exe);
        //        File.WriteAllBytes(string.Format("{0}\\{1}", path, "MetisStService.vshost.exe.manifest"), Resources.MetisStService_vshost_exe_manifest);
        //        File.WriteAllText(string.Format("{0}\\{1}", path, "MetisStService.vshost.exe.config"), Resources.MetisStService_vshost_exe_config);
        //        return new Result(true);
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

        //public static Result CreateStConfiguration(Settings instance)
        //{
        //    try
        //    {
        //        var path = instance.StInstallationFolder;
        //        var serverAddressParts = instance.DatabaseServerAddress.Split('\\');
        //        var content = string.Format(_configFile, instance.StIpAddress,
        //                                                 instance.StPort,
        //                                                 serverAddressParts[0],
        //                                                 serverAddressParts.Count() == 2 ? serverAddressParts[1] : string.Empty,
        //                                                 instance.DatabaseName,
        //                                                 instance.DatabaseUserName,
        //                                                 instance.DatabaseUserPassword,
        //                                                 instance.StLogsFolder);
        //        File.WriteAllText(string.Format("{0}\\{1}", path, "config.xml"), content);
        //        return new Result(true);
        //    }
        //    catch (Exception e)
        //    {
        //        return new Result(e.Message);
        //    }
        //}

    }
}
