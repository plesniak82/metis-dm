﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MetisDM.Controls.MainPage.ViewModels;

namespace MetisDM.Controls.MainPage.Views
{
	/// <summary>
	/// Interaction logic for DevicesMapping.xaml
	/// </summary>
	public partial class DevicesMapping
	{
		public DevicesMapping()
		{
			InitializeComponent();
			//Loaded += DevicesMapping_Loaded;
		}

		void DevicesMapping_Loaded(object sender, RoutedEventArgs e)
		{
			if (DataContext is DevicesMappingViewModel)
			{
				(DataContext as DevicesMappingViewModel).SetData();
			}
		}
	}
}
