﻿using MetisDM.Controls.MainPage.ViewModels;

namespace MetisDM.Controls.MainPage.Views
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage
    {
        public MainPage()
        {
            DataContext = new MainViewModel();
            InitializeComponent();
            (DataContext as MainViewModel).PropertyChanged += MainPage_PropertyChanged;
        }

        void MainPage_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy")
            {
                Cursor = (DataContext as MainViewModel).IsBusy ? System.Windows.Input.Cursors.Wait : System.Windows.Input.Cursors.Arrow;
            }
        }
    }
}
