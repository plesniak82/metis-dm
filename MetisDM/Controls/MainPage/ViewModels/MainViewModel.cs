﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.SetupStepValidators;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ApplicationSystem = System.Windows.Application;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public partial class MainViewModel : ViewModelBase
	{
		#region Constructor

		public MainViewModel()
		{
			_settings = new Settings
			{
				LogFolder = GetCurrentDirectory() + "\\LogFolder",
				SettingsFolder = GetCurrentDirectory() + "\\SettingsFolder",
				SettingsFile = "\\MigrationSettings.xml",

				MigrateReads = false,
				MigrateStructure = true
			};
			_settings.SettingsFile = string.Format("{0}{1}", SettingsFolder, _settings.SettingsFile);

			//SourceDatabaseServerAddress = "10.3.0.114";
			//SourceDatabaseUserName = "serwer";
			//SourceDatabaseUserPassword = "963852741";
			//SourceDatabaseName = "SystemTelemetryczny_1.0.1";

			//TargetDatabaseServerAddress = "test1.telemetria.eu";
			//TargetDatabaseUserName = "test_ST1";
			//TargetDatabaseUserPassword = "test_ST1";
			//TargetDatabaseName = "Metis2003_PL";

			_settingsWritter = new SettingsWritter<Settings>(_settings);
			_settingsWritter.LoadSettings();
			_settings.DevicesMappingViewModel = new DevicesMappingViewModel(_settings);
		}

		public static String GetCurrentDirectory()
		{
			String str = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
			if (str != null && str.StartsWith("file:\\"))
			{
				str = str.Substring(6, str.Length - 6);
			}

			return str;
		}

		#endregion Constructor

		#region Commands

		#region Back Command

		private RelayCommand _backCommand;

		public RelayCommand BackCommand
		{
			get { return _backCommand ?? (_backCommand = new RelayCommand(OnBackCommandExecute, CanRaiseBackCommand)); }
		}

		private void OnBackCommandExecute()
		{
			var currentStep = _stepsSequence.First(e => e == Step);
			var stepFound = false;
			foreach (var step in _stepsSequence.OrderByDescending(e => (int)e))
			{
				if (step == currentStep)
				{
					stepFound = true;
					continue;
				}
				if (stepFound)
				{
					Step = step;
					break;
				}
			}
			Errors = new List<string>();
		}

		private bool CanRaiseBackCommand()
		{
			return Step > SetupStep.GeneralSettings && Step < SetupStep.Migration;
		}

		#endregion Back Command

		#region Next Command

		private RelayCommand _nextCommand;

		public RelayCommand NextCommand
		{
			get { return _nextCommand ?? (_nextCommand = new RelayCommand(OnNextCommandExecute, CanRaiseNextCommand)); }
		}

		private void OnNextCommandExecute()
		{
			var afterValidate = new SetupStepBase.ValidationCompletedHandler((sender, resultStatus) =>
			{
				IsBusy = false;
				StepHasError = resultStatus.HasErrors;
				if (!resultStatus.HasErrors)
				{
					var currentStep = _stepsSequence.First(e => e == Step);
					var stepFound = false;
					foreach (var step in _stepsSequence.OrderBy(e => (int)e))
					{
						if (step == currentStep)
						{
							stepFound = true;
							continue;
						}
						if (stepFound)
						{
							Step = step;
							if (Step == SetupStep.DevicesMapping)
							{
								IsBusy = true;
								_settings.DevicesMappingViewModel.SetData();
								IsBusy = false;
							}
							break;
						}
					}
				}
				else
				{
					Errors = resultStatus.Errors.Select(e => e.Value).ToList();
				}
			});

			var validator = _validators.FirstOrDefault(e => e.SetupStep == Step);
			if (validator != null)
			{
				validator.Validated += afterValidate;
				IsBusy = true;
				validator.Validate(_settings);
			}
			else
			{
				afterValidate.Invoke(this, new ResultStatus(new List<KeyValuePair<string, string>>()));
			}
		}

		private bool CanRaiseNextCommand()
		{
			return Step < SetupStep.Migration;
		}

		#endregion Next Command

		#region Close Command

		private RelayCommand _closeCommand;

		public RelayCommand CloseCommand
		{
			get { return _closeCommand ?? (_closeCommand = new RelayCommand(OnCloseCommandExecute, CanRaiseCloseCommand)); }
		}

		private void OnCloseCommandExecute()
		{
			ApplicationSystem.Current.Shutdown();
		}

		private bool CanRaiseCloseCommand()
		{
			return Step != SetupStep.Migration || _installationCompleted;
		}

		#endregion Close Command

		#region St Logs Folder

		private RelayCommand _openStLogsFolderCommand;

		public RelayCommand OpenStLogsFolderCommand
		{
			get { return _openStLogsFolderCommand ?? (_openStLogsFolderCommand = new RelayCommand(OnOpenStLogsFolderCommandExecute, CanRaiseOpenStLogsFolderCommand)); }
		}

		private void OnOpenStLogsFolderCommandExecute()
		{
			var fbd = new FolderBrowserDialog { SelectedPath = LogFolder };
			var result = fbd.ShowDialog();
			if (result == DialogResult.OK)
			{
				LogFolder = fbd.SelectedPath;
			}
		}

		private bool CanRaiseOpenStLogsFolderCommand()
		{
			return true;
		}

		#endregion St Logs Folder

		#region Show errors

		private RelayCommand<MigrationTaskType> _showErrorsCommand;

		public RelayCommand<MigrationTaskType> ShowErrorsCommand
		{
			get
			{
				if (_showErrorsCommand == null)
				{
					_showErrorsCommand = new RelayCommand<MigrationTaskType>(OnShowErrorsExecuted);
				}
				return _showErrorsCommand;
			}
		}

		private void OnShowErrorsExecuted(MigrationTaskType step)
		{
			var task = Tasks.FirstOrDefault(e => e.MigrationTaskType == step);
			if (task != null)
			{
				var errors = task.GetErrors().Select(e => e.Value);
				var message = string.Format("There were followig errors:");
				var win = new ErrorWindow.ErrorWindow(message, errors);
				win.Show();
			}
		}

		#endregion Show errors

		#region Show warnngs

		private RelayCommand<MigrationTaskType> _showWarningsCommand;

		public RelayCommand<MigrationTaskType> ShowWarningsCommand
		{
			get
			{
				if (_showWarningsCommand == null)
				{
					_showWarningsCommand = new RelayCommand<MigrationTaskType>(OnShowWarningsExecuted);
				}
				return _showWarningsCommand;
			}
		}

		private void OnShowWarningsExecuted(MigrationTaskType step)
		{
			var task = Tasks.FirstOrDefault(e => e.MigrationTaskType == step);
			if (task != null)
			{
				var errors = task.GetWarnings().Select(e => e.Value);
				var message = string.Format("There were followig warnings:");
				var win = new ErrorWindow.ErrorWindow(message, errors);
				win.Show();
			}
		}

		#endregion Show warnngs

		#endregion Commands

		#region Methods

		private static decimal _prevProgress = -1;

		private static DateTime _startTime;

		private void OnStepChanged()
		{
			RaisePropertyChanged("Step");
			RaisePropertyChanged("CloseCommandContent");
			RaisePropertyChanged("NextCommandContent");
			BackCommand.RaiseCanExecuteChanged();
			NextCommand.RaiseCanExecuteChanged();
			CloseCommand.RaiseCanExecuteChanged();
			if (Step == SetupStep.Migration)
			{
				_settingsWritter.SaveSettings();
				Migrate();
			}
		}

		private void Migrate()
		{
			var migrate = new Migrate.Migrate(_settings);
			migrate.CheckLogDirectory();
			Tasks = migrate.MigrationTasks.OrderBy(e => e.MigrationTaskType).ToList();
			migrate.Run();
			migrate.Completed += s =>
			{
				_installationCompleted = true;
				RaisePropertyChanged("CloseCommandContent");
				CloseCommand.RaiseCanExecuteChanged();
				if (migrate.HasErrors)
				{
					MessageBox.Show(@"There were errors during the migration.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else if (migrate.HasWarnings)
				{
					MessageBox.Show(@"There were warnings during the migration.", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show(@"Migration finished successfully", @"Migration finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			};
			migrate.ProgressChanged += InstallProgressChanged;
			migrate.CancelCompleted += InstallCancelCompleted;
		}

		private void InstallCancelCompleted(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		private void InstallProgressChanged(object sender, MigrationTaskType installationStep, TaskState taskState, int done, int total)
		{
			try
			{
				decimal percent = total > 0 ? done * 100 / total : 0;

				TimeSpan? remainingTime = null;

				if (percent == 0 && percent != _prevProgress)
				{
					_startTime = DateTime.Now;
				}
				if (done > 0 && total > 0)
				{
					TimeSpan? elapsedTime = DateTime.Now.Subtract(_startTime);
					try
					{
						remainingTime = new TimeSpan(Decimal.ToInt64(elapsedTime.Value.Ticks / done * (total - done)));
					}
					catch (Exception ex)
					{
						Debug.WriteLine(ex.ToString());
					}
				}

				_prevProgress = percent;
				//Details = String.Format("{0,14}   ({1}/{2})", dateTime.Day == 0 ? dateTime.ToString("dd.HH:mm:ss") : dateTime.ToString("HH:mm:ss"), done, total);
				Progress = (int)percent;
				ProgressCount = String.Format("{0} / {1}", done, total);
				if (remainingTime != null)
				{
					var remainingDate = new DateTime(remainingTime.Value.Ticks);
					RemainingTime = String.Format("{0}", remainingDate.Day <= 1 ? remainingDate.ToString("HH:mm:ss") : String.Format("{0}.{1}", remainingDate.Day - 1, remainingDate.ToString("HH:mm:ss")));
				}
				else
				{
					RemainingTime = String.Format("{0}", "--:--:--");
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
				throw;
			}
		}

		private void InstallCompleted(object sender)
		{
			_installationCompleted = true;
			RaisePropertyChanged("CloseCommandContent");
			CloseCommand.RaiseCanExecuteChanged();
		}

		#endregion Methods
	}
}