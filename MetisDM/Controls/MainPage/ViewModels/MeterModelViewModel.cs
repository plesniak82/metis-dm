﻿using System.Collections.Generic;
using System.Linq;
using AT.Models.Devices;
using Metis1.Enums;
using Metis2;
using MetisDM.Classes;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public class MeterModelViewModel : ValidateViewModel
	{
		public Metis1.MeterModel MeterModel { get; set; }

		public MeterModelPulseViewModel[] Metis1Pulses { get; set; }
		public Metis1.DeviceType[] Metis1DeviceTypes { get; set; }

		public DeviceType Metis2DeviceType { get; set; }
		public List<DeviceType> Metis2DeviceTypes { get; set; }

		public List<DevicesExchangeViewModel> DevicesExchanges { get; set; }

		public DevicesExchangeViewModel SelectedItem { get; set; }

		public bool AutoAdjust { get; set; }

		public MeterModelViewModel(Metis1.MeterModel meterModel, string metis2DeviceTypeName, IEnumerable<MeterModelPulseViewModel> pulses, IEnumerable<Metis1.DeviceType> metis1DeviceTypes, IEnumerable<DeviceType> metis2DeviceTypes, IEnumerable<vwDeviceUse> deviceUses)
		{
			MeterModel = meterModel;
			Metis1Pulses = pulses.ToArray();
			Metis1DeviceTypes = metis1DeviceTypes.ToArray();

			var metis2DevicesTransposedFromMetis1Devices = Metis1DeviceTypes.Select(e => Database.Mappings.DeviceTypeToDeviceTypeModel((DeviceTypes)e.Id))
				.Where(x => x.HasValue).Select(e => e.Value.Key).ToArray();

			var measurementDevices = deviceUses.GroupBy(e => e.MeasuredDevice_Id).Select(e => new {Key=(Types)e.Key, Devices=e.Select(i => (Types)i.TransmissionDevice_Id.Value).Distinct().ToArray()} ).ToArray();

			var measuredMetis2DevicesWitchCanConnectToMetis1Devices = measurementDevices.Where(e => metis2DevicesTransposedFromMetis1Devices.Intersect(e.Devices).Count() >= e.Devices.Count()).ToArray();

			Metis2DeviceTypes = new List<DeviceType>();

			metis2DeviceTypes.Where(e => measuredMetis2DevicesWitchCanConnectToMetis1Devices.Any(x => x.Key == (Types) e.Id)).ToList().ForEach(e => Metis2DeviceTypes.Add(new DeviceType{Id = e.Id, IsConfigurable = true, Name = e.Name}));
			Metis2DeviceTypes.Add(new DeviceType{Id = 0});
			metis2DeviceTypes.Where(e => !Metis2DeviceTypes.Select(f => f.Id).Contains(e.Id)).ToList().ForEach(e => Metis2DeviceTypes.Add(new DeviceType{Id = e.Id, IsConfigurable = false, Name = e.Name}));

			DevicesExchanges = Metis1DeviceTypes.Select(e => new DevicesExchangeViewModel(e, null, Metis2DeviceTypes)).ToList();
			if (metis2DeviceTypeName != null)
			{
				Metis2DeviceType = Metis2DeviceTypes.FirstOrDefault(e => e.Name == metis2DeviceTypeName);
			}
			else
			{
				Metis2DeviceType = Metis2DeviceTypes.FirstOrDefault(e => e.Name == meterModel.Name);
			}
		}

	}
}
