﻿using Metis1;
using MetisDM.Classes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public class DevicesMappingViewModel : ValidateViewModel
	{
		#region Properties

		public List<MeterModelViewModel> MeterModels
		{
			get { return _meterModels; }
			set
			{
				_meterModels = value;
				RaisePropertyChanged("MeterModels");
			}
		}

		public MeterModelViewModel SelectedMeterModel
		{
			get { return _selectedMeterModel; }
			set
			{
				_selectedMeterModel = value;
				RaisePropertyChanged("SelectedMeterModel");
				OnSelectedMeterModelChanged();
			}
		}

		#endregion Properties

		#region Fields

		private readonly Settings _settings;

		private List<MeterModelViewModel> _meterModels;
		private MeterModelViewModel _selectedMeterModel;

		#endregion Fields

		#region Constructors

		public DevicesMappingViewModel(Settings settings)
		{
			_settings = settings;
		}

		#endregion Constructors

		#region Methods

		public void SetData()
		{
			if (_settings != null && _settings.SourceDatabaseConnectionString != null && _settings.TargetDatabaseConnectionString != null)
			{
				try
				{
					Metis2.DeviceType[] metis2DeviceTypes;
					Metis2.vwDeviceUse[] metis2DeviceUse;
					using (var db = new Metis2.Metis2Entities(_settings.TargetDatabaseConnectionString))
					{
						metis2DeviceTypes = db.DeviceType.Where(e => e.IsImplemented).OrderBy(e => e.NameToSort).ToArray();
						metis2DeviceUse = db.vwDeviceUse.Where(e => e.Language_Id == 1).ToArray();
					}
					using (var db = new Metis1Entities(_settings.SourceDatabaseConnectionString))
					{
						var pulses = db.PulseWeight.ToArray();
						var meterModelPulses = db.MeterModelPulse.ToArray();

						var deviceTypeIds = new List<int>();
						if (_settings.ClientIds != null && _settings.ClientIds.Any())
						{
							deviceTypeIds = db.Device.Where(e => e.DeviceType_Id != (int)Metis1.Enums.DeviceTypes.AtWmbus05 &&
																e.DeviceActivation.Any(f => _settings.ClientIds.Contains(f.Client_Id))).Select(e => e.DeviceType_Id).Distinct().ToList();
						}

						var meterModelToDeviceTypes = deviceTypeIds != null && deviceTypeIds.Any()
							? db.MeterInstallation.Join(db.DeviceInstallation, i => i.Terminal_Id, o => o.Terminal_Id, (i, o) => new { i.Meter.MeterModel_Id, DeviceType = o.Interface.Device.DeviceType }).Distinct().Where(e => deviceTypeIds.Contains(e.DeviceType.Id))
							: db.MeterInstallation.Join(db.DeviceInstallation, i => i.Terminal_Id, o => o.Terminal_Id, (i, o) => new { i.Meter.MeterModel_Id, DeviceType = o.Interface.Device.DeviceType }).Distinct();

						var meterPulsesVms = meterModelPulses.Join(pulses, i => i.PulseWeight_Id, o => o.Id, (i, o) => new { i.MeterModel_Id, o.Value })
							.GroupBy(e => e.MeterModel_Id)
							.Where(e => e.Key.HasValue)
							.Select(e => new MeterModelPulseViewModel(e.Key.Value, e.Select(x => x.Value).ToArray()))
							.ToArray();
						MeterModels = db.Meter.Join(db.MeterInstallation, i => i.Id, o => o.Meter_Id, (i, o) => i.MeterModel)
							.Distinct()
							.OrderBy(e => e.Name).ToArray().Select(e =>
							{
								string deviceTypeName = null;
								if (_settings.DeviceMapping != null)
								{
									var keyPair = _settings.DeviceMapping.FirstOrDefault(x => x[0] == e.Name);
									deviceTypeName = (keyPair != null && keyPair.Count > 1) ? keyPair[1] : null;
								}
								return new MeterModelViewModel(e, deviceTypeName, meterPulsesVms.Where(x => x.MeterModel_Id == e.Id), meterModelToDeviceTypes.Where(x => x.MeterModel_Id == e.Id).Select(x => x.DeviceType), metis2DeviceTypes, metis2DeviceUse);
							}).ToList();
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e.ToString());
				}
			}
		}

		private void OnSelectedMeterModelChanged()
		{
		}

		#endregion Methods
	}
}