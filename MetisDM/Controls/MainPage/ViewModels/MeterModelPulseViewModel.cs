﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public class MeterModelPulseViewModel
	{
		public int MeterModel_Id { get; set; }
		public decimal[] Pulses { get; set; }

		public MeterModelPulseViewModel(int meterModelId, decimal[] pulses)
		{
			MeterModel_Id = meterModelId;
			Pulses = pulses;
		}
	}
}
