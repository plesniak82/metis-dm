﻿using MetisDM.Classes;
using MetisDM.Enums;
using MetisDM.SetupStepValidators;
using MetisDM.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public partial class MainViewModel
	{
		#region Properties

		private readonly Settings _settings = new Settings();

		private readonly List<SetupStepBase> _validators = new List<SetupStepBase>
		{
			new GeneralSettingsValidator(),
			new SourceDatabaseSettingsValidator(),
			new TargetDatabaseSettingsValidator(),
			new DevicesMappingValidator()
		};

		private bool _installationCompleted;

		private bool _isBusy;
		private bool _stepHasError;

		private SetupStep _step = SetupStep.GeneralSettings;

		private int _progress;

		private string _progressCount;

		private string _remainingTime;

		private Collection<SetupStep> _stepsSequence = new Collection<SetupStep>
		{
			SetupStep.GeneralSettings,
			SetupStep.SourceDatabaseConnectionSettings,
			SetupStep.TargetDatabaseConnectionSettings,
			SetupStep.DevicesMapping,
			SetupStep.Migration,
		};

		private List<string> _errors;

		public bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				_isBusy = value;
				RaisePropertyChanged("IsBusy");
			}
		}

		public bool StepHasError
		{
			get { return _stepHasError; }
			set
			{
				_stepHasError = value;
				RaisePropertyChanged("StepHasError");
				NextCommand.RaiseCanExecuteChanged();
			}
		}

		public string Metis2Version { get { return string.Format("Version: {0}", new SystemVersion().CurrentVersion); } }

		public SetupStep Step
		{
			get { return _step; }
			set
			{
				_step = value;
				OnStepChanged();
			}
		}

		public int Progress
		{
			get { return _progress; }
			set
			{
				_progress = value;
				RaisePropertyChanged("Progress");
			}
		}

		public string ProgressCount
		{
			get { return _progressCount; }
			set
			{
				_progressCount = value;
				RaisePropertyChanged("ProgressCount");
			}
		}

		public string RemainingTime
		{
			get { return _remainingTime; }
			set
			{
				_remainingTime = value;
				RaisePropertyChanged("RemainingTime");
			}
		}

		public List<string> Errors
		{
			get { return _errors; }
			set
			{
				_errors = value;
				RaisePropertyChanged("Errors");
				StepHasError = _errors != null && _errors.Any();
			}
		}

		public string CloseCommandContent
		{
			get
			{
				return _installationCompleted ? "Close" : "Cancel";
			}
		}

		public string NextCommandContent
		{
			get
			{
				var currentStep = _stepsSequence.First(e => e == Step);
				var stepFound = false;
				foreach (var step in _stepsSequence.OrderBy(e => (int)e))
				{
					if (step == currentStep)
					{
						stepFound = true;
						continue;
					}
					if (stepFound)
					{
						if (step == SetupStep.Migration)
						{
							return "Migrate";
						}
						break;
					}
				}
				return "Next >";
			}
		}

		#region General

		//public MigrationType MigrationType
		//{
		//	get { return _settings.MigrationType; }
		//	set
		//	{
		//		_settings.MigrationType = value;
		//		RaisePropertyChanged("MigrationType");
		//		OnMigrationTypeChanged();
		//		NextCommand.RaiseCanExecuteChanged();
		//	}
		//}

		public bool MigrateReads
		{
			get { return _settings.MigrateReads; }
			set
			{
				_settings.MigrateReads = false;
				RaisePropertyChanged("MigrateReads");
				NextCommand.RaiseCanExecuteChanged();
			}
		}

		public bool MigrateStructure
		{
			get { return _settings.MigrateStructure; }
			set
			{
				_settings.MigrateStructure = value;
				RaisePropertyChanged("MigrateStructure");
				NextCommand.RaiseCanExecuteChanged();
			}
		}

		public string LogFolder
		{
			get { return _settings.LogFolder; }
			set
			{
				_settings.LogFolder = value;
				RaisePropertyChanged("LogFolder");
			}
		}

		public string SettingsFolder
		{
			get { return _settings.SettingsFolder; }
			set
			{
				_settings.SettingsFolder = value;
				RaisePropertyChanged("SettingsFolder");
			}
		}

		#endregion General

		#region Source database settings

		private SettingsWritter<Settings> _settingsWritter;

		public string SourceDatabaseServerAddress
		{
			get { return _settings.SourceDatabaseServerAddress; }
			set { _settings.SourceDatabaseServerAddress = value; }
		}

		public string SourceDatabaseName
		{
			get { return _settings.SourceDatabaseName; }
			set { _settings.SourceDatabaseName = value; }
		}

		public string SourceDatabaseUserName
		{
			get { return _settings.SourceDatabaseUserName; }
			set { _settings.SourceDatabaseUserName = value; }
		}

		public string SourceDatabaseUserPassword
		{
			get { return _settings.SourceDatabaseUserPassword; }
			set { _settings.SourceDatabaseUserPassword = value; }
		}

		#endregion Source database settings

		#region Target database settings

		public string TargetDatabaseServerAddress
		{
			get { return _settings.TargetDatabaseServerAddress; }
			set { _settings.TargetDatabaseServerAddress = value; }
		}

		public string TargetDatabaseName
		{
			get { return _settings.TargetDatabaseName; }
			set { _settings.TargetDatabaseName = value; }
		}

		public string TargetDatabaseUserName
		{
			get { return _settings.TargetDatabaseUserName; }
			set { _settings.TargetDatabaseUserName = value; }
		}

		public string TargetDatabaseUserPassword
		{
			get { return _settings.TargetDatabaseUserPassword; }
			set { _settings.TargetDatabaseUserPassword = value; }
		}

		#endregion Target database settings

		#region Devices Mapping

		public DevicesMappingViewModel DevicesMappingViewModel
		{
			get { return _settings.DevicesMappingViewModel; }
		}

		#endregion Devices Mapping

		#region Installation

		private List<TaskBase> _tasks;

		public List<TaskBase> Tasks
		{
			get { return _tasks; }
			set
			{
				_tasks = value;
				RaisePropertyChanged("Tasks");
			}
		}

		#endregion Installation

		#endregion Properties
	}
}