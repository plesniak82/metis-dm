﻿using System.Collections.Generic;
using System.Linq;

namespace MetisDM.Controls.MainPage.ViewModels
{
	public class DevicesExchangeViewModel
	{
		public Metis1.DeviceType Metis1DeviceType { get; set; }
		public Metis2.DeviceType Metis2DeviceType { get; set; }
		public List<Metis2.DeviceType> Metis2DeviceTypes { get; set; }

		public DevicesExchangeViewModel(Metis1.DeviceType metis1DeviceType, Metis2.DeviceType metis2DeviceType, List<Metis2.DeviceType> metis2DeviceTypes)
		{
			Metis1DeviceType = metis1DeviceType;
			Metis2DeviceType = metis2DeviceType;
			Metis2DeviceTypes = metis2DeviceTypes;
			if (Metis2DeviceType == null)
			{
				Metis2DeviceType = Metis2DeviceTypes.FirstOrDefault(e => e.Name == metis1DeviceType.Name);
			}
		}
	}
}
