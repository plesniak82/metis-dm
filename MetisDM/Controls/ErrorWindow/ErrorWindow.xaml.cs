﻿using System.Collections.Generic;
using System.Windows;

namespace MetisDM.Controls.ErrorWindow
{
    /// <summary>
    /// Interaction logic for ErrorWindow.xaml
    /// </summary>
    public partial class ErrorWindow
    {
        public ErrorWindow(string message, IEnumerable<string> lines)
        {
            InitializeComponent();
            _list.ItemsSource = lines;
            _message.Text = message;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
