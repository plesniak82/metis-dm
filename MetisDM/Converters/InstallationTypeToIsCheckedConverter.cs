﻿using System;
using System.Globalization;
using System.Windows.Data;
using MetisDM.Enums;

namespace MetisDM.Converters
{
    class InstallationTypeToIsCheckedConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as MigrationType?;
            var param = parameter as MigrationType?;
            if (val != null && param != null)
            {
                return val == param;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as bool?;
            var param = parameter as MigrationType?;
            if (val != null && param != null)
            {
                return val.Value ? param : (param == MigrationType.Migration ? MigrationType.MigrationContinue : MigrationType.Migration);
            }
            return MigrationType.Migration;
        }
    }
}
