﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using MetisDM.Enums;

namespace MetisDM.Converters
{
    class StepToVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var step = value as SetupStep?;
            var tabItem = parameter as SetupStep?;
            var result = false;
            if (value != null && parameter != null)
            {
                result = step == tabItem;
            }
            return result ? Visibility.Visible : Visibility.Collapsed;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
