﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MetisDM.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if (parameter == null)
                {
                    return (bool) value ? Visibility.Visible : Visibility.Collapsed;
                }
                if (parameter.ToString() == "not") //negacja
                {
                    return (bool) value ? Visibility.Collapsed : Visibility.Visible;
                }
            }

            return Visibility.Collapsed;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null && parameter.ToString() == "not") //negacja
            {

                return (Visibility) value != Visibility.Visible;
            }
            return (Visibility) value == Visibility.Visible;
        }
    }
}
