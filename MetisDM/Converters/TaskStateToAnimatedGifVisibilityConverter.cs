﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using MetisDM.Enums;

namespace MetisDM.Converters
{
    public class TaskStateToAnimatedGifVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var param = parameter as string;
            if (value is TaskState && (TaskState) value == TaskState.InProgress)
            {
                return param == "animated" ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return param == "animated" ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
