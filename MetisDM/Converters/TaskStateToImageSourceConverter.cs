﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using MetisDM.Enums;

namespace MetisDM.Converters
{
    public class TaskStateToImageSourceConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TaskState)
            {
                var img = new BitmapImage();
                img.BeginInit();
                img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;

                switch ((TaskState)value)
                {
                    case TaskState.InProgress:
                        img.UriSource = new Uri("/MetisDM;component/Assets/Images/InProgress.gif", UriKind.Relative);
                        break;
                    case TaskState.Errors:
                        img.UriSource = new Uri("/MetisDM;component/Assets/Images/Error.png", UriKind.Relative);
                        break;
					case TaskState.Warnings:
						img.UriSource = new Uri("/MetisDM;component/Assets/Images/Warning.png", UriKind.Relative);
                        break;
                    case TaskState.Successful:
                        img.UriSource = new Uri("/MetisDM;component/Assets/Images/CheckSign.png", UriKind.Relative);
                        break;
                    default:
                        img.UriSource = new Uri("/MetisDM;component/Assets/Images/Empty.png", UriKind.Relative);
                        break;
                }
                img.EndInit();
                return img;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
