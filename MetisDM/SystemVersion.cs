﻿namespace MetisDM
{
	public class SystemVersion
    {
	    public string CurrentVersion
		{
			get
			{
				var assembly = System.Reflection.Assembly.GetExecutingAssembly();
				var assemblyName = assembly.GetName().Name;
				var gitVersionInformationType = assembly.GetType(assemblyName + ".GitVersionInformation");
				var versionField = gitVersionInformationType.GetField("AssemblySemVer");
				return (string)versionField.GetValue(null);
			}
		}

		public string CurrentSemanticVersion
		{
			get
			{
				var assembly = System.Reflection.Assembly.GetExecutingAssembly();
				var assemblyName = assembly.GetName().Name;
				var gitVersionInformationType = assembly.GetType(assemblyName + ".GitVersionInformation");
				var versionField = gitVersionInformationType.GetField("FullSemVer");
				return (string)versionField.GetValue(null);
			}
		}
	}
}
