﻿namespace MetisDM.Enums
{
	public enum MigrationTaskType
	{
		Client							= 1,
		Application						= 2, 
		Role							= 3,
		User							= 4,
		AddressStructure				= 5,
		DeviceType						= 6,
		TransmissionDevice				= 7,
		MeasurementDevice				= 8,
		TransmissionDeviceActivation	= 9,
		TransmissionNetwork				= 10,
		TransmissionNetworkDevice		= 11,
		MeasurementDeviceActivation		= 12,
		MeasurementPoint				= 13,
		UserMeasurementPoint			= 14,
		Montage							= 15,
		TransmissionDeviceConnection	= 16,
		MeasurementDeviceConnection		= 17,
		TransmissionNetworkSchedule		= 18,
		Reads							= 19,
	}
}
