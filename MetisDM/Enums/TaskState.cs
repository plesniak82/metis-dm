﻿namespace MetisDM.Enums
{
    public enum TaskState
    {
        WaitingForQueue,
        InProgress,
        Errors,
        Warnings,
        Successful
    }
}
