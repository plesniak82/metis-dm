﻿namespace MetisDM.Enums
{
    public enum SetupStep
    {
        GeneralSettings = 1,
        SourceDatabaseConnectionSettings = 2,
        TargetDatabaseConnectionSettings = 3,
		DevicesMapping = 4,
        Migration = 5,
    }
}
