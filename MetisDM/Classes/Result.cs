﻿using System.Collections.Generic;
using System.Linq;

namespace MetisDM.Classes
{
    public class Result
    {
        public List<string> Errors { get; set; }

        public bool HasErrors { get { return Errors != null && Errors.Any(); } }

        public bool? Value { get; set; }

        public Result(bool? resultValue=null)
        {
            Errors = new List<string>();
            Value = resultValue;
        }

        public Result(IEnumerable<string> errors, bool? resultValue = null)
        {
            Errors = errors.ToList();
            Value = resultValue;
        }

        public Result(string error, bool? resultValue = null)
        {
            Errors = new List<string>{error};
            Value = resultValue;
        }
    }
}
