﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AT.Platforms.DotNet.Prototypes;

namespace MetisDM.Classes
{
	/// <summary>
	/// W razie potrzeby klasa może być klasą generyczną i zapisywać dowolne ustawienia klas do plików
	/// </summary>
	class SettingsWritter<T>
	{
		private readonly T _appSettings;

		// ReSharper disable once UnusedMember.Local
		private SettingsWritter() { }

		public SettingsWritter(T settings)
		{
			_appSettings = settings;

			var settingsTmp = settings as Settings;
			if (settingsTmp != null)
			{

				if (!Directory.Exists(settingsTmp.LogFolder))
				{
					Directory.CreateDirectory(settingsTmp.LogFolder);
				}

				if (!Directory.Exists(settingsTmp.SettingsFolder))
				{
					Directory.CreateDirectory(settingsTmp.SettingsFolder);
				}
			}
		}

		/// <summary>
		/// Zapisanie ustawień do pliku
		/// </summary>
		public void SaveSettings()
		{
			try
			{
				var settingsTmp = _appSettings as Settings;
				if (settingsTmp != null)
				{
					settingsTmp.RefreshSettings();

					if (File.Exists(settingsTmp.SettingsFile))
						File.Delete(settingsTmp.SettingsFile);
				
					_appSettings.SerializeToFile((settingsTmp.SettingsFile));
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Exception");
			}

		}

		/// <summary>
		/// Załądowanie ustawień z pliku
		/// </summary>
		/// <returns></returns>
		public T LoadSettings()
		{
			var appSettings = _appSettings as Settings;
			if (appSettings != null && File.Exists(appSettings.SettingsFile))
			{
				var tmpSettings = Serializer.DeserializeFromFile<T>(appSettings.SettingsFile);

				var pi = typeof (T).GetProperties();
				foreach (var propertyInfo in pi.Where(e => e.Name != "TargetDatabaseConnectionString" && e.Name != "SourceDatabaseConnectionString" && !e.Name.Contains("Setting")))
				{
					var c = propertyInfo.GetValue(tmpSettings, null);
					propertyInfo.SetValue(_appSettings, c,null);
				}
			}
			return _appSettings;
		}
	}
}
