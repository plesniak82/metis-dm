﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight;

namespace MetisDM.Classes
{
    public abstract class ValidateViewModel : ViewModelBase, IDisposable
    {

        #region Errors

        protected readonly List<KeyValuePair<string, string>> Errors = new List<KeyValuePair<string, string>>();

        public bool HasErrors
        {
            get { return Errors.Any(); }
        }

        protected void AddError(string property, string message)
        {
            Errors.Add(new KeyValuePair<string, string>(property, message));
            RaisePropertyChanged("HasErrors");
        }

        protected void AddError(string message)
        {
            AddError(null, message);
        }

        protected void AddErrors(List<string> messages)
        {
            messages.ForEach(e => Errors.Add(new KeyValuePair<string, string>(null, e)));
            RaisePropertyChanged("HasErrors");
        }

        protected void RemoveError(string property)
        {
            Errors.Where(e => e.Key == property).ToList().ForEach(e => Errors.Remove(e));
            RaisePropertyChanged("HasErrors");
        }

        protected void ClearErrors()
        {
            Errors.Clear();
            RaisePropertyChanged("HasErrors");
        }

        public List<KeyValuePair<string, string>> GetErrors()
        {
            return Errors ?? new List<KeyValuePair<string, string>>();
        }

        #endregion

        #region Warnings

        protected readonly List<KeyValuePair<string, string>> Warnings = new List<KeyValuePair<string, string>>();

        public bool HasWarnings
        {
            get { return Warnings.Any(); }
        }

        protected void AddWarning(string property, string message)
        {
            Warnings.Add(new KeyValuePair<string, string>(property, message));
            RaisePropertyChanged("HasWarnings");
        }

        protected void AddWarning(string message)
        {
            AddWarning(null, message);
        }

        protected void AddWarnings(List<string> messages)
        {
            messages.ForEach(e => Warnings.Add(new KeyValuePair<string, string>(null, e)));
            RaisePropertyChanged("HasWarnings");
        }

        protected void RemoveWarning(string property)
        {
            Warnings.Where(e => e.Key == property).ToList().ForEach(e => Warnings.Remove(e));
            RaisePropertyChanged("HasWarnings");
        }

        protected void ClearWarnings()
        {
            Warnings.Clear();
            RaisePropertyChanged("HasWarnings");
        }

        public List<KeyValuePair<string, string>> GetWarnings()
        {
            return Warnings ?? new List<KeyValuePair<string, string>>();
        }

        #endregion

        public void Dispose()
        {
            Cleanup();
        }
    }
}
