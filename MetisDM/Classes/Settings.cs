﻿using MetisDM.Controls.MainPage.ViewModels;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetisDM.Classes
{
	public class Settings
	{
		#region Properties

		public bool MigrateStructure { get; set; }
		public bool MigrateReads { get; set; }
		public string SourceDatabaseServerAddress { get; set; }
		public string SourceDatabaseName { get; set; }
		public string SourceDatabaseUserName { get; set; }
		public string SourceDatabaseUserPassword { get; set; }
		public string TargetDatabaseServerAddress { get; set; }
		public string TargetDatabaseName { get; set; }
		public string TargetDatabaseUserName { get; set; }
		public string TargetDatabaseUserPassword { get; set; }
		public string LogFolder { get; set; }
		public List<List<string>> DeviceMapping { get; set; }

		[XmlIgnore]
		public DevicesMappingViewModel DevicesMappingViewModel { get; set; }

		[XmlIgnore]
		public string SettingsFolder { get; set; }

		[XmlIgnore]
		public string SettingsFile { get; set; }

		[XmlIgnore]
		public string SourceDatabaseConnectionString
		{
			get
			{
				return string.Format(@"metadata=res://Metis1/Metis1Model.csdl|res://Metis1/Metis1Model.ssdl|res://Metis1/Metis1Model.msl;provider=System.Data.SqlClient;provider connection string=""data source={0};initial catalog={1};persist security info=True;user id={2};password={3};MultipleActiveResultSets=True;App=EntityFramework""",
										SourceDatabaseServerAddress, SourceDatabaseName, SourceDatabaseUserName, SourceDatabaseUserPassword);
			}
		}

		[XmlIgnore]
		public string TargetDatabaseConnectionString
		{
			get
			{
				return string.Format(@"metadata=res://Metis2/Metis2Model.csdl|res://Metis2/Metis2Model.ssdl|res://Metis2/Metis2Model.msl;provider=System.Data.SqlClient;provider connection string=""data source={0};initial catalog={1};persist security info=True;user id={2};password={3};MultipleActiveResultSets=True;App=EntityFramework""",
										TargetDatabaseServerAddress, TargetDatabaseName, TargetDatabaseUserName, TargetDatabaseUserPassword);
			}
		}

		#endregion Properties

		#region Fields

		[XmlIgnore]
		public List<int> ClientIds = new List<int>
		{
			//6,
			//7,
			//9,
			//10,
			//47,
		};

		/* "Zielona_Góra_Łuż",
							"Zielona_Góra_Morelowe",
							"Zielona_Góra_Piast",
							"Zielona_Góra_Przyj",
							"Zielona_Góra_Słon" */

		public bool OnlyLastDailyreads = false;

		#endregion Fields

		#region Constructors

		public Settings()
		{
			RefreshSettings();
		}

		#endregion Constructors

		#region Methods

		public void RefreshSettings()
		{
			SettingsFile = string.Format("{0}\\MigrationSettings.xml", SettingsFolder);
		}

		#endregion Methods
	}
}