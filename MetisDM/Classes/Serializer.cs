﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace MetisDM.Classes
{
	public static class Serializer<T>
	{
		public static string ToString(T toSerialize)
		{
			var xmlSerializer = new XmlSerializer(typeof(T));
			var textWriter = new StringWriter();

			xmlSerializer.Serialize(textWriter, toSerialize);
			return textWriter.ToString();
		}

		public static T Deserialize(string str)
		{
			if (str == null) throw new ArgumentNullException("str");
			var serializer = new XmlSerializer(typeof(T));
			var textReader = new StringReader(str);
			return (T)serializer.Deserialize(textReader);
		}
	}
}