﻿using System.Collections.Generic;
using System.Linq;

namespace MetisDM.Classes
{
    public class ResultStatus
    {
        public bool HasErrors { get { return Errors.Any(); }}
        public List<KeyValuePair<string, string>> Errors { get; set; }

        public ResultStatus(List<KeyValuePair<string, string>> errors)
        {
            Errors = errors;
        }
    }
}
