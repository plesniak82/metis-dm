﻿namespace Metis1.Enums
{
	public enum AddressNodeTypes
	{
		Country		= 1,
		Region1		= 2,
		Region2		= 3,
		Region3		= 4,
		Region4		= 5,
		City		= 6,
		Street		= 7,
		Building	= 8,
		Staircase	= 9,
		Local		= 10
	}
}
