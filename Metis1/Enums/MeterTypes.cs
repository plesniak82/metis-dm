﻿namespace Metis1.Enums
{
	public enum MeterTypes
	{
		HeatMeter				= 1,
		ElectricalEnergyMeter	= 2,
		GasMeter				= 3,
		WaterMeter				= 4,
		HeatCostAllocator		= 5, 
		TemperatureSensor		= 6
	}
}
