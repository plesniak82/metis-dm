﻿namespace Metis1.Enums
{
	public enum ReadKinds
	{
		ReadKind_ManualInkasent	= 1,
		ReadKind_ManualReciver	= 2,
		ReadKind_Instant		= 3,
		ReadKind_Logged			= 4,
		ReadKind_Diagnostic		= 5,
		ReadKind_ErrorLog		= 6
	}
}
