﻿using System.Data.Objects;

namespace Metis1
{
	public partial class Metis1Entities : ObjectContext
	{
		public Metis1Entities(string connectionString, int commandTimeout) :  base(connectionString, "Metis1Entities")
        {
			CommandTimeout = commandTimeout;
			ContextOptions.LazyLoadingEnabled = true;
        }
	}
}
