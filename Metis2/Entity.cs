﻿using System.Data.Objects;

namespace Metis2
{
	public partial class Metis2Entities : ObjectContext
	{
		public Metis2Entities(string connectionString, int commandTimeout) :  base(connectionString, "Metis2Entities")
        {
			CommandTimeout = commandTimeout;
			ContextOptions.LazyLoadingEnabled = true;
        }
	}
}
