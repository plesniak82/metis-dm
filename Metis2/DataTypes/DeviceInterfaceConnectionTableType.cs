﻿namespace Metis2.DataTypes
{
	public class DeviceInterfaceConnectionTableType
	{
		#region Fields

		public int ChildInterface_Id;
		public int? ConnectionEvent_Id;
		public int? OpticalInterfaceProperties_Id;
		public int? OpticalShieldProperties_Id;
		public int ParentInterface_Id;
		public int? PulseWeight_Id;
		public int? VirtualInterfaceNumber;

		#endregion Fields

		#region Constructors

		public DeviceInterfaceConnectionTableType(int childInterface_Id,
													int? connectionEvent_Id,
													int? opticalInterfaceProperties_Id,
													int? opticalShieldProperties_Id,
													int parentInterface_Id,
													int? pulseWeight_Id)
		{
			ChildInterface_Id = childInterface_Id;
			ConnectionEvent_Id = connectionEvent_Id;
			OpticalInterfaceProperties_Id = opticalInterfaceProperties_Id;
			OpticalShieldProperties_Id = opticalShieldProperties_Id;
			ParentInterface_Id = parentInterface_Id;
			PulseWeight_Id = pulseWeight_Id;
		}

		#endregion Constructors
	}
}