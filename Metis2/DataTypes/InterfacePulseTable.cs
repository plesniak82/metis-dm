﻿namespace Metis2.DataTypes
{
	public class InterfacePulseTable
	{
		public int? DeviceType_Id;
		public int? Interface_Id;
		public int? InterfaceNumber;
		public int? InterfacePulse_Id;
		public bool IsDefault;
		public int PulseWeight_Id;

		public InterfacePulseTable(	int? deviceTypeId,
									int? interfaceId,
									int? interfaceNumber,
									int? interfacePulseId,
									bool isDefault,
									int pulseWeightId)
		{
			DeviceType_Id		= deviceTypeId;
			Interface_Id		= interfaceId;
			InterfaceNumber		= interfaceNumber;
			InterfacePulse_Id	= interfacePulseId;
			IsDefault			= isDefault;
			PulseWeight_Id		= pulseWeightId;
		}
	}
}
