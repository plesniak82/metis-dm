﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metis2.DataTypes
{
	public class ScheduleItemType
	{
		public byte? DayOfMonth;
		public byte? DayOfWeek;
		public decimal? Duration;
		public byte? HourOfDay;
		public int Id;
		public decimal? Interval;
		public bool IsEnabled;
		public bool? LastDayOfMonth;
		public byte? MinuteOfHour;
		public byte? MonthOfYear;
		public byte Offset;
		public DateTimeOffset? OnDate;
		public int Schedule_Id;

		public ScheduleItemType(byte? dayOfMonth,
								byte? dayOfWeek,
								decimal? duration,
								byte? hourOfDay,
								int id, 
								decimal? interval,
								bool isEnabled,
								bool? lastDayOfMonth,
								byte? minuteOfHour,
								byte? monthOfYear,
								byte offset,
								DateTimeOffset? onDate,
								int schedule_Id)
		{
			Id				= id;
			DayOfMonth		= dayOfMonth;
			DayOfWeek		= dayOfWeek;
			Duration		= duration;
			HourOfDay		= hourOfDay;
			Interval		= interval;
			IsEnabled		= isEnabled;
			LastDayOfMonth	= lastDayOfMonth;
			MinuteOfHour	= minuteOfHour;
			MonthOfYear		= monthOfYear;
			Offset			= offset;
			OnDate			= onDate;
			Schedule_Id		= schedule_Id;
		}
	}
}
