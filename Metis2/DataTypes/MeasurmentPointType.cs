﻿using System;

namespace Metis2.DataTypes
{
	public class MeasurmentPointType
	{
		public int? AddressNode_Id;
		public int? Coordinate_Id;
		public int DataSource;
		public String Description;
		public String GisAddress;
		public int? Id;
		public bool? IsActive;
		public decimal? Latitude;
		public String Location;
		public decimal? Longitude;
		public bool? MainMeter;
		public int MeasuredQuantity_Id;
		public int? MeasuredPoint_Id;
		public String Name;
		public int? NetworkType_Id;

		public MeasurmentPointType(	int? addressNode_Id,
									int? coordinate_Id,
									int dataSource,
									String description,
									String gisAddress,
									int? id,
									bool? isActive,
									decimal? latitude,
									String location,
									decimal? longitude,
									bool? mainMeter,
									int measuredQuantity_Id,
									int? measuredPoint_Id,
									String name,
									int? networkType_Id)
		{
			AddressNode_Id			= addressNode_Id;
			Coordinate_Id			= coordinate_Id;
			DataSource				= dataSource;
			Description				= description;
			GisAddress				= gisAddress;
			Id						= id;
			IsActive				= isActive;
			Latitude				= latitude;
			Location				= location;
			Longitude				= longitude;
			MainMeter				= mainMeter;
			MeasuredQuantity_Id		= measuredQuantity_Id;
			MeasuredPoint_Id		= measuredPoint_Id;
			Name					= name;
			NetworkType_Id			= networkType_Id;
			
		}
	}
}
