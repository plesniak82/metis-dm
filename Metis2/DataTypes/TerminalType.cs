﻿using System;

namespace Metis2.DataTypes
{
	public class TerminalType
	{
		public DateTimeOffset? ActivationDateFrom;
		public DateTimeOffset? ActivationDateTo;
		public int? Id;
		public String MeasurementPointName;
		public int? MeasurmentPoint_Id;
		public String Name;
		public int? NetworkType_Id;

		public TerminalType(	DateTimeOffset? activationDateFrom,
								DateTimeOffset? activationDateTo,
								int? id,
								String measurementPointName,
								int? measurmentPoint_Id,
								String name,
								int? networkType_Id)
		{
			ActivationDateFrom		= activationDateFrom;
			ActivationDateTo		= activationDateTo;
			Id						= id;
			MeasurementPointName	= measurementPointName;
			MeasurmentPoint_Id		= measurmentPoint_Id;
			Name					= name;
			NetworkType_Id			= networkType_Id;
		}
	}
}
