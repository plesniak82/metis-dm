﻿namespace Metis2.DataTypes
{
	public class ClientResource
	{
		public int ClientResource_Id;
		public int? ClientResourceType_Id;

		public ClientResource(	int clientResource_Id,
								int? clientResourceType_Id)
		{
			ClientResource_Id		= clientResource_Id;
			ClientResourceType_Id	= clientResourceType_Id;
		}
	}
}
