﻿namespace Metis2.Enums
{
	public enum AddressNodeTypes
	{
		City		= 1,
		Street		= 2,
		Building	= 3,
		Staircase	= 4,
		Local		= 5
	}
}
