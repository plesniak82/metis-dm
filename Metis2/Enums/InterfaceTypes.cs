﻿namespace Metis2.Enums
{
	public enum InterfaceTypes
	{
		TCP							= 1,
		RS232						= 2,
		IMP							= 3,
		WMBUS						= 4,
		WaterMeasurement			= 5,
		TempeatureMeasurement		= 6,
		PreasureMeasurement			= 7,
		GasMeasurement				= 8,
		EletricalEnergyMeasurement	= 9,
		HeatMeasurement				= 10,
		HeatAllocating				= 12,
		Optical						= 13
	}
}
