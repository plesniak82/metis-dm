﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metis2.Enums
{
	public enum DeviceTypeGroups
	{
		Converters = 1,
		WmbusConcentrators = 2,
		WmbusRetransmiters = 3,
		WaterMeterTransmissionDevices = 4,
		GasMeterTransmissionDevices = 5,
		HeatMeterTransmissionDevices = 6,
		WaterMeters = 7,
		GasMeters = 8,
		HeatMeters = 9,
		ElectricEnergyMeters = 10,
		HeatCostAllocators = 11,
		Other = 12,
		TemperatureControllers = 13,
		ElectricityMetersModules = 14
	}
}
