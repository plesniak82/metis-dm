﻿namespace Metis2.Enums
{
	public enum TransmissionNetworkTypes
	{
		WmbusRadioNetwork	= 1,
		BatteryGsmNetwork	= 2,
	}
}
