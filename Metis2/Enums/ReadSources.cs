﻿namespace Metis2.Enums
{
	public enum ReadSources
	{
		Device		= 1,
		User		= 2,
		Event		= 3,
		Terminal	= 4
	}
}
