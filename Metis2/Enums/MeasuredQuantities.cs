﻿namespace Metis2.Enums
{
	public enum MeasuredQuantities
	{
		//ColdWaterUsage			= 1,
		//WarmWaterUsage			= 2,
		//GasUsage				= 3,
		//ElectricEnergyUsage		= 4,
		//HeatEnergyUsage			= 5,
		//HeatEnergyCost			= 6,
		//Temperature				= 7,
		//Pressure				= 8,

		ColdWaterUsage			= 1,
		WarmWaterUsage			= 2,
		GasUsage				= 3,
		HeatEnergyUsage			= 4,
		HeatEnergyCost			= 5,
		ElectricEnergyUsage		= 6,
		Temperature				= 7,
		Pressure				= 8
	}
}
